# PCF Graphics and UI Library Seed Project
This folder is a seed project that is a good starting point to build UI and Graphics application using 
the PCF UI and Graphics library. 

## Folder structure

 * pcf - This folder contains the PCF Graphics and UI Library. It contains the required header files and dlls
 * app - This folder contains the src, headers, examples and build scripts specific to your application.

### PCF 
You would want to browse through the headers in pcf/ui and pcf/graphics to get an understanding of the API available.

### app
The app folder is structured as follows:

#### Directories 

 * examples - Contains examples on how to use different UI controls/widgets
 * include - Your header (*.h) files  go here. Certain helper header files are provided. Do not edit those files
 * resources -  Contains resources and manifest files required by the library. Do not change these files. 
 * srcs - Your source (*.c) files go here. A file init.c and main.c is provided. init.c contains helper code do not edit that file. main.c contains the main() function. Add your code to this file. You can add new files to this folder. 
 * bin - This folder does not exist at first. It will be created once you build your application. It contains the final executable
 * build - This folder does not exist at first. It will be created once you build your application. It contains temporary files required to build the final application. 

#### Files

 * makefile - The Build script. You should not need to change this file unless you modify the folder structure or need certain other libraries in your application. If you add sub-folders/directories to the srcs folder you will need to modify the makefile accordingly

## Prerequisites

To use the library and build the applications you will require mingw64. 

You can download the same from https://mingw-w64.org/ 

***Note***: *The library provided has been built and tested with mingw64 (i686 version), hence while installing mingw64 choose the i686 version*

## How to build the app

Go to the app folder 
`cd app`

To build the application run the command:

`make app`

To build the examples run the command:

`make examples`

To build bothe the app and examples run 

`make` or `make all`

To clean the project run:

`make clean`

