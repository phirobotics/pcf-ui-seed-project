/**
 * In this example shows how to create a form and add a button to the form.
 * 
 **/

#include <stdio.h>
#include <app_sysinit.h>

#include <pcf/ui/ui.h>
#include <pcf/graphics/graphics.h>

#include <pcf/ui/mainform/mainform.h>
#include <pcf/ui/control/control.h>
#include <pcf/ui/button/button.h>

void addButton(UiMainForm_t* mainform, int x, int y, uint32_t width, uint32_t height, const char* text);
void PCF_STDCALL buttonClickHandler(UiButton_t* button);

int main() {
    int failed = app_sysinit();
    if(failed) {
        printf("System failed to initialize \n");
        return failed;
    }

    printf("System Initialized\n");
    
    // Control info for creating the main form.
    UiControlInfo_t info = {0};

    info.parent = NULL;
    info.typeId = enControlTypeIdValue_Mainform;
    info.location.X = 10;
    info.location.Y = 10;
    info.size.Height = 400;
    info.size.Width = 400;
    info.maximumSize.Height = 800;
    info.maximumSize.Width  = 800;
    info.minimumSize.Height = 300;
    info.minimumSize.Width  = 300;
    info.text = "Button - MainForm";

    pcf_status_t status;

    // Create main form.
    UiMainForm_t* mainform = pcf_ui_mainform_create(&info, &status);

    if(mainform) {
        int32_t exitcode;

        addButton(mainform, 10, 50, 100, 20, "Button");

        status = pcf_ui_mainform_run(mainform, &exitcode);//

        fprintf(stdout, "mainform exited with code = %d, status = %d\n", exitcode, status);
    }

    return 0;
}

void addButton(UiMainForm_t* mainform, int x, int y, uint32_t width, uint32_t height, const char* text) {
    // Get the control object for the main form
    UiControl_t* parent = pcf_ui_mainform_asControl(mainform);
    
    if(parent) {
        // Control info for creating button
        UiControlInfo_t info = {0};
        info.parent = parent;
        info.typeId = enControlTypeIdValue_SimpleButton;
        info.location.X = x;
        info.location.Y = y;
        info.size.Height = height;
        info.size.Width = width;
        info.text = "Button Text";//Shall have text "What?"
        pcf_status_t status;

        UiButton_t* button = pcf_ui_button_new(&info,&status);

        if(button == NULL) {
            fprintf(stdout, "Failed while creating button control, status = %d\n", status);
            return;
        } else {
            if(pcf_ui_button_attachClickedEventHandler(button, buttonClickHandler)) {
                fprintf(stdout, "Failed while adding button click handler\n");
                return;
            }
        }
    }
}

void PCF_STDCALL buttonClickHandler(UiButton_t* button)
{
    UiControl_t* control = pcf_ui_button_asControl(button);
    pcf_ui_property_text text;
    if(!pcf_ui_control_getText(control, text))
    {
        fprintf(stdout, "Button with text = %s is clicked\n", text);
    }
}