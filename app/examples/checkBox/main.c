#include <stdio.h>
#include <app_sysinit.h>

#include <pcf/ui/ui.h>
#include <pcf/graphics/graphics.h>

#include <pcf/ui/mainform/mainform.h>
#include <pcf/ui/control/control.h>

#include <pcf/ui/checkbox/checkbox.h>

void addCheckBox(UiMainForm_t* mainform, int x, int y, uint32_t width, uint32_t height, const char* text);

int main()
{
    int failed = app_sysinit();
    if(failed)
    {
        printf("System failed to initialize \n");
        return failed;
    }
    printf("System Initialized\n");
    

    UiControlInfo_t info = {0};

    info.parent = NULL;

    info.typeId = enControlTypeIdValue_Mainform;

    info.location.X = 10;
    info.location.Y = 10;
    info.size.Height = 400;
    info.size.Width = 400;
    info.maximumSize.Height = 800;
    info.maximumSize.Width  = 800;
    info.minimumSize.Height = 300;
    info.minimumSize.Width  = 300;
    info.text = "Test MainForm";

    pcf_status_t status;

    UiMainForm_t* mainform = pcf_ui_mainform_create(&info, &status);

    if(mainform) {
        int32_t exitcode;

        addCheckBox(mainform, 10, 138, 100, 24, "Checkbox");

        status = pcf_ui_mainform_run(mainform, &exitcode);

        fprintf(stdout, "mainform exited with code = %d, status = %d\n", exitcode, status);
    }

    return 0;
}

void addCheckBox(UiMainForm_t* mainform, int x, int y, uint32_t width, uint32_t height, const char* text) {
    UiControl_t* parent = pcf_ui_mainform_asControl(mainform);
    if(parent) {
        UiControlInfo_t info = {0};
        info.parent = parent;
        info.typeId = enControlTypeIdValue_CheckBox;
        info.location.X = x;
        info.location.Y = y;
        info.size.Height = height;
        info.size.Width = width;
        info.text = "(Un)Check this";
        pcf_status_t status;
        UiCheckBox_t* chbx = pcf_ui_checkbox_new(&info,&status);
        if(chbx == NULL) {
            fprintf(stdout, "Failed while creating checkbox control, status = %d\n", status);
            return;
        }
    }
}
