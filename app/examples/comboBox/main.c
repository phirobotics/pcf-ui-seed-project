#include <stdio.h>
#include <app_sysinit.h>

#include <pcf/ui/ui.h>
#include <pcf/graphics/graphics.h>

#include <pcf/ui/mainform/mainform.h>
#include <pcf/ui/control/control.h>

#include <pcf/ui/combobox/combobox.h>

void addComboBox(UiMainForm_t* mainform, int x, int y, uint32_t width, uint32_t height);
void PCF_STDCALL comboboxIndexChangeHandler(const UiComboBox_t* comboBox, int32_t oldIndex, int32_t newIndex);

int main()
{
    int failed = app_sysinit();
    if(failed)
    {
        printf("System failed to initialize \n");
        return failed;
    }
    printf("System Initialized\n");
    
    UiControlInfo_t info = {0};

    info.parent = NULL;

    info.typeId = enControlTypeIdValue_Mainform;

    info.location.X = 10;
    info.location.Y = 10;
    info.size.Height = 400;
    info.size.Width = 400;
    info.maximumSize.Height = 800;
    info.maximumSize.Width  = 800;
    info.minimumSize.Height = 300;
    info.minimumSize.Width  = 300;
    info.text = "Test MainForm";

    pcf_status_t status;

    UiMainForm_t* mainform = pcf_ui_mainform_create(&info, &status);

    if(mainform){
        int32_t exitcode;

        addComboBox(mainform, 10, 110, 100, 24);

        status = pcf_ui_mainform_run(mainform, &exitcode);//

        fprintf(stdout, "mainform exited with code = %d, status = %d\n", exitcode, status);
    }

    return 0;
}

void addComboBox(UiMainForm_t* mainform, int x, int y, uint32_t width, uint32_t height)
{
    UiControl_t* parent = pcf_ui_mainform_asControl(mainform);
    if(parent)
    {
        UiControlInfo_t info = {0};
        info.parent = parent;
        info.typeId = enControlTypeIdValue_ComboBox;
        info.location.X = x;
        info.location.Y = y;
        info.size.Height = height;
        info.size.Width = width;
        info.text = NULL;
        pcf_status_t status;
        UiComboBox_t* cmbx = pcf_ui_combobox_new(&info,&status);
        if(cmbx == NULL)
        {
            fprintf(stdout, "Failed while creating combobox control, status = %d\n", status);
            return;
        }
        int index = -1;
        pcf_ui_combobox_addText(cmbx, "Item 0", &index);
        pcf_ui_combobox_addText(cmbx, "Item 1", &index);
        pcf_ui_combobox_addText(cmbx, "Item 2", &index);
        pcf_ui_combobox_addText(cmbx, "Item 3", &index);

        pcf_ui_combobox_attachSelectedIndexChangeEventHandler(cmbx, comboboxIndexChangeHandler);
    }
}

void PCF_STDCALL comboboxIndexChangeHandler(const UiComboBox_t* comboBox, int32_t oldIndex, int32_t newIndex)
{
    pcf_ui_property_text oldText;
    pcf_ui_property_text newText;

    pcf_ui_combobox_getItemText(comboBox, oldIndex, oldText);
    pcf_ui_combobox_getItemText(comboBox, newIndex, newText);
    fprintf(stdout, "Combobox Old Item = %s, New item = %s\n", oldText, newText);
}