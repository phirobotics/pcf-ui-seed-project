#include <stdio.h>
#include <app_sysinit.h>

#include <pcf/ui/ui.h>
#include <pcf/graphics/graphics.h>
#include <pcf/ui/mainform/mainform.h>
#include <pcf/ui/control/control.h>

void testGraphicsContext(UiMainForm_t* parentForm, int x, int y, int width, int height);

int main()
{
    int failed = app_sysinit();
    if(failed)
    {
        printf("System failed to initialize \n");
        return failed;
    }
    printf("System Initialized\n");
    //write your code after this line
    UiControlInfo_t info = {0};

    info.parent = NULL;

    info.typeId = enControlTypeIdValue_Mainform;

    info.location.X = 10;
    info.location.Y = 10;
    info.size.Height = 400;
    info.size.Width = 400;
    info.maximumSize.Height = 800;
    info.maximumSize.Width  = 800;
    info.minimumSize.Height = 300;
    info.minimumSize.Width  = 300;
    info.text = "Test MainForm";

    pcf_status_t status;

    UiMainForm_t* mainform = pcf_ui_mainform_create(&info, &status);

    if(mainform){
        int32_t exitcode;

        testGraphicsContext(mainform, 200, 140, 160, 120);

        status = pcf_ui_mainform_run(mainform, &exitcode);//

        fprintf(stdout, "mainform exited with code = %d, status = %d\n", exitcode, status);
    }

    return 0;
}
