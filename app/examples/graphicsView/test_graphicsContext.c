#include <stdio.h>
#include <pcf/graphics/graphics.h>

#include <pcf/graphics/context2d/context2d.h>
#include <pcf/graphics/pen/pen.h>
#include <pcf/graphics/path/path.h>
#include <pcf/graphics/brush/brush.h>
#include <pcf/graphics/image/image.h>

#include <pcf/ui/ui.h>
#include <pcf/ui/control/control.h>
#include <pcf/ui/mainform/mainform.h>
#include <pcf/ui/drawingview/drawingview.h>




void PCF_STDCALL DrawingViewPaintEventHandler(UiDrawingView_t* view, UiPaintEventArgs_t* e);
void PCF_STDCALL DrawingViewMouseClickEventHandler(const UiControl_t* sender, const UiControlMouseClickEventArgs_t* eventArgs);
void PCF_STDCALL DrawingViewMouseMoveEventHandler(const UiControl_t* sender, const UiControlMouseEventArgs_t* eventArgs);
void printImageData(GraphicsImageData_t* data);

void testGraphicsContext(UiMainForm_t* parentForm, int x, int y, int width, int height)
{
    UiControl_t* parent = pcf_ui_mainform_asControl(parentForm);
    if(parent)
    {
        UiControlInfo_t info = {0};
        info.parent = parent;
        info.typeId = enControlTypeIdValue_DrawingView;
        info.location.X = x;
        info.location.Y = y;
        info.size.Height = height;
        info.size.Width = width;
        info.text = "This is a dview";//Shall have text "What?"
        pcf_status_t status;
        UiDrawingView_t* view = pcf_ui_drawingView_new(&info,&status);
        if(view)
        {
            fprintf(stdout, "Created drawing view\n");
            status = pcf_ui_drawingView_attachPaintEventHandler(view, DrawingViewPaintEventHandler);
            if(status)
            {
                fprintf(stdout, "Failed while adding paint handler to drawing-view\n");
            }
            else
            {
                UiControl_t* drawingControl = pcf_ui_drawingView_asControl(view);
                pcf_ui_control_attachMouseClickEventHandler(drawingControl, DrawingViewMouseClickEventHandler);
                pcf_ui_control_attachMouseMoveEventHandler(drawingControl, DrawingViewMouseMoveEventHandler);
                fprintf(stdout, "Successfully added paint handler to drawing-view\n");
            }
        }
        else
        {
            fprintf(stdout, "Failed while creating drawing view\n");
        }
    }
    else
    {
        fprintf(stdout, "Failed while converting parent as control\n");
    }
}

void PCF_STDCALL DrawingViewMouseClickEventHandler(const UiControl_t* sender, const UiControlMouseClickEventArgs_t* eventArgs)
{
    if(eventArgs->down)
    {
        fprintf(stdout, "Mouse Down within DrawingView at (X, Y) = (%d, %d)\n", eventArgs->x, eventArgs->y);
    }
    else
    {
        fprintf(stdout, "Mouse Up within DrawingView at (X, Y) = (%d, %d)\n", eventArgs->x, eventArgs->y);
    }
}
void PCF_STDCALL DrawingViewMouseMoveEventHandler(const UiControl_t* sender, const UiControlMouseEventArgs_t* eventArgs)
{
    fprintf(stdout, "Mouse within DrawingView at (X, Y) = (%d, %d)\n", eventArgs->x, eventArgs->y);
}

void printImageData(GraphicsImageData_t* data)
{
    if(data)
    {
        int iw = data->width;
        int ih = data->height;
        int pixJump = data->bitsPerPixel/8;
        for(int i = 0; i < ih; i++)
        {
            uint8_t* scanLinePtr = data->imageBuffer + i * data->stride;
            for(int j = 0; j < iw; j++)
            {
                uint8_t* pixelPtr = scanLinePtr + j * pixJump;
                uint8_t  alpha  = *(pixelPtr + 3);
                uint8_t  red = *(pixelPtr + 2);
                uint8_t  green   = *(pixelPtr + 1);
                uint8_t  blue   = *(pixelPtr);
                printf("[A = %03d, R = %03d, G = %03d, B = %03d] " , alpha, red, green, blue);
            }
            printf("\n");
        }
        switch(data->format)
        {
            case enGraphicsImagePixelFormat_1BPP:
                fprintf(stdout, "Image format = enGraphicsImagePixelFormat_1BPP\n");
                break;
            case enGraphicsImagePixelFormat_4BPP:
                fprintf(stdout, "Image format = enGraphicsImagePixelFormat_4BPP\n");
                break;
            case enGraphicsImagePixelFormat_8BPP:
                fprintf(stdout, "Image format = enGraphicsImagePixelFormat_8BPP\n");
                break;
            case enGraphicsImagePixelFormat_A1R5G5B5:
                fprintf(stdout, "Image format = enGraphicsImagePixelFormat_A1R5G5B5\n");
                break;
            case enGraphicsImagePixelFormat_Gray16:
                fprintf(stdout, "Image format = enGraphicsImagePixelFormat_Gray16\n");
                break;
            case enGraphicsImagePixelFormat_A0R5G5B5:
                fprintf(stdout, "Image format = enGraphicsImagePixelFormat_A0R5G5B5\n");
                break;
            case enGraphicsImagePixelFormat_R5G6B5:
                fprintf(stdout, "Image format = enGraphicsImagePixelFormat_R5G6B5\n");
                break;
            case enGraphicsImagePixelFormat_R8G8B8:
                fprintf(stdout, "Image format = enGraphicsImagePixelFormat_R8G8B8\n");
                break;
            case enGraphicsImagePixelFormat_A8R8G8B8:
                fprintf(stdout, "Image format = enGraphicsImagePixelFormat_A8R8G8B8\n");
                break;
            case enGraphicsImagePixelFormat_PA8R8G8B8:
                fprintf(stdout, "Image format = enGraphicsImagePixelFormat_PA8R8G8B8\n");
                break;
            case enGraphicsImagePixelFormat_R16G16B16:
                fprintf(stdout, "Image format = enGraphicsImagePixelFormat_R16G16B16\n");
                break;
            case enGraphicsImagePixelFormat_A16R16G16B16:
                fprintf(stdout, "Image format = enGraphicsImagePixelFormat_A16R16G16B16\n");
                break;
            case enGraphicsImagePixelFormat_PA16R16G16B16:
                fprintf(stdout, "Image format = enGraphicsImagePixelFormat_PA16R16G16B16\n");
                break;
            default:
                fprintf(stdout, "Image format = UnKnown\n");
                break;
        }
    }
    printf("\n");
}

void PCF_STDCALL DrawingViewPaintEventHandler(UiDrawingView_t* view, UiPaintEventArgs_t* e)
{

    pcf_status_t status;
    pcf_graphics_context2D_setSmoothingMode(e->context, enGraphicsContextSmoothingMode_HighestQuality);
    GraphicsPoint_t points[4];
    points[0].X = 10;
    points[0].Y = 10;
    points[1].X = 50;
    points[1].Y = 10;
    points[2].X = 30;
    points[2].Y = 40;
    points[3].X = 60;
    points[3].Y = 80;
    GraphicsPenConfig_t penConfig = {0};

    penConfig.color.value = ((0xFF << 24) | (0xFF) << 16 | (0x00 << 8) | (0x00));
    penConfig.endCap = enGraphicsLineCap_None;
    penConfig.startCap = enGraphicsLineCap_None;
    penConfig.lineJoinType = enGraphicsLineJoinType_Round;
    penConfig.style = enGraphicsPenStyle_Solid;
    penConfig.extra = NULL;
    penConfig.width = 1.0f;

    GraphicsBrushConfig_t brushConfig = {0};
    brushConfig.foreColor.value = (0xFF << 24 | 0x00 << 16 | 0xFF << 8 | 0x00);
    brushConfig.backColor.value = (0x00 << 24 | 0xFF << 16 | 0xFF << 8 | 0xFF);

    GraphicsImage_t* brushImage = pcf_graphics_image_newFromFile("images/3x3RGB.bmp", &status);
    if(brushImage != NULL && status == PCF_GFX_StatusCode_None)
    {
        brushConfig.type = enGraphicsBrushType_Pattern;
        brushConfig.patternBrushRect.Height = 3;
        brushConfig.patternBrushRect.Width = 3;
        brushConfig.patternBrushRect.X = 0;
        brushConfig.patternBrushRect.Y = 0;
        brushConfig.patternImage = brushImage;
        GraphicsImageData_t* idata = pcf_graphics_image_getData(brushImage, &status);
        if(status)
        {
            fprintf(stdout, "Failed while getting image data, status = %d\n", status);
        }
        else
        {
            fprintf(stdout, "Image Data width, height, bpp, stride, bufferSize = %d, %d, %d, %d, %d\n",
                     idata->width, idata->height, idata->bitsPerPixel, idata->stride, idata->bufferSize);
            printImageData(idata);
            pcf_graphics_image_freeData(idata);
        }
    }
    else
    {
        brushConfig.type = enGraphicsBrushType_Hatch;
        brushConfig.hatchType = enGraphicsBrushHatchStyle_ForwardDiagonal;
        brushConfig.wrapMode  = enGraphicsWrapMode_Tile;
        brushConfig.patternBrushRect.Height = 0;
        brushConfig.patternBrushRect.Width = 0;
        brushConfig.patternBrushRect.X = 0;
        brushConfig.patternBrushRect.Y = 0;
        brushConfig.patternImage = NULL;
    }

    GraphicsPen_t* pen = pcf_graphics_pen_new(&penConfig, &status);
    GraphicsPath_t* path = pcf_graphics_path_new(&status);
    GraphicsBrush_t* brush = pcf_graphics_brush_new(&brushConfig, &status);

    pcf_graphics_path_addBezier(path, 120, 1, 110, 20, 90, 30, 130, 5);
    pcf_graphics_path_addLine(path, 10,10, 50, 100);
    pcf_graphics_path_addLines(path, points, 3, NULL);
    pcf_graphics_context2D_drawFillPath(e->context, brush, path);

    status = pcf_graphics_context2D_drawLine(e->context, pen, 100, 100, 150, 150);
    pcf_graphics_pen_setColor(pen, (0xFF << 24 | 0x00 << 16 | 0x00 << 8 | 0xFF));
    pcf_graphics_context2D_drawPolygon(e->context, pen, points, 3);

    points[0].X = 30;
    points[0].Y = 40;
    points[1].X = 60;
    points[1].Y = 40;
    points[2].X = 45;
    points[2].Y = 80;
    pcf_graphics_pen_setColor(pen, (0xFF << 24 | 0xFF << 16 | 0x00 << 8 | 0xFF));
    pcf_graphics_pen_setWidth(pen, 3.0f);
    pcf_graphics_context2D_drawPolygon(e->context, pen, points, 3);

    pcf_graphics_pen_setColor(pen, (0xFF << 24 | 0x00 << 16 | 0xFF << 8 | 0xFF));
    pcf_graphics_pen_setWidth(pen, 1.5f);


    points[0].X = e->clientRect.Width - 10;
    points[0].Y = e->clientRect.Height - 10;
    points[1].X = e->clientRect.Width - 50;
    points[1].Y = e->clientRect.Height - 10;
    points[2].X = e->clientRect.Width - 30;
    points[2].Y = e->clientRect.Height - 40;
    points[3].X = 60;
    points[3].Y = 80;

    Context2DState_t state;
    pcf_graphics_context2D_pushState(e->context, &state);
    pcf_graphics_pen_setColor(pen, (0xF1 << 24 | 0x00 << 16 | 0x00 << 8 | 0xFF));
    pcf_graphics_pen_setWidth(pen, 1.0f);

    pcf_graphics_context2D_translateTransformF(e->context, e->clientRect.Width/2, -e->clientRect.Height/2, enGraphicsMatrixOperationOrder_Append);
    fprintf(stdout, "tx = %d, ty = %d\n", e->clientRect.Width/2, -e->clientRect.Height/2);
    pcf_graphics_context2D_scaleTransformF(e->context, 1.0f, -1.0f, enGraphicsMatrixOperationOrder_Append);

    pcf_graphics_pen_setLineJoinType(pen, enGraphicsLineJoinType_Miter);
    pcf_graphics_pen_setStartCap(pen, enGraphicsLineCap_SquareAnchor);
    pcf_graphics_pen_setEndCap(pen, enGraphicsLineCap_DiamondAnchor);

    pcf_graphics_context2D_drawEllise(e->context, pen, 10 ,10 , 40, 40);
    pcf_graphics_context2D_drawLine(e->context, pen, 10 ,10 , 40, 40);
    pcf_graphics_context2D_drawBezier(e->context, pen, 0, 0, 10, 20, 30, 30, 40, 20);

    pcf_graphics_context2D_popState(e->context, state);

    pcf_graphics_pen_setColor(pen, (0x6A << 24 | 0xFF << 16 | 0x00 << 8 | 0x0F));
    pcf_graphics_pen_setWidth(pen, 1.0f);

    pcf_graphics_context2D_drawLines(e->context, pen, points, 4);
    pcf_graphics_context2D_drawBezier(e->context, pen, points[0].X, points[0].Y,
                                      points[1].X, points[1].Y,
                                      points[2].X, points[2].Y,
                                      points[3].X, points[3].Y);

    pcf_graphics_brush_delete(brush);
    pcf_graphics_image_delete(brushImage);
    pcf_graphics_path_delete(path);
    pcf_graphics_pen_delete(pen);
    return;
}
