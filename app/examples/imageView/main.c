#include <stdio.h>
#include <app_sysinit.h>

#include <pcf/ui/ui.h>
#include <pcf/graphics/graphics.h>

#include <pcf/ui/mainform/mainform.h>
#include <pcf/ui/control/control.h>

#include <pcf/ui/imageview/imageview.h>

#include <pcf/io/path/path.h>

void addImageView(UiMainForm_t* mainform, int x, int y, uint32_t width, uint32_t height, const char* imagefile);

int main()
{
    int failed = app_sysinit();
    if(failed)
    {
        printf("System failed to initialize \n");
        return failed;
    }
    printf("System Initialized\n");
    //write your code after this line
    UiControlInfo_t info = {0};

    info.parent = NULL;

    info.typeId = enControlTypeIdValue_Mainform;

    info.location.X = 10;
    info.location.Y = 10;
    info.size.Height = 200;
    info.size.Width = 400;
    info.maximumSize.Height = 800;
    info.maximumSize.Width  = 800;
    info.minimumSize.Height = 300;
    info.minimumSize.Width  = 300;
    info.text = "Test MainForm";

    pcf_status_t status;

    UiMainForm_t* mainform = pcf_ui_mainform_create(&info, &status);

    if(mainform){
        int32_t exitcode;

        const char* exeDir = pcf_ui_getApplicationExeDirectory(&status);

        if(status)
        {
            fprintf(stdout, "Failed to get the application directory, status = %d\n", status);
        }

        fprintf(stdout, "The directory is = %s\n", exeDir);

        pcf_io_path_t imagePath = {0};
        status = pcf_io_path_joinPath(exeDir, "..//..//images//Logo.bmp", imagePath);
        const char* iPath = status == 0 ? imagePath : NULL;
        addImageView(mainform, 20, 10, 302, 97, iPath);
        
        status = pcf_ui_mainform_run(mainform, &exitcode);//

        fprintf(stdout, "mainform exited with code = %d, status = %d\n", exitcode, status);
    }

    return 0;
}

void addImageView(UiMainForm_t* mainform, int x, int y, uint32_t width, uint32_t height, const char* imagefile)
{
    UiControl_t* parent = pcf_ui_mainform_asControl(mainform);
    if(parent)
    {
        UiControlInfo_t info = {0};
        info.parent = parent;
        info.typeId = enControlTypeIdValue_ImageView;
        info.location.X = x;
        info.location.Y = y;
        info.size.Height = height;
        info.size.Width = width;
        info.text = "Image";
        pcf_status_t status;
        UiImageView_t* imview = pcf_ui_imageView_new(&info,&status);
        if(imview == NULL)
        {
            fprintf(stdout, "Failed while creating imview control, status = %d\n", status);
            return;
        }
        else
        {
            if(pcf_io_path_doesFileExist(imagefile))
            {
                status = pcf_ui_imageView_show(imview, imagefile);
            }
            else
            {
                fprintf(stderr, "Image file doesn't exist!");
                return;
            }
        }
    }
}
