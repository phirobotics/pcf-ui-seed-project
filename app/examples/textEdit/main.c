#include <stdio.h>
#include <app_sysinit.h>

#include <pcf/ui/ui.h>
#include <pcf/graphics/graphics.h>
#include <pcf/ui/mainform/mainform.h>
#include <pcf/ui/control/control.h>

#include <pcf/ui/textedit/textedit.h>

void addTextEdit(UiMainForm_t* mainform, int x, int y, uint32_t width, uint32_t height, const char* text);

int main()
{
    int failed = app_sysinit();
    if(failed)
    {
        printf("System failed to initialize \n");
        return failed;
    }
    printf("System Initialized\n");
    //write your code after this line
    UiControlInfo_t info = {0};

    info.parent = NULL;

    info.typeId = enControlTypeIdValue_Mainform;

    info.location.X = 10;
    info.location.Y = 10;
    info.size.Height = 400;
    info.size.Width = 400;
    info.maximumSize.Height = 800;
    info.maximumSize.Width  = 800;
    info.minimumSize.Height = 300;
    info.minimumSize.Width  = 300;
    info.text = "Test MainForm";

    pcf_status_t status;

    UiMainForm_t* mainform = pcf_ui_mainform_create(&info, &status);

    if(mainform){
        int32_t exitcode;

        addTextEdit(mainform, 10, 30, 100, 20, "Editor");

        status = pcf_ui_mainform_run(mainform, &exitcode);//

        fprintf(stdout, "mainform exited with code = %d, status = %d\n", exitcode, status);
    }

    return 0;
}

void addTextEdit(UiMainForm_t* mainform, int x, int y, uint32_t width, uint32_t height, const char* text)
{
    UiControl_t* parent = pcf_ui_mainform_asControl(mainform);
    if(parent)
    {
        UiControlInfo_t info = {0};
        info.parent = parent;
        info.typeId = enControlTypeIdValue_TextEdit;
        info.location.X = x;
        info.location.Y = y;
        info.size.Height = height;
        info.size.Width = width;
        info.text = "Type something";//Shall have text "What?"
        pcf_status_t status;
        UiTextEdit_t* edit = pcf_ui_textedit_new(&info,&status);
        if(edit == NULL)
        {
            fprintf(stdout, "Failed while creating textedit control, status = %d\n", status);
            return;
        }
        else
        {
            size_t length = 0;
            status = pcf_ui_textedit_getTextLength(edit, &length);
            if(status)
            {
                fprintf(stdout, "Failed while retrieving textedit text length, status = %d\n", status);
            }
            else
            {
                size_t strlenSized = strlen(info.text);
                fprintf(stdout, "Actual length of text = %d, read length = %d\n", strlenSized, length);
                status = pcf_ui_textedit_clear(edit);
                if(status)
                {
                    fprintf(stdout, "Failed while clearing text to text-edit, status = %d\n", status);
                }
                status = pcf_ui_textedit_appendText(edit, "Appended Text!");
                if(status)
                {
                    fprintf(stdout, "Failed while appending text to text-edit, status = %d\n", status);
                }
                else
                {
                    pcf_ui_textedit_setSelectedTextRange(edit, 0, 8); //Appended will appear selected
                    int sText = -1;
                    int eText = -1;
                    status = pcf_ui_textedit_getSelectedTextRange(edit, &sText, &eText);
                    if(status)
                    {
                       fprintf(stdout, "Failed while getting selected text range, status = %d\n", status);
                    }
                    else
                    {
                        fprintf(stdout, "Selected start = %d, end = %d\n", sText, eText);
                        pcf_ui_textedit_clear(edit);
                        status = pcf_ui_textedit_insertTextAt(edit, 0, "abdef");
                        status = pcf_ui_textedit_insertTextAt(edit, 2, "c");
                        pcf_ui_textedit_setCaretPosition(edit, 3);
                        int32_t index = -1;
                        status = pcf_ui_textedit_getCaretPosition(edit, &index);
                        fprintf(stdout, "Status = %d, caret position = %d\n", status, index);
                    }
                }
            }
        }
    }
}