#include <stdio.h>
#include <app_sysinit.h>

#include <pcf/ui/ui.h>
#include <pcf/graphics/graphics.h>
#include <pcf/ui/mainform/mainform.h>
#include <pcf/ui/control/control.h>

#include <pcf/ui/trackbar/trackbar.h>

void addTrackbar(UiMainForm_t* mainform, int x, int y, uint32_t width, uint32_t height, const char* text);

int main()
{
    int failed = app_sysinit();
    if(failed)
    {
        printf("System failed to initialize \n");
        return failed;
    }
    printf("System Initialized\n");
    //write your code after this line
    UiControlInfo_t info = {0};

    info.parent = NULL;

    info.typeId = enControlTypeIdValue_Mainform;

    info.location.X = 10;
    info.location.Y = 10;
    info.size.Height = 400;
    info.size.Width = 400;
    info.maximumSize.Height = 800;
    info.maximumSize.Width  = 800;
    info.minimumSize.Height = 300;
    info.minimumSize.Width  = 300;
    info.text = "Test MainForm";

    pcf_status_t status;

    UiMainForm_t* mainform = pcf_ui_mainform_create(&info, &status);

    if(mainform){
        int32_t exitcode;

        addTrackbar(mainform, 30, 70, 100, 40, "Trackabr");

        status = pcf_ui_mainform_run(mainform, &exitcode);//

        fprintf(stdout, "mainform exited with code = %d, status = %d\n", exitcode, status);
    }

    return 0;
}

void addTrackbar(UiMainForm_t* mainform, int x, int y, uint32_t width, uint32_t height, const char* text)
{
    UiControl_t* parent = pcf_ui_mainform_asControl(mainform);
    if(parent)
    {
        UiControlInfo_t info = {0};
        info.parent = parent;
        info.typeId = enControlTypeIdValue_TextEdit;
        info.location.X = x;
        info.location.Y = y;
        info.size.Height = height;
        info.size.Width = width;
        info.text = NULL;
        pcf_status_t status;
        UiTrackbar_t* tbar = pcf_ui_trackbar_newH(&info,&status);
        if(tbar == NULL)
        {
            fprintf(stdout, "Failed while creating trakcbar control, status = %d\n", status);
            return;
        }
        pcf_ui_trackbar_setValue(tbar, 80);
        uint32_t value = 0;
        status = pcf_ui_trackbar_getValue(tbar, &value);
        if(value != 80)
        {
            fprintf(stdout, "Failed while setting or resading trakcbar control, status = %d\n", status);
            return;
        }
        UiTrackbarConfiguration_t config;
        config.tickStyle = enUiTrackTickStyle_Both;
        config.maximum = 200;
        config.minimum = 100;
        config.pageSize = 20;
        config.tickFrequency = 20;
        config.hideTicks = enBoolean_False;
        config.hideToolTips = enBoolean_False;
        pcf_ui_trackbar_configure(tbar, &config);
        pcf_ui_trackbar_setValue(tbar, 150);
    }
}