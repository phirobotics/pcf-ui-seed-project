ifeq ($(OS),Windows_NT)
  ifeq ($(shell uname -s),) # not in a bash-like shell
    CLEANUP = del /F /Q
    MKDIR = mkdir
	COPY = copy
  else # in a bash-like shell, like msys
    CLEANUP = rm -f
    MKDIR = mkdir -p
	COPY = cp
  endif
    TARGET_EXTENSION=exe
else
    CLEANUP = rm -f
    MKDIR = mkdir -p
	COPY = cp
    TARGET_EXTENSION=out
endif

.PHONY: clean
.PHONY: all
.PHONY: examples

PATH_PCF = ../../../pcf/
PATH_SRC = 
PATH_INC = ../../include/

PATH_BUILD_ROOT = build/
PATH_DEPENDS = $(PATH_BUILD)depends/
PATH_OBJ = $(PATH_BUILD)objs/

PATH_TARGET_ROOT=bin/

#Define mode
DEBUG=1

APP_NAME=trackbar

BUILD_PATHS = $(PATH_BUILD) $(PATH_OBJ) $(PATH_TARGET)

#If you have directories inside PATH_SRC this variable will need to change.
SRC = $(wildcard $(PATH_SRC)*.c)

COMPILE = gcc -c
LINK = gcc
DEPEND = gcc -MM -MG -MF
RESOURCE = windres.exe

#Standard
C_COMPILER_STD = -std=c99

#Defines
DEFS = -DPCF_GFX_CONF_USING_SHAREDLIB -DPCF_UI_CONF_USING_SHAREDLIB

#Include search directories
INC_DIRS = $(PATH_INC) $(PATH_PCF)pcf/include $(PATH_PCF)graphics/include $(PATH_PCF)ui/include

#
INCLUDES  = $(patsubst %,-I%,$(INC_DIRS))

CFLAGS = -Wall -Werror $(C_COMPILER_STD) 

CFLAGS += $(INCLUDES) $(DEFS)

ULIBDIR = $(PATH_PCF)/graphics/lib \
	$(PATH_PCF)/ui/lib

LIBDIR      =   $(patsubst %,-L%,$(ULIBDIR))
LIBS = $(PATH_PCF)graphics/lib/libpcfgraphicsdlld.a $(PATH_PCF)ui/lib/libpcfuidlld.a -lgdiplus

ifeq ($(DEBUG),1)
MODE=Debug
CFLAGS += -g -Og
DEFS += -DPCF_DEBUG
else
MODE=Release
CFLAGS += -Os
endif

PATH_BUILD = $(PATH_BUILD_ROOT)$(MODE)/
PATH_TARGET= $(PATH_TARGET_ROOT)$(MODE)/

OBJS = $(patsubst $(PATH_SRC)%.c,$(PATH_OBJ)%.o,$(SRC))
OBJS +=$(PATH_OBJ)init.o
OBJS +=$(PATH_OBJ)resources.res

all: $(BUILD_PATHS) $(PATH_TARGET)$(APP_NAME).$(TARGET_EXTENSION)
	@echo "--- Application Built --- "
	@echo "--- Copying libraries --- "
	$(COPY) $(PATH_PCF)graphics/bin/$(MODE)/*.dll $(PATH_TARGET)
	$(COPY) $(PATH_PCF)ui/bin/$(MODE)/*.dll $(PATH_TARGET)

$(PATH_TARGET)$(APP_NAME).$(TARGET_EXTENSION): $(OBJS) #$(PATH_DEPENDS)%.d
	@echo "--- Building Application ---"
	$(LINK) -o $@ $^ $(LIBDIR) $(LIBS)

$(PATH_OBJ)%.o:: $(PATH_SRC)%.c
	@echo "--- Creating .o files from .c files ---"
	$(COMPILE) $(CFLAGS) $< -o $@

$(PATH_OBJ)init.o:: ../../srcs/init.c
	$(COMPILE) $(CFLAGS) $< -o $@

$(PATH_OBJ)resources.res:: ../../resources/resources.rc
	$(RESOURCE) -J rc -O coff -i $< -o $@

$(PATH_DEPENDS)%.d:: $(PATHT)%.c
	$(DEPEND) $@ $<

$(PATH_BUILD):
	@echo "--- Creating Build Directory ---"
	$(MKDIR) $(PATH_BUILD)

$(PATH_DEPENDS):
	@echo "Creating dependency directory"	
	$(MKDIR) $(PATH_DEPENDS)

$(PATH_OBJ):
	@echo "--- Creating OBJ directory ---"
	$(MKDIR) $(PATH_OBJ)
    
$(PATH_TARGET):
	@echo "--- Creating target directory ---"
	$(MKDIR) $(PATH_TARGET)

clean:
	@echo "--- Cleaning objs and target ---"
	$(CLEANUP) $(PATH_OBJ)*.o
	$(CLEANUP) $(PATH_TARGET)*
	$(CLEANUP) -rf $(PATH_OBJ)
	$(CLEANUP) -rf $(PATH_TARGET)

.PRECIOUS: $(PATH_TARGET)%.$(TARGET_EXTENSION)
.PRECIOUS: $(PATH_DEPENDS)%.d
.PRECIOUS: $(PATH_OBJ)%.o
