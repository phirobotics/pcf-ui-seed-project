#include <stdio.h>
#include <app_sysinit.h>

#include <pcf/ui/ui.h>
#include <pcf/graphics/graphics.h>

#include <pcf/ui/mainform/mainform.h>
#include <pcf/ui/control/control.h>

#include <pcf/ui/combobox/combobox.h>

void addComboBox(UiMainForm_t* mainform, int x, int y, uint32_t width, uint32_t height);
void PCF_STDCALL comboboxIndexChangeHandler(const UiComboBox_t* comboBox, int32_t oldIndex, int32_t newIndex);

int main()
{
    int failed = app_sysinit();
    if(failed)
    {
        printf("System failed to initialize \n");
        return failed;
    }
    printf("System Initialized\n");
    
    // Your code goes here.    

    return 0;
}

