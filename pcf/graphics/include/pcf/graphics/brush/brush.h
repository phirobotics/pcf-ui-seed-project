#ifndef __PCF_GRAPHICS_BRUSH_H_INCLUDED__
#define __PCF_GRAPHICS_BRUSH_H_INCLUDED__

#include <pcf/graphics/brush/types.h>
#include <pcf/graphics/matrix/types.h>
PCF_GRAPHICS_BEGIN_CDECLARE

GraphicsBrush_t* PCF_GFX_API pcf_graphics_brush_new(const GraphicsBrushConfig_t* config, pcf_status_t* status);
pcf_status_t     PCF_GFX_API pcf_graphics_brush_delete(GraphicsBrush_t* brush);
GraphicsBrush_t* PCF_GFX_API pcf_graphics_brush_clone(const GraphicsBrush_t* other, pcf_status_t* status);

GraphicsHatchBrush_t* PCF_GFX_API pcf_graphics_brush_asHatchBrush(GraphicsBrush_t* brush, pcf_status_t* status);
GraphicsPatternBrush_t* PCF_GFX_API pcf_graphics_brush_asPatternBrush(GraphicsBrush_t* brush, pcf_status_t* status);

pcf_status_t     PCF_GFX_API pcf_graphics_brush_getType(const GraphicsBrush_t* brush, EnGraphicsBrushType_t* type);
EnBoolean_t      PCF_GFX_API pcf_graphics_brush_isHatchType(const GraphicsBrush_t* brush, pcf_status_t* status);
EnBoolean_t      PCF_GFX_API pcf_graphics_brush_isPatternType(const GraphicsBrush_t* brush, pcf_status_t* status);

pcf_status_t     PCF_GFX_API pcf_graphics_hatchBrush_getHatchStyle(const GraphicsHatchBrush_t* brush, EnGraphicsBrushHatchStyle_t* hatchStyle);
pcf_status_t     PCF_GFX_API pcf_graphics_hatchBrush_getForeColor(const GraphicsHatchBrush_t* brush, GraphicsColor_t* color);
pcf_status_t     PCF_GFX_API pcf_graphics_hatchBrush_getBackColor(const GraphicsHatchBrush_t* brush, GraphicsColor_t* color);

pcf_status_t     PCF_GFX_API pcf_graphics_patternBrush_getTransform(GraphicsPatternBrush_t* brush, GraphicsMatrix_t* matrix);
pcf_status_t     PCF_GFX_API pcf_graphics_patternBrush_setTransform(GraphicsPatternBrush_t* brush, const GraphicsMatrix_t* matrix);
pcf_status_t     PCF_GFX_API pcf_graphics_patternBrush_resetTransform(GraphicsPatternBrush_t* brush);

pcf_status_t     PCF_GFX_API pcf_graphics_patternBrush_multiplyTransform(GraphicsPatternBrush_t* brush, const GraphicsMatrix_t* matrix,
                                                                         EnGraphicsMatrixOperationOrder_t order);
pcf_status_t     PCF_GFX_API pcf_graphics_patternBrush_translateTransform(GraphicsPatternBrush_t* brush, gfx_real_t tx, gfx_real_t ty,
                                                                           EnGraphicsMatrixOperationOrder_t order);
pcf_status_t     PCF_GFX_API pcf_graphics_patternBrush_scaleTransform(GraphicsPatternBrush_t* brush, gfx_real_t sx, gfx_real_t sy,
                                                                       EnGraphicsMatrixOperationOrder_t order);
pcf_status_t     PCF_GFX_API pcf_graphics_patternBrush_rotateTransform(GraphicsPatternBrush_t* brush, gfx_real_t angleInRadians,
                                                                        EnGraphicsMatrixOperationOrder_t order);

pcf_status_t     PCF_GFX_API pcf_graphics_patternBrush_getWrapMode(GraphicsPatternBrush_t* brush, EnGraphicsWrapMode_t* mode);
pcf_status_t     PCF_GFX_API pcf_graphics_patternBrush_setWrapMode(GraphicsPatternBrush_t* brush, EnGraphicsWrapMode_t mode);

PCF_GRAPHICS_END_CDECLARE
#endif // __PCF_GRAPHICS_BRUSH_H_INCLUDED__
