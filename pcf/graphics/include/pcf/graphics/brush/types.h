#ifndef __PCF_GRAPHICS_BRUSH_TYPES_H_INCLUDED__
#define __PCF_GRAPHICS_BRUSH_TYPES_H_INCLUDED__

#include <pcf/graphics/types.h>
#include <pcf/graphics/image/types.h>

PCF_GRAPHICS_BEGIN_CDECLARE

typedef enum
{
	enGraphicsBrushHatchStyle_Horizontal = 0,
	enGraphicsBrushHatchStyle_Vertical = 1,
	enGraphicsBrushHatchStyle_ForwardDiagonal = 2,
	enGraphicsBrushHatchStyle_BackwardDiagonal = 3,
	enGraphicsBrushHatchStyle_Cross = 4,
	enGraphicsBrushHatchStyle_LargeGrid = 4,
	enGraphicsBrushHatchStyle_DiagonalCross = 5,
	enGraphicsBrushHatchStyle_05Percent = 6,
	enGraphicsBrushHatchStyle_10Percent = 7,
	enGraphicsBrushHatchStyle_20Percent = 8,
	enGraphicsBrushHatchStyle_25Percent = 9,
	enGraphicsBrushHatchStyle_30Percent = 10,
	enGraphicsBrushHatchStyle_40Percent = 11,
	enGraphicsBrushHatchStyle_50Percent = 12,
	enGraphicsBrushHatchStyle_60Percent = 13,
	enGraphicsBrushHatchStyle_70Percent = 14,
	enGraphicsBrushHatchStyle_75Percent = 15,
	enGraphicsBrushHatchStyle_80Percent = 16,
	enGraphicsBrushHatchStyle_90Percent = 17,
	enGraphicsBrushHatchStyle_LightDownwardDiagonal = 18,
	enGraphicsBrushHatchStyle_LightUpwardDiagonal = 19,
	enGraphicsBrushHatchStyle_DarkDownwardDiagonal = 20,
	enGraphicsBrushHatchStyle_DarkUpwardDiagonal = 21,
	enGraphicsBrushHatchStyle_WideDownwardDiagonal = 22,
	enGraphicsBrushHatchStyle_WideUpwardDiagonal = 23,
	enGraphicsBrushHatchStyle_LightVertical = 24,
	enGraphicsBrushHatchStyle_LightHorizontal = 25,
	enGraphicsBrushHatchStyle_NarrowVertical = 26,
	enGraphicsBrushHatchStyle_NarrowHorizontal = 27,
	enGraphicsBrushHatchStyle_DarkVertical = 28,
	enGraphicsBrushHatchStyle_DarkHorizontal = 29,
	enGraphicsBrushHatchStyle_DashedDownwardDiagonal = 30,
	enGraphicsBrushHatchStyle_DashedUpwardDiagonal = 31,
	enGraphicsBrushHatchStyle_DashedHorizontal = 32,
	enGraphicsBrushHatchStyle_DashedVertical = 33,
	enGraphicsBrushHatchStyle_SmallConfetti = 34,
	enGraphicsBrushHatchStyle_LargeConfetti = 35,
	enGraphicsBrushHatchStyle_ZigZag = 36,
	enGraphicsBrushHatchStyle_Wave = 37,
	enGraphicsBrushHatchStyle_DiagonalBrick = 38,
	enGraphicsBrushHatchStyle_HorizontalBrick = 39,
	enGraphicsBrushHatchStyle_Weave = 40,
	enGraphicsBrushHatchStyle_Plaid = 41,
	enGraphicsBrushHatchStyle_Divot = 42,
	enGraphicsBrushHatchStyle_DottedGrid = 43,
	enGraphicsBrushHatchStyle_DottedDiamond = 44,
	enGraphicsBrushHatchStyle_Shingle = 45,
	enGraphicsBrushHatchStyle_Trellis = 46,
	enGraphicsBrushHatchStyle_Sphere = 47,
	enGraphicsBrushHatchStyle_SmallGrid = 48,
	enGraphicsBrushHatchStyle_SmallCheckerBoard = 49,
	enGraphicsBrushHatchStyle_LargeCheckerBoard = 50,
	enGraphicsBrushHatchStyle_OutlinedDiamond = 51,
	enGraphicsBrushHatchStyle_SolidDiamond = 52,
	enGraphicsBrushHatchStyle_Total = 53,
	enGraphicsBrushHatchStyle_Min = enGraphicsBrushHatchStyle_Horizontal,
	enGraphicsBrushHatchStyle_Max = enGraphicsBrushHatchStyle_Total - 1
} EnGraphicsBrushHatchStyle_t;

typedef enum
{
    enGraphicsBrushType_Default,
    enGraphicsBrushType_Solid,
    enGraphicsBrushType_Hatch,
    enGraphicsBrushType_Pattern,
    enGraphicsBrushType_MaxValue = PCF_ENUM_MAXVALUE
}EnGraphicsBrushType_t;

typedef enum
{
  enGraphicsWrapMode_Tile,
  enGraphicsWrapMode_TileFlipX,
  enGraphicsWrapMode_TileFlipY,
  enGraphicsWrapMode_TileFlipXY,
  enGraphicsWrapMode_Clamp
}EnGraphicsWrapMode_t ;

typedef struct
{
    EnGraphicsBrushType_t type;
    EnGraphicsBrushHatchStyle_t hatchType;
    GraphicsColor_t foreColor;
    GraphicsColor_t backColor;
    GraphicsImage_t* patternImage;
    GraphicsRectF_t patternBrushRect;
    EnGraphicsWrapMode_t wrapMode;
}GraphicsBrushConfig_t;

typedef struct _pcf_graphics_brush_t GraphicsBrush_t;
typedef void GraphicsHatchBrush_t;
typedef void GraphicsPatternBrush_t;

PCF_GRAPHICS_END_CDECLARE

#endif // __PCF_GRAPHICS_BRUSH_TYPES_H_INCLUDED__
