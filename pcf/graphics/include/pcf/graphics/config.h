#ifndef __PCF_GRAPHICS_CONFIG_H_INCLUDED__
#define __PCF_GRAPHICS_CONFIG_H_INCLUDED__

//Remove following and include core pcf headers
#include <pcf/config/config.h>

#ifdef PCF_GFX_CONF_BUILD_STATICLIB
    #define PCF_GFX_API
    #define PCF_GFX_IS_BUILDING_SHAREDLIB 0

#elif defined(PCF_GFX_CONF_BUILD_SHAREDLIB)
    #define PCF_GFX_API PCF_EXPORT_API
    #define PCF_GFX_IS_BUILDING_SHAREDLIB 1

#elif defined(PCF_GFX_CONF_USING_SHAREDLIB)
    #define PCF_GFX_API PCF_IMPORT_API
	#define PCF_GFX_IS_BUILDING_SHAREDLIB 0

#elif defined(PCF_GFX_CONF_USING_STATICLIB)
    #define PCF_GFX_API
	#define PCF_GFX_IS_BUILDING_SHAREDLIB 0

#else
#error "Define one of the above 4 macros."
#endif // PCF_GFX_CONF_BUILD_STATICLIB

#endif // __PCF_GRAPHICS_CONFIG_H_INCLUDED__
