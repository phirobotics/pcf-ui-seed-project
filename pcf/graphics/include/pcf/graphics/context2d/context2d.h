#ifndef __PCF_GRAPHICS_CONTEXT2D_H_INCLUDED__
#define __PCF_GRAPHICS_CONTEXT2D_H_INCLUDED__

#include <pcf/graphics/context2d/types.h>
#include <pcf/graphics/matrix/types.h>
#include <pcf/graphics/region/types.h>
#include <pcf/graphics/font/types.h>

PCF_GRAPHICS_BEGIN_CDECLARE

GraphicsContext2D_t* PCF_GFX_API pcf_graphics_context2D_new(const GraphicsContext2DInfo_t* info, pcf_status_t* status);
GraphicsContext2D_t* PCF_GFX_API pcf_graphics_context2D_newFromImage(GraphicsImage_t* image, pcf_status_t* status);

pcf_status_t PCF_GFX_API pcf_graphics_context2D_delete(GraphicsContext2D_t* context);

EnBoolean_t  PCF_GFX_API pcf_graphics_context2D_isValid(GraphicsContext2D_t* context, pcf_status_t* status);
pcf_status_t PCF_GFX_API pcf_graphics_context2D_clear(GraphicsContext2D_t* context, GraphicsColor_t* clearColor);

pcf_status_t PCF_GFX_API pcf_graphics_context2D_measureFontHeight(const GraphicsContext2D_t* context, const GraphicsFont_t* font, gfx_real_t* height);
pcf_status_t PCF_GFX_API pcf_graphics_context2D_getNearestColor(const GraphicsContext2D_t *context, GraphicsColor_t* argb);

pcf_status_t PCF_GFX_API pcf_graphics_context2D_setCompositionMode(GraphicsContext2D_t* context,
                                                                   EnGraphicsColorComopositionMode_t mode);

pcf_status_t PCF_GFX_API pcf_graphics_context2D_getCompositionMode(const GraphicsContext2D_t* context,
                                                                   EnGraphicsColorComopositionMode_t* mode);

pcf_status_t PCF_GFX_API pcf_graphics_context2D_setSmoothingMode(GraphicsContext2D_t* context,
                                                                 EnGraphicsContextSmoothingMode_t mode);
pcf_status_t PCF_GFX_API pcf_graphics_context2D_getSmoothingMode(const GraphicsContext2D_t* context,
                                                                 EnGraphicsContextSmoothingMode_t* mode);

pcf_status_t PCF_GFX_API pcf_graphics_context2D_setInterpolationMode(GraphicsContext2D_t* context,
                                                                     EnGraphicsInterpolationMode_t mode);
pcf_status_t PCF_GFX_API pcf_graphics_context2D_getInterpolationMode(const GraphicsContext2D_t* context,
                                                                     EnGraphicsInterpolationMode_t* mode);

pcf_status_t PCF_GFX_API pcf_graphics_context2D_setClipRect(GraphicsContext2D_t* context, gfx_int_t x, gfx_int_t y,
                                                            gfx_int_t width, gfx_int_t height, EnGraphicsCombineMode_t mode);
pcf_status_t PCF_GFX_API pcf_graphics_context2D_getClipBounds(GraphicsContext2D_t* context, GraphicsRect_t* rect);
pcf_status_t PCF_GFX_API pcf_graphics_context2D_setClipRectF(GraphicsContext2D_t* context, gfx_real_t x, gfx_real_t y,
                                                            gfx_real_t width, gfx_real_t height, EnGraphicsCombineMode_t mode);
pcf_status_t PCF_GFX_API pcf_graphics_context2D_getClipBoundsF(GraphicsContext2D_t* context, GraphicsRectF_t* rect);

pcf_status_t PCF_GFX_API pcf_graphics_context2D_translateClip(GraphicsContext2D_t* context, gfx_int_t tx, gfx_int_t ty);
pcf_status_t PCF_GFX_API pcf_graphics_context2D_translateClipF(GraphicsContext2D_t* context, gfx_real_t tx, gfx_real_t ty);

EnBoolean_t  PCF_GFX_API pcf_graphics_context2D_isClipEmpty(GraphicsContext2D_t* context, pcf_status_t* status);
EnBoolean_t  PCF_GFX_API pcf_graphics_context2D_isPointVisible(GraphicsContext2D_t* context, gfx_int_t x, gfx_int_t y,
                                                               pcf_status_t* status);
EnBoolean_t  PCF_GFX_API pcf_graphics_context2D_isPointVisibleF(GraphicsContext2D_t* context, gfx_real_t x, gfx_real_t y,
                                                               pcf_status_t* status);
EnBoolean_t  PCF_GFX_API pcf_graphics_context2D_isRectVisible(GraphicsContext2D_t* context, gfx_int_t x, gfx_int_t y,
                                                               gfx_int_t width, gfx_int_t height, pcf_status_t* status);
EnBoolean_t  PCF_GFX_API pcf_graphics_context2D_isRectVisibleF(GraphicsContext2D_t* context, gfx_real_t x, gfx_real_t y,
                                                               gfx_real_t width, gfx_real_t height, pcf_status_t* status);

pcf_status_t PCF_GFX_API pcf_graphics_context2D_pushState(GraphicsContext2D_t* context, Context2DState_t* state);
pcf_status_t PCF_GFX_API pcf_graphics_context2D_popState(GraphicsContext2D_t* context, Context2DState_t state);
//ToDo: Add get/set for new section

pcf_status_t PCF_GFX_API pcf_graphics_context2D_resetTransform(GraphicsContext2D_t* context);
pcf_status_t PCF_GFX_API pcf_graphics_context2D_setTransform(GraphicsContext2D_t* context, GraphicsMatrix_t* transform);
GraphicsMatrix_t* PCF_GFX_API pcf_graphics_context2D_getTransform(const GraphicsContext2D_t* context, pcf_status_t* status);

pcf_status_t PCF_GFX_API pcf_graphics_context2D_translateTransformF(const GraphicsContext2D_t* context,
                                                                   gfx_real_t tx, gfx_real_t ty,
                                                                   EnGraphicsMatrixOperationOrder_t order);
pcf_status_t PCF_GFX_API pcf_graphics_context2D_scaleTransformF(const GraphicsContext2D_t* context,
                                                                   gfx_real_t sx, gfx_real_t sy,
                                                                   EnGraphicsMatrixOperationOrder_t order);
pcf_status_t PCF_GFX_API pcf_graphics_context2D_rotateTransformF(const GraphicsContext2D_t* context,
                                                                   gfx_real_t angleInRadians,
                                                                   EnGraphicsMatrixOperationOrder_t order);

pcf_status_t PCF_GFX_API pcf_graphics_context2D_multiplyTransformF(const GraphicsContext2D_t* context,
                                                                  GraphicsMatrix_t* transform,
                                                                  EnGraphicsMatrixOperationOrder_t order);

pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawLineF(GraphicsContext2D_t* context, const GraphicsPen_t* pen,
                                                                   gfx_real_t x1, gfx_real_t y1,
                                                                   gfx_real_t x2, gfx_real_t y2);
pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawLine(GraphicsContext2D_t* context, const GraphicsPen_t* pen,
                                                                   gfx_int_t x1, gfx_int_t y1,
                                                                   gfx_int_t x2, gfx_int_t y2);
pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawLinesF(GraphicsContext2D_t* context, const GraphicsPen_t* pen,
                                                                   const GraphicsPointF_t* points, size_t count);
pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawLines(GraphicsContext2D_t* context, const GraphicsPen_t* pen,
                                                                   const GraphicsPoint_t* points, size_t count);

pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawArcF(GraphicsContext2D_t* context, const GraphicsPen_t* pen,
                                                                   gfx_real_t x, gfx_real_t y,
                                                                   gfx_real_t width, gfx_real_t height,
                                                                   gfx_real_t startAngleInRadians,
                                                                   gfx_real_t sweepAngleInRadians);
pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawArc(GraphicsContext2D_t* context, const GraphicsPen_t* pen,
                                                                   gfx_int_t x, gfx_int_t y,
                                                                   gfx_int_t width, gfx_int_t height,
                                                                   gfx_real_t startAngleInRadians,
                                                                   gfx_real_t sweepAngleInRadians);

pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawBezierF(GraphicsContext2D_t* context, const GraphicsPen_t* pen,
                                                            gfx_real_t x1, gfx_real_t y1, gfx_real_t x2, gfx_real_t y2,
                                                            gfx_real_t x3, gfx_real_t y3, gfx_real_t x4, gfx_real_t y4);

pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawBezier(GraphicsContext2D_t* context, const GraphicsPen_t* pen,
                                                           gfx_int_t x1, gfx_int_t y1, gfx_int_t x2, gfx_int_t y2,
                                                           gfx_int_t x3, gfx_int_t y3, gfx_int_t x4, gfx_int_t y4);

pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawBeziersF(GraphicsContext2D_t* context, const GraphicsPen_t* pen,
                                                             const GraphicsPointF_t* points, size_t count);

pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawBeziers(GraphicsContext2D_t* context, const GraphicsPen_t* pen,
                                                            const GraphicsPoint_t* points, size_t count);

pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawRectangle(GraphicsContext2D_t* context, const GraphicsPen_t* pen,
                                                              gfx_int_t x, gfx_int_t y, gfx_int_t width, gfx_int_t height);
pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawRectangleF(GraphicsContext2D_t* context, const GraphicsPen_t* pen,
                                                              gfx_real_t x, gfx_real_t y, gfx_real_t width, gfx_real_t height);
pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawRectangles(GraphicsContext2D_t* context, const GraphicsPen_t* pen,
                                                              const GraphicsRect_t* rectangles, size_t count);
pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawRectanglesF(GraphicsContext2D_t* context, const GraphicsPen_t* pen,
                                                              const GraphicsRectF_t* rectangles, size_t count);

pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawEllise(GraphicsContext2D_t* context, const GraphicsPen_t* pen,
                                                           gfx_int_t x, gfx_int_t y, gfx_int_t width, gfx_int_t height);
pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawEllipseF(GraphicsContext2D_t* context, const GraphicsPen_t* pen,
                                                           gfx_real_t x, gfx_real_t y, gfx_real_t width, gfx_real_t height);

pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawPie(GraphicsContext2D_t* context, const GraphicsPen_t* pen,
                                                        gfx_int_t x, gfx_int_t y, gfx_int_t width, gfx_int_t height,
                                                        gfx_real_t startAngleInRadians, gfx_real_t sweepAngleInRadians);
pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawPieF(GraphicsContext2D_t* context, const GraphicsPen_t* pen,
                                                        gfx_real_t x, gfx_real_t y, gfx_real_t width, gfx_real_t height,
                                                        gfx_real_t startAngleInRadians, gfx_real_t sweepAngleInRadians);

pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawPolygon(GraphicsContext2D_t* context, const GraphicsPen_t* pen,
                                                            const GraphicsPoint_t* points, size_t count);
pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawPolygonF(GraphicsContext2D_t* context, const GraphicsPen_t* pen,
                                                            const GraphicsPointF_t* points, size_t count);

pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawFillRectangle(GraphicsContext2D_t* context, const GraphicsBrush_t* brush,
                                                                  gfx_int_t x, gfx_int_t y, gfx_int_t width, gfx_int_t height);
pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawFillRectangleF(GraphicsContext2D_t* context, const GraphicsBrush_t* brush,
                                                                   gfx_real_t x, gfx_real_t y, gfx_real_t width, gfx_real_t height);
pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawFillRectangles(GraphicsContext2D_t* context, const GraphicsBrush_t* brush,
                                                                   const GraphicsRect_t* rectangles, size_t count);
pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawFillRectanglesF(GraphicsContext2D_t* context, const GraphicsBrush_t* brush,
                                                                    const GraphicsRectF_t* rectangles, size_t count);

pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawFillPolygon(GraphicsContext2D_t* context, const GraphicsBrush_t* brush,
                                                                EnGraphicsFillMode_t fillMode, const GraphicsPoint_t* points,
                                                                size_t count);
pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawFillPolygonF(GraphicsContext2D_t* context, const GraphicsBrush_t* brush,
                                                                EnGraphicsFillMode_t fillMode, const GraphicsPointF_t* points,
                                                                size_t count);
pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawFillPolygon1(GraphicsContext2D_t* context, const GraphicsBrush_t* brush,
                                                                 const GraphicsPoint_t* points, size_t count);
pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawFillPolygonF1(GraphicsContext2D_t* context, const GraphicsBrush_t* brush,
                                                                  const GraphicsPointF_t* points, size_t count);

pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawFillEllise(GraphicsContext2D_t* context, const GraphicsBrush_t* brush,
                                                               gfx_int_t x, gfx_int_t y, gfx_int_t width, gfx_int_t height);
pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawFillEllipseF(GraphicsContext2D_t* context, const GraphicsBrush_t* brush,
                                                                 gfx_real_t x, gfx_real_t y, gfx_real_t width, gfx_real_t height);

pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawFillPie(GraphicsContext2D_t* context, const GraphicsBrush_t* brush,
                                                            gfx_int_t x, gfx_int_t y, gfx_int_t width, gfx_int_t height,
                                                            gfx_real_t startAngleInRadians, gfx_real_t sweepAngleInRadians);
pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawFillPieF(GraphicsContext2D_t* context, const GraphicsBrush_t* brush,
                                                             gfx_real_t x, gfx_real_t y, gfx_real_t width, gfx_real_t height,
                                                             gfx_real_t startAngleInRadians, gfx_real_t sweepAngleInRadians);

pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawFillPath(GraphicsContext2D_t* context, const GraphicsBrush_t* brush,
                                                             GraphicsPath_t* path);

pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawRegion(GraphicsContext2D_t* context, const GraphicsBrush_t* brush,
                                                           GraphicsRegion_t* region);

pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawImageAt(GraphicsContext2D_t* context, const GraphicsImage_t* image,
                                                            gfx_int_t x, gfx_int_t y);
pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawImageAtF(GraphicsContext2D_t* context, const GraphicsImage_t* image,
                                                             gfx_real_t x, gfx_real_t y);
pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawImageIn(GraphicsContext2D_t* context, const GraphicsImage_t* image,
                                                            gfx_int_t x, gfx_int_t y, gfx_int_t width, gfx_int_t height);
pcf_status_t PCF_GFX_API pcf_graphics_context2D_drawImageInF(GraphicsContext2D_t* context, const GraphicsImage_t* image,
                                                             gfx_real_t x, gfx_real_t y, gfx_real_t width, gfx_real_t height);
PCF_GRAPHICS_BEGIN_CDECLARE
#endif // __PCF_GRAPHICS_CONTEXT2D_H_INCLUDED__
