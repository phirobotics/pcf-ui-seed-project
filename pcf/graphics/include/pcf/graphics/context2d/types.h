#ifndef __PCF_GRAPHICS_CONTEXT2D_TYPES_H_INCLUDED__
#define __PCF_GRAPHICS_CONTEXT2D_TYPES_H_INCLUDED__

#include <pcf/graphics/types.h>
#include <pcf/graphics/brush/types.h>
#include <pcf/graphics/image/types.h>
#include <pcf/graphics/matrix/types.h>
#include <pcf/graphics/pen/types.h>
#include <pcf/graphics/path/types.h>

PCF_GRAPHICS_BEGIN_CDECLARE

typedef enum
{
    enGraphicsContextSmoothingMode_Default,
    enGraphicsContextSmoothingMode_HighSpeed,
    enGraphicsContextSmoothingMode_HighQuality,
    enGraphicsContextSmoothingMode_HighestQuality
}EnGraphicsContextSmoothingMode_t;

typedef enum
{
    enGraphicsColorComopositionMode_Default,
    enGraphicsColorComopositionMode_SourceOver,
    enGraphicsColorComopositionMode_SourceCopy
}EnGraphicsColorComopositionMode_t;

typedef enum
{
  enGraphicsInterpolationMode_Invalid,
  enGraphicsInterpolationMode_Default,
  enGraphicsInterpolationMode_LowQuality,
  enGraphicsInterpolationMode_HighQuality,
  enGraphicsInterpolationMode_Bilinear,
  enGraphicsInterpolationMode_Bicubic,
  enGraphicsInterpolationMode_NearestNeighbor,
  enGraphicsInterpolationMode_HighQualityBilinear,
  enGraphicsInterpolationMode_HighQualityBicubic
}EnGraphicsInterpolationMode_t;

typedef enum
{
  enGraphicsCombineMode_Replace = 0,
  enGraphicsCombineMode_Intersect = 1,
  enGraphicsCombineMode_Union = 2,
  enGraphicsCombineMode_Xor = 3,
  enGraphicsCombineMode_Exclude = 4,
  enGraphicsCombineMode_Complement = 5
}EnGraphicsCombineMode_t;

PCF_GRAPHICS_BEGIN_CDECLARE
#endif // __PCF_GRAPHICS_CONTEXT2D_TYPES_H_INCLUDED__
