#ifndef __PCF_GRAPHICS_DEFS_H_INCLUDED__
#define __PCF_GRAPHICS_DEFS_H_INCLUDED__

#include <pcf/graphics/config.h>

#ifdef PCF_GFX_CONF_ENABLE_DEBUG
#define PCF_GFX_ENABLE_DEBUG 1
#else
#define PCF_GFX_ENABLE_DEBUG 0
#endif // PCF_GFX_CONF_ENABLE_DEBUG

#ifndef PCF_GFX_API
#error "Required macro PCF_GFX_API is missing."
#endif // PCF_GFX_API

#ifndef PCF_GFX_IS_BUILDING_SHAREDLIB
#error "Required macro PCF_GFX_IS_BUILDING_SHAREDLIB is missing."
#endif // PCF_GFX_IS_BUILDING_SHAREDLIB

#ifndef PCF_BEGIN_CDECLARE
#error "Required macro PCF_BEGIN_CDECLARE is missing."
#endif // PCF_BEGIN_CDECLARE

#ifndef PCF_END_CDECLARE
#error "Required macro PCF_END_CDECLARE is missing."
#endif // PCF_END_CDECLARE

#ifndef PCF_FILE
#error "Required macro PCF_FILE is missing."
#endif // PCF_FILE

#ifndef PCF_LINE
#error "Required macro PCF_LINE is missing."
#endif // PCF_LINE

#ifndef PCF_FUNC
#error "Required macro PCF_FUNC is missing."
#endif // PCF_FUNC

#ifndef PCF_ENUM_SIZE
#error "Required macro PCF_ENUM_SIZE is missing."
#endif // PCF_ENUM_SIZE

#ifndef PCF_ENUM_MAXVALUE
#error "Required macro PCF_ENUM_MAXVALUE is missing."
#endif // PCF_ENUM_MAXVALUE

#ifndef PCF_CDECL
#error "Required macro PCF_CDECL is missing."
#endif // PCF_CDECL

#ifndef PCF_STDCALL
#error "Required macro PCF_STDCALL is missing."
#endif // PCF_STDCALL

#ifndef PCF_INLINE
#error "Required macro PCF_INLINE is missing."
#endif // PCF_INLINE

#ifndef PCF_PATH_TEXT_SIZE
#error "Required macro PCF_PATH_TEXT_SIZE is missing."
#endif // PCF_PATH_TEXT_SIZE

#ifndef PCF_PROPERTY_TEXT_SIZE
#error "Required macro PCF_PROPERTY_TEXT_SIZE is missing."
#endif // PCF_PROPERTY_TEXT_SIZE

#define PCF_GRAPHICS_BEGIN_CDECLARE  PCF_BEGIN_CDECLARE
#define PCF_GRAPHICS_END_CDECLARE    PCF_END_CDECLARE



/***********  PCF_GRAPHICS_SPECIFIC_DEFINES ************/

#ifdef PCF_GFX_SHALL_DEFINE_PRIMITIVES
#error "Pcf Graphics reserved macro PCF_GFX_SHALL_DEFINE_PRIMITIVES is pre-defined."
#endif // PCF_GFX_SHALL_DEFINE_PRIMITIVES

#if defined(PCF_DEFINES_CORE_GRAPHICS_ELEMENTS) && (PCF_DEFINES_CORE_GRAPHICS_ELEMENTS != 0)
#define PCF_GFX_SHALL_DEFINE_PRIMITIVES 0
#else
#define PCF_GFX_SHALL_DEFINE_PRIMITIVES 1
#endif // defined(PCF_DEFINES_CORE_GRAPHICS_ELEMENTS) && (PCF_DEFINES_CORE_GRAPHICS_ELEMENTS == 1)

#define PCF_GFX_VER(platform, major, minor)  \
(uint32_t)(((platform) << 16) | ((major) << 8) | ((minor) << 0))


#ifdef PCF_GFX_PLATFORM_NONE
#error "Pcf Graphics reserved macro PCF_GFX_PLATFORM_NONE is pre-defined."
#endif // PCF_GFX_PLATFORM_NONE

#define PCF_GFX_PLATFORM_NONE             0

#ifdef PCF_GFX_PLATFORM_WIN32_GDI
#error "Pcf Graphics reserved macro PCF_GFX_PLATFORM_WIN32_GDI is \
pre-defined."
#endif // PCF_GFX_PLATFORM_WIN32_GDI

#define PCF_GFX_PLATFORM_WIN32_GDI        1

#ifdef PCF_GFX_PLATFORM_WIN32_GDIPLUS
#error "Pcf Graphics reserved macro PCF_GFX_PLATFORM_WIN32_GDIPLUS \
 is pre-defined."
#endif // PCF_GFX_PLATFORM_WIN32_GDIPLUS

#define PCF_GFX_PLATFORM_WIN32_GDIPLUS    2

#ifdef PCF_GFX_PLATFORM_WIN32_DDRAW
#error "Pcf Graphics reserved macro PCF_GFX_PLATFORM_WIN32_DDRAW is \
 pre-defined."
#endif // PCF_GFX_PLATFORM_WIN32_DDRAW

#define PCF_GFX_PLATFORM_WIN32_DDRAW      3

#ifdef PCF_GFX_PLATFORM_WIN32_DIRECTX10
#error "Pcf Graphics reserved macro PCF_GFX_PLATFORM_WIN32_DIRECTX10 \
is pre-defined."
#endif // PCF_GFX_PLATFORM_WIN32_DIRECTX10

#define PCF_GFX_PLATFORM_WIN32_DIRECTX10  4

#ifdef PCF_GFX_PLATFORM_WIN32_DIRECTX11
#error "Pcf Graphics reserved macro PCF_GFX_PLATFORM_WIN32_DIRECTX11 \
is pre-defined."
#endif // PCF_GFX_PLATFORM_WIN32_DIRECTX11

#define PCF_GFX_PLATFORM_WIN32_DIRECTX11  5

#ifdef PCF_GFX_PLATFORM_OPENGL
#error "Pcf Graphics reserved macro PCF_GFX_PLATFORM_OPENGL is \
pre-defined."
#endif // PCF_GFX_PLATFORM_OPENGL

#define PCF_GFX_PLATFORM_OPENGL           6

#ifdef PCF_GFX_PLATFORM_OPENGL_MODERN
#error "Pcf Graphics reserved macro PCF_GFX_PLATFORM_OPENGL_MODERN is \
pre-defined."
#endif // PCF_GFX_PLATFORM_OPENGL_MODERN

#define PCF_GFX_PLATFORM_OPENGL_MODERN    7

#ifdef PCF_GFX_PLATFORM_RAWDISPLAY
#error "Pcf Graphics reserved macro PCF_GFX_PLATFORM_RAWDISPLAY is \
pre-defined."
#endif // PCF_GFX_PLATFORM_RAWDISPLAY

#define PCF_GFX_PLATFORM_RAWDISPLAY       8

#ifdef PCF_GFX_PROPERTY_TEXT_SIZE
#error "Pcf Graphics reserved macro PCF_GFX_PROPERTY_TEXT_SIZE is \
pre-defined."
#endif // PCF_GFX_PLATFORM_RAWDISPLAY

#if   (PCF_PROPERTY_TEXT_SIZE < 17)
#define PCF_GFX_PROPERTY_TEXT_SIZE 16
#elif (PCF_PROPERTY_TEXT_SIZE < 33)
#define PCF_GFX_PROPERTY_TEXT_SIZE 32
#elif (PCF_PROPERTY_TEXT_SIZE < 65)
#define PCF_GFX_PROPERTY_TEXT_SIZE 64
#elif (PCF_PROPERTY_TEXT_SIZE < 129)
#define PCF_GFX_PROPERTY_TEXT_SIZE 128
#else
#define PCF_GFX_PROPERTY_TEXT_SIZE 256
#endif

#ifdef PCF_GFX_StatusCode_None
#error "Don't define the macro PCF_GFX_StatusCode_None on your own, \
it is reserved."
#endif // PCF_GFX_StatusCode_None
#define PCF_GFX_StatusCode_None                   (0)

#ifdef PCF_GFX_StatusCode_Error
#error "Don't define the macro PCF_GFX_StatusCode_Error on your own, \
it is reserved."
#endif // PCF_GFX_StatusCode_Error
#define PCF_GFX_StatusCode_Error        (PCF_GFX_StatusCode_None -1)

#ifdef PCF_GFX_StatusCode_InvalidArg
#error "Don't define the macro PCF_GFX_StatusCode_InvalidArg on your \
own, it is reserved."
#endif // PCF_GFX_StatusCode_InvalidArg
#define PCF_GFX_StatusCode_InvalidArg   (PCF_GFX_StatusCode_Error - 1)

#ifdef PCF_GFX_StatusCode_NullArg
#error "Don't define the macro PCF_GFX_StatusCode_NullArg on your own, \
 it is reserved."
#endif // PCF_GFX_StatusCode_NullArg
#define PCF_GFX_StatusCode_NullArg    (PCF_GFX_StatusCode_InvalidArg - 1)

#ifdef PCF_GFX_StatusCode_NotImplemented
#error "Don't define the macro PCF_GFX_StatusCode_NotImplemented on \
your own, it is reserved."
#endif // PCF_GFX_StatusCode_NotImplemented
#define PCF_GFX_StatusCode_NotImplemented  (PCF_GFX_StatusCode_NullArg - 1)

#ifdef PCF_GFX_StatusCode_NotSupported
#error "Don't define the macro PCF_GFX_StatusCode_NotSupported on your \
 own, it is reserved."
#endif // PCF_GFX_StatusCode_NotSupported
#define PCF_GFX_StatusCode_NotSupported       \
    (PCF_GFX_StatusCode_NotImplemented - 1)

#ifdef PCF_GFX_StatusCode_NotEnoughMemory
#error "Don't define the macro PCF_GFX_StatusCode_NotEnoughMemory on your \
 own, it is reserved."
#endif // PCF_GFX_StatusCode_NotEnoughMemory
#define PCF_GFX_StatusCode_NotEnoughMemory     \
   (PCF_GFX_StatusCode_NotSupported - 1)

#ifdef PCF_GFX_StatusCode_AllocationError
#error "Don't define the macro PCF_GFX_StatusCode_AllocationError on \
 your own, it is reserved."
#endif // PCF_GFX_StatusCode_AllocationError
#define PCF_GFX_StatusCode_AllocationError     \
   (PCF_GFX_StatusCode_NotEnoughMemory - 1)

#ifdef PCF_GFX_StatusCode_InvalidCapacity
#error "Don't define the macro PCF_GFX_StatusCode_InvalidCapacity on \
your own, it is reserved."
#endif // PCF_GFX_StatusCode_InvalidCapacity
#define PCF_GFX_StatusCode_InvalidCapacity      \
  (PCF_GFX_StatusCode_AllocationError - 1)

#ifdef PCF_GFX_StatusCode_InvalidRange
#error "Don't define the macro PCF_GFX_StatusCode_InvalidRange on your \
 own, it is reserved."
#endif // PCF_GFX_StatusCode_InvalidRange
#define PCF_GFX_StatusCode_InvalidRange        \
   (PCF_GFX_StatusCode_InvalidCapacity - 1)

#ifdef PCF_GFX_StatusCode_MaxCapacity
#error "Don't define the macro PCF_GFX_StatusCode_MaxCapacity on your \
 own, it is reserved."
#endif // PCF_GFX_StatusCode_MaxCapacity
#define PCF_GFX_StatusCode_MaxCapacity         \
    (PCF_GFX_StatusCode_InvalidRange - 1)

#ifdef PCF_GFX_StatusCode_ResourceNotFound
#error "Don't define the macro PCF_GFX_StatusCode_ResourceNotFound \
on your own, it is reserved."
#endif // PCF_GFX_StatusCode_ResourceNotFound
#define PCF_GFX_StatusCode_ResourceNotFound    \
        (PCF_GFX_StatusCode_MaxCapacity - 1)

#ifdef PCF_GFX_StatusCode_IndexOutOfRange
#error "Don't define the macro PCF_GFX_StatusCode_IndexOutOfRange \
on your own, it is reserved."
#endif // PCF_GFX_StatusCode_IndexOutOfRange
#define PCF_GFX_StatusCode_IndexOutOfRange     \
   (PCF_GFX_StatusCode_ResourceNotFound - 1)

#ifdef PCF_GFX_StatusCode_InvalidOperation
#error "Don't define the macro PCF_GFX_StatusCode_InvalidOperation \
 on your own, it is reserved."
#endif // PCF_GFX_StatusCode_InvalidOperation
#define PCF_GFX_StatusCode_InvalidOperation    \
   (PCF_GFX_StatusCode_IndexOutOfRange - 1)

#ifdef PCF_GFX_StatusCode_FileDoesntExist
#error "Don't define the macro PCF_GFX_StatusCode_FileDoesntExist \
on your own, it is reserved."
#endif // PCF_GFX_StatusCode_FileDoesntExist
#define PCF_GFX_StatusCode_FileDoesntExist     \
   (PCF_GFX_StatusCode_InvalidOperation - 1)

#ifdef PCF_GFX_StatusCode_FileFormatNotSupported
#error "Don't define the macro PCF_GFX_StatusCode_FileFormatNotSupported \
on your own, it is reserved."
#endif // PCF_GFX_StatusCode_FileFormatNotSupported
#define PCF_GFX_StatusCode_FileFormatNotSupported \
    (PCF_GFX_StatusCode_FileDoesntExist - 1)

#ifdef PCF_GFX_StatusCode_InvalidFileFormat
#error "Don't define the macro PCF_GFX_StatusCode_InvalidFileFormat on \
 your own, it is reserved."
#endif // PCF_GFX_StatusCode_InvalidFileFormat
#define PCF_GFX_StatusCode_InvalidFileFormat     \
 (PCF_GFX_StatusCode_FileFormatNotSupported - 1)

#ifdef PCF_GFX_StatusCode_InternalError
#error "Don't define the macro PCF_GFX_StatusCode_InternalError on your \
own, it is reserved."
#endif // PCF_GFX_StatusCode_InternalError
#define PCF_GFX_StatusCode_InternalError         \
  (PCF_GFX_StatusCode_InvalidFileFormat - 1)

#ifdef PCF_GFX_StatusCode_NotAllowed
#error "Don't define the macro PCF_GFX_StatusCode_NotAllowed on your own, \
 it is reserved."
#endif // PCF_GFX_StatusCode_NotAllowed
#define PCF_GFX_StatusCode_NotAllowed            \
  (PCF_GFX_StatusCode_InternalError - 1)

#ifdef PCF_GFX_StatusCode_InvalidState
#error "Don't define the macro PCF_GFX_StatusCode_InvalidState on your \
own, it is reserved."
#endif // PCF_GFX_StatusCode_InvalidState
#define PCF_GFX_StatusCode_InvalidState         \
   (PCF_GFX_StatusCode_NotAllowed - 1)

#ifdef PCF_GFX_StatusCode_Count
#error "Don't define the macro PCF_GFX_StatusCode_Count on your own, \
it is reserved."
#endif // PCF_GFX_StatusCode_Count
#define PCF_GFX_StatusCode_Count               \
     (-(PCF_GFX_StatusCode_InvalidState))

#ifdef PCF_GFX_StatusCode_EndBoundary
#error "Don't define the macro PCF_GFX_StatusCode_EndBoundary on your own, \
 it is reserved."
#endif // PCF_GFX_StatusCode_EndBoundary
#define PCF_GFX_StatusCode_EndBoundary        \
     (PCF_GFX_StatusCode_InvalidState - 1000)


#if defined(PCF_GFX_COLOR_BLACK)
#error "PCF_GFX_COLOR_BLACK is reserved macro don't define it anywhere \
else."
#endif // PCF_GFX_COLOR_BLACK 0
#define PCF_GFX_COLOR_BLACK          ((uint32_t)(0x00000000))

#if defined(PCF_GFX_COLOR_DARKRED)
#error "PCF_GFX_COLOR_DARKRED is reserved macro don't define it anywhere \
 else."
#endif // PCF_GFX_COLOR_DARKRED 1
#define PCF_GFX_COLOR_DARKRED        ((uint32_t)(0x00000080))

#if defined(PCF_GFX_COLOR_DARKGREEN)
#error "PCF_GFX_COLOR_DARKGREEN is reserved macro don't define it anywhere \
 else."
#endif // PCF_GFX_COLOR_DARKGREEN 2
#define PCF_GFX_COLOR_DARKGREEN      ((uint32_t)(0x00008000))

#if defined(PCF_GFX_COLOR_DARKBLUE)
#error "PCF_GFX_COLOR_DARKBLUE is reserved macro don't define it anywhere \
else."
#endif // PCF_GFX_COLOR_DARKBLUE 3
#define PCF_GFX_COLOR_DARKBLUE       ((uint32_t)(0x00800000))

#if defined(PCF_GFX_COLOR_DARKYELLOW)
#error "PCF_GFX_COLOR_DARKYELLOW is reserved macro don't define it \
 anywhere else."
#endif // PCF_GFX_COLOR_DARKYELLOW 4
#define PCF_GFX_COLOR_DARKYELLOW     ((uint32_t)(0x00008080))

#if defined(PCF_GFX_COLOR_DARKMAGENTA)
#error "PCF_GFX_COLOR_DARKMAGENTA is reserved macro don't define it \
 anywhere else."
#endif // PCF_GFX_COLOR_DARKMAGENTA 5
#define PCF_GFX_COLOR_DARKMAGENTA    ((uint32_t)(0x00800080))

#if defined(PCF_GFX_COLOR_DARKCYAN)
#error "PCF_GFX_COLOR_DARKCYAN is reserved macro don't define it \
anywhere else."
#endif // PCF_GFX_COLOR_DARKCYAN 6
#define PCF_GFX_COLOR_DARKCYAN       ((uint32_t)(0x00808000))

#if defined(PCF_GFX_COLOR_LIGHTGRAY)
#error "PCF_GFX_COLOR_LIGHTGRAY is reserved macro don't define it \
 anywhere else."
#endif // PCF_GFX_COLOR_LIGHTGRAY 7
#define PCF_GFX_COLOR_LIGHTGRAY      ((uint32_t)(0x00C0C0C0))

#if defined(PCF_GFX_COLOR_MONEYGREEN)
#error "PCF_GFX_COLOR_MONEYGREEN is reserved macro don't define it \
 anywhere else."
#endif // PCF_GFX_COLOR_MONEYGREEN 8
#define PCF_GFX_COLOR_MONEYGREEN     ((uint32_t)(0x00C0DCC0))

#if defined(PCF_GFX_COLOR_SKYBLUE)
#error "PCF_GFX_COLOR_SKYBLUE is reserved macro don't define it \
anywhere else."
#endif // PCF_GFX_COLOR_SKYBLUE 9
#define PCF_GFX_COLOR_SKYBLUE        ((uint32_t)(0x00F0CAA6))

#if defined(PCF_GFX_COLOR_CREAM)
#error "PCF_GFX_COLOR_CREAM is reserved macro don't define it \
anywhere else."
#endif // PCF_GFX_COLOR_CREAM 10
#define PCF_GFX_COLOR_CREAM          ((uint32_t)(0x00F0FBFF))

#if defined(PCF_GFX_COLOR_MEDIUMGRAY)
#error "PCF_GFX_COLOR_MEDIUMGRAY is reserved macro don't define it \
anywhere else."
#endif // PCF_GFX_COLOR_MEDIUMGRAY 11
#define PCF_GFX_COLOR_MEDIUMGRAY     ((uint32_t)(0x00A4A0A0))

#if defined(PCF_GFX_COLOR_DARKGRAY)
#error "PCF_GFX_COLOR_DARKGRAY is reserved macro don't define it \
anywhere else."
#endif // PCF_GFX_COLOR_DARKGRAY 12
#define PCF_GFX_COLOR_DARKGRAY       ((uint32_t)(0x00808080))

#if defined(PCF_GFX_COLOR_RED)
#error "PCF_GFX_COLOR_RED is reserved macro don't define it anywhere \
 else."
#endif // PCF_GFX_COLOR_RED 13
#define PCF_GFX_COLOR_RED            ((uint32_t)(0x000000FF))

#if defined(PCF_GFX_COLOR_GREEN)
#error "PCF_GFX_COLOR_GREEN is reserved macro don't define it anywhere \
else."
#endif // PCF_GFX_COLOR_GREEN 14
#define PCF_GFX_COLOR_GREEN          ((uint32_t)(0x0000FF00))

#if defined(PCF_GFX_COLOR_YELLOW)
#error "PCF_GFX_COLOR_YELLOW is reserved macro don't define it anywhere \
else."
#endif // PCF_GFX_COLOR_YELLOW 15
#define PCF_GFX_COLOR_YELLOW         ((uint32_t)(0x0000FFFF))

#if defined(PCF_GFX_COLOR_BLUE)
#error "PCF_GFX_COLOR_BLUE is reserved macro don't define it anywhere \
 else."
#endif // PCF_GFX_COLOR_BLUE 16
#define PCF_GFX_COLOR_BLUE           ((uint32_t)(0x00FF0000))

#if defined(PCF_GFX_COLOR_MAGENTA)
#error "PCF_GFX_COLOR_MAGENTA is reserved macro don't define it anywhere \
else."
#endif // PCF_GFX_COLOR_MAGENTA 17
#define PCF_GFX_COLOR_MAGENTA        ((uint32_t)(0x00FF00FF))

#if defined(PCF_GFX_COLOR_CYAN)
#error "PCF_GFX_COLOR_CYAN is reserved macro don't define it anywhere \
else."
#endif // PCF_GFX_COLOR_CYAN 18
#define PCF_GFX_COLOR_CYAN           ((uint32_t)(0x00FFFF00))

#if defined(PCF_GFX_COLOR_WHITE)
#error "PCF_GFX_COLOR_WHITE is reserved macro don't define it anywhere \
 else."
#endif // PCF_GFX_COLOR_WHITE 19
#define PCF_GFX_COLOR_WHITE          ((uint32_t)(0x00FFFFFF))

#if defined(PCF_GFX_BUILTIN_COLOR_COUNT)
#error "PCF_GFX_BUILTIN_COLOR_COUNT is reserved macro don't define it \
anywhere else."
#endif // PCF_GFX_BUILTIN_COLOR_COUNT 0-19
#define PCF_GFX_BUILTIN_COLOR_COUNT 20

#if defined(PCF_GFX_RGB_TO_COLOR)
#error "PCF_GFX_RGB_TO_COLOR is reserved macro don't define it anywhere \
else."
#endif // PCF_GFX_RGB_TO_COLOR

#if defined(PCF_GFX_RGBA_TO_COLOR)
#error "PCF_GFX_RGB_TO_COLOR is reserved macro don't define it anywhere \
 else."
#endif // PCF_GFX_RGB_TO_COLOR

#if defined(PCF_GFX_COLOR_READ_RED)
#error "PCF_GFX_COLOR_READ_RED is reserved macro don't define it anywhere \
else."
#endif // PCF_GFX_COLOR_READ_RED

#if defined(PCF_GFX_COLOR_READ_GREEN)
#error "PCF_GFX_COLOR_READ_GREEN is reserved macro don't define it \
anywhere else."
#endif // PCF_GFX_COLOR_READ_GREEN

#if defined(PCF_GFX_COLOR_READ_BLUE)
#error "PCF_GFX_COLOR_READ_BLUE is reserved macro don't define it \
anywhere else."
#endif // PCF_GFX_COLOR_READ_BLUE

#define PCF_GFX_RGB_TO_COLOR(red, green, blue) \
         ((uint32_t)(((uint8_t)blue << 16)  | \
         ((uint8_t)green << 8) | ((uint8_t)red)))

#define PCF_GFX_RGBA_TO_COLOR(red, green, blue, alpha) \
         ((uint32_t)(((uint8_t)alpha << 24) | \
         ((uint8_t)blue << 16) | ((uint8_t)green << 8) | ((uint8_t)red)))

#define PCF_GFX_COLOR_READ_RED(color)   \
((uint8_t) (((uint32_t)color) & 0x000000FF))

#define PCF_GFX_COLOR_READ_GREEN(color) \
((uint8_t) ((((uint32_t)color) & 0x0000FF00) >> 8))

#define PCF_GFX_COLOR_READ_BLUE(color)  \
((uint8_t) ((((uint32_t)color) & 0x00FF0000) >> 16))

#if defined(PCF_GFX_TRACE)
#error "PCF_GFX_TRACE is reserved macro don't define it anywhere else."
#endif // PCF_GFX_TRACE

#if defined(PCF_GFX_DEBUG)
#error "PCF_GFX_DEBUG is reserved macro don't define it anywhere else."
#endif // PCF_GFX_DEBUG

#if defined(PCF_GFX_INFO)
#error "PCF_GFX_INFO is reserved macro don't define it anywhere else."
#endif // PCF_GFX_INFO

#if defined(PCF_GFX_WARN)
#error "PCF_GFX_WARN is reserved macro don't define it anywhere else."
#endif // PCF_GFX_WARN

#if defined(PCF_GFX_ERROR)
#error "PCF_GFX_ERROR is reserved macro don't define it anywhere else."
#endif // PCF_GFX_ERROR

#if defined(PCF_GFX_FATAL)
#error "PCF_GFX_FATAL is reserved macro don't define it anywhere else."
#endif // PCF_GFX_FATAL

#if (PCF_GFX_ENABLE_DEBUG == 0)
#define PCF_GFX_TRACE(...)
#define PCF_GFX_DEBUG(...)
#define PCF_GFX_INFO(...)
#define PCF_GFX_WARN(...)
#define PCF_GFX_ERROR(...)
#define PCF_GFX_FATAL(...)
#else
#define PCF_GFX_TRACE(...)  pcf_graphics_log(enLogType_Trace, PCF_FILE, PCF_FUNC, PCF_LINE, __VA_ARGS__);
#define PCF_GFX_DEBUG(...)  pcf_graphics_log(enLogType_Debug, PCF_FILE, PCF_FUNC, PCF_LINE, __VA_ARGS__);
#define PCF_GFX_INFO(...)   pcf_graphics_log(enLogType_Information, PCF_FILE, PCF_FUNC, PCF_LINE, __VA_ARGS__);
#define PCF_GFX_WARN(...)   pcf_graphics_log(enLogType_Warn, PCF_FILE, PCF_FUNC, PCF_LINE, __VA_ARGS__);
#define PCF_GFX_ERROR(...)  pcf_graphics_log(enLogType_Error, PCF_FILE, PCF_FUNC, PCF_LINE, __VA_ARGS__);
#define PCF_GFX_FATAL(...)  pcf_graphics_log(enLogType_Fatal, PCF_FILE, PCF_FUNC, PCF_LINE, __VA_ARGS__);
#endif // PCF_GFX_DISABLE_VARGS_LOG

#endif // __PCF_GRAPHICS_DEFS_H_INCLUDED__
