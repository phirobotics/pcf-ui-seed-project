#ifndef __PCF_GRAPHICS_FONT_H_INCLUDED__
#define __PCF_GRAPHICS_FONT_H_INCLUDED__

#include <pcf/graphics/font/types.h>
PCF_GRAPHICS_BEGIN_CDECLARE

GraphicsFont_t* PCF_GFX_API pcf_graphics_font_new(const GraphicsFontConfig_t* config, pcf_status_t* status);
pcf_status_t    PCF_GFX_API pcf_graphics_font_delete(GraphicsFont_t* font);
GraphicsFont_t* PCF_GFX_API pcf_graphics_font_clone(const GraphicsFont_t* other, pcf_status_t* status);

pcf_status_t    PCF_GFX_API pcf_graphics_font_getSize(const GraphicsFont_t* font, gfx_real_t* size);
pcf_status_t    PCF_GFX_API pcf_graphics_font_getStyle(const GraphicsFont_t* font, EnGraphicsFontStyle_t* style);

PCF_GRAPHICS_END_CDECLARE

#endif // __PCF_GRAPHICS_FONT_H_INCLUDED__
