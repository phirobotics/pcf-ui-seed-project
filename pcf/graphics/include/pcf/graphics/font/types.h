#ifndef __PCF_GRAPHICS_FONT_TYPES_H_INCLUDED__
#define __PCF_GRAPHICS_FONT_TYPES_H_INCLUDED__

PCF_GRAPHICS_BEGIN_CDECLARE

typedef void GraphicsFont_t;

typedef enum
{
  enGraphicsFontStyle_Regular,
  enGraphicsFontStyle_Bold,
  enGraphicsFontStyle_Italic,
  enGraphicsFontStyle_BoldItalic,
  enGraphicsFontStyle_Underline,
  enGraphicsFontStyle_Strikeout
}EnGraphicsFontStyle_t;

typedef enum
{
  enGraphicsFontType_MonoSpace,
  enGraphicsFontType_SansSerif,
  enGraphicsFontType_Serif,
}EnGraphicsFontType_t;

typedef struct
{
    gfx_real_t size;
    EnGraphicsFontStyle_t style;
    EnGraphicsFontType_t type;
}GraphicsFontConfig_t;

PCF_GRAPHICS_END_CDECLARE
#endif // __PCF_GRAPHICS_FONT_TYPES_H_INCLUDED__
