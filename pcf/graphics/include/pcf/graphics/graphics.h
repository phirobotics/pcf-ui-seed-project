#ifndef __PCF_GRAPHICS_H_INCLUDED__
#define __PCF_GRAPHICS_H_INCLUDED__

#include <pcf/graphics/types.h>

PCF_GRAPHICS_BEGIN_CDECLARE

uint32_t PCF_GFX_API pcf_graphics_getVersion(void);
EnGraphicsPlatformType_t PCF_GFX_API pcf_graphics_getPlatfromType(void);
const char* PCF_GFX_API pcf_graphics_getPlatfromTypeText(void);


pcf_status_t PCF_GFX_API pcf_graphics_log(EnLogType_t logType, const char* fileName,
                                           const char* functionName, int32_t line,
                                           const char* format, ...);

pcf_status_t PCF_GFX_API pcf_graphics_init(const GraphicsConfig_t* config);
EnBoolean_t  PCF_GFX_API pcf_graphics_isInitialized();
EnBoolean_t  PCF_GFX_API pcf_graphics_hasSupportForCustomColors();

void*        PCF_GFX_API pcf_graphics_malloc(size_t size, pcf_status_t* status);
pcf_status_t PCF_GFX_API pcf_graphics_free(void* mem);
void*        PCF_GFX_API pcf_graphics_realloc(void* mem, size_t resize, pcf_status_t* status);

EnBoolean_t  PCF_GFX_API pcf_graphics_isRectEmpty(const GraphicsRect_t* rect, pcf_status_t* status);
pcf_status_t PCF_GFX_API pcf_graphics_makeRectEmpty(GraphicsRect_t* rect);
pcf_status_t PCF_GFX_API pcf_graphics_findRectIntersection(const GraphicsRect_t* rectA,
                                                           const GraphicsRect_t* rectB,
                                                           GraphicsRect_t* intersection);
EnBoolean_t  PCF_GFX_API pcf_graphics_isPointInRect(const GraphicsRect_t* rect,
                                                    const GraphicsPoint_t* point,
                                                    pcf_status_t* status);
PCF_GRAPHICS_END_CDECLARE

#endif // __PCF_GRAPHICS_H_INCLUDED__
