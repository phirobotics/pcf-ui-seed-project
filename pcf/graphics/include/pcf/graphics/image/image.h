#ifndef __PCF_GRAPHICS_IMAGE_H_INCLUDED__
#define __PCF_GRAPHICS_IMAGE_H_INCLUDED__

#include <pcf/graphics/image/types.h>
#include <pcf/graphics/context2d/types.h>

PCF_GRAPHICS_BEGIN_CDECLARE

GraphicsImage_t* PCF_GFX_API pcf_graphics_image_newFromContext(gfx_int_t  width, gfx_int_t  height,
                                                    GraphicsContext2D_t* context, pcf_status_t* status);

GraphicsImage_t* PCF_GFX_API pcf_graphics_image_newFromData(gfx_int_t  width, gfx_int_t  height, gfx_int_t stride,
                                                            uint8_t* data, EnGraphicsImagePixelFormat_t format, pcf_status_t* status);

GraphicsImage_t* PCF_GFX_API pcf_graphics_image_newFromFile(const char* fullFilePath, pcf_status_t* status);

pcf_status_t     PCF_GFX_API pcf_graphics_image_delete(GraphicsImage_t* image);

GraphicsImage_t* PCF_GFX_API pcf_graphics_image_clone(const GraphicsImage_t* other,
                                                      pcf_status_t* status);

GraphicsImageData_t* PCF_GFX_API pcf_graphics_image_getData(const GraphicsImage_t* image,
                                                            pcf_status_t* status);

pcf_status_t PCF_GFX_API pcf_graphics_image_freeData(GraphicsImageData_t* imageData);

PCF_GRAPHICS_END_CDECLARE

#endif // __PCF_GRAPHICS_IMAGE_H_INCLUDED__
