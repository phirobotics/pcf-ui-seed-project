#ifndef __PCF_GRAPHICS_IMAGE_TYPES_H_INCLUDED__
#define __PCF_GRAPHICS_IMAGE_TYPES_H_INCLUDED__

#include <pcf/graphics/types.h>

PCF_GRAPHICS_BEGIN_CDECLARE

typedef struct _pcf_graphics_image_t GraphicsImage_t;

typedef enum
{
    enGraphicsImagePixelFormat_Default,
    enGraphicsImagePixelFormat_1BPP,
    enGraphicsImagePixelFormat_4BPP,
    enGraphicsImagePixelFormat_8BPP,
    enGraphicsImagePixelFormat_A1R5G5B5,
    enGraphicsImagePixelFormat_Gray16,
    enGraphicsImagePixelFormat_A0R5G5B5,
    enGraphicsImagePixelFormat_R5G6B5,
    enGraphicsImagePixelFormat_R8G8B8,
    enGraphicsImagePixelFormat_A8R8G8B8,
    enGraphicsImagePixelFormat_PA8R8G8B8,
    enGraphicsImagePixelFormat_R16G16B16,
    enGraphicsImagePixelFormat_A16R16G16B16,
    enGraphicsImagePixelFormat_PA16R16G16B16,
    enGraphicsImagePixelFormat_Min = enGraphicsImagePixelFormat_Default,
    enGraphicsImagePixelFormat_Max = enGraphicsImagePixelFormat_PA16R16G16B16,
    enGraphicsImagePixelFormat_MaxValue = PCF_ENUM_MAXVALUE
}EnGraphicsImagePixelFormat_t;

typedef enum
{
    enGraphicsImageChannel_Gray = 1,
    enGraphicsImageChannel_Red = 1,
    enGraphicsImageChannel_Green = (enGraphicsImageChannel_Red << 1),
    enGraphicsImageChannel_Blue = (enGraphicsImageChannel_Green << 1),
    enGraphicsImageChannel_RedGreen = (enGraphicsImageChannel_Red | enGraphicsImageChannel_Green),
    enGraphicsImageChannel_GreenBlue = (enGraphicsImageChannel_Green | enGraphicsImageChannel_Blue),
    enGraphicsImageChannel_BlueRed = (enGraphicsImageChannel_Blue | enGraphicsImageChannel_Red),
    enGraphicsImageChannel_Alpha = (enGraphicsImageChannel_Blue << 1)
}EnGraphicsImageChannel_t;

typedef struct
{
    uint32_t  width;
    uint32_t  height;
    uint32_t stride;
    EnGraphicsImagePixelFormat_t format;
    uint32_t bitsPerPixel;
    uint8_t* imageBuffer;
    size_t   bufferSize;
}GraphicsImageData_t;

PCF_GRAPHICS_END_CDECLARE

#endif // __PCF_GRAPHICS_IMAGE_TYPES_H_INCLUDED__
