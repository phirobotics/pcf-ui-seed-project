#ifndef __PCF_GRAPHICS_MATRIX_H_INCLUDED__
#define __PCF_GRAPHICS_MATRIX_H_INCLUDED__

#include <pcf/graphics/matrix/types.h>

PCF_GRAPHICS_BEGIN_CDECLARE

GraphicsMatrix_t* PCF_GFX_API pcf_graphics_matrix_new(pcf_status_t* status);
GraphicsMatrix_t* PCF_GFX_API pcf_graphics_matrix_newFrom(gfx_real_t m11, gfx_real_t m12, gfx_real_t m21,
                                                          gfx_real_t m22, gfx_real_t dx, gfx_real_t dy, pcf_status_t* status);
GraphicsMatrix_t* PCF_GFX_API pcf_graphics_matrix_clone(const GraphicsMatrix_t* other, pcf_status_t* status);

pcf_status_t  PCF_GFX_API pcf_graphics_matrix_delete(GraphicsMatrix_t* matrix);

pcf_status_t  PCF_GFX_API pcf_graphics_matrix_setElements(GraphicsMatrix_t* matrix, gfx_real_t m11, gfx_real_t m12,
                                                              gfx_real_t m21, gfx_real_t m22, gfx_real_t dx, gfx_real_t dy);

pcf_status_t  PCF_GFX_API pcf_graphics_matrix_multiply(GraphicsMatrix_t* matrix, const GraphicsMatrix_t* other,
                                                       EnGraphicsMatrixOperationOrder_t order);
pcf_status_t  PCF_GFX_API pcf_graphics_matrix_translate(GraphicsMatrix_t* matrix, gfx_real_t dx, gfx_real_t dy,
                                                        EnGraphicsMatrixOperationOrder_t order);
pcf_status_t  PCF_GFX_API pcf_graphics_matrix_scale(GraphicsMatrix_t* matrix, gfx_real_t sx, gfx_real_t sy,
                                                    EnGraphicsMatrixOperationOrder_t order);
pcf_status_t  PCF_GFX_API pcf_graphics_matrix_rotate(GraphicsMatrix_t* matrix, gfx_real_t rotationAngleInRadians,
                                                     EnGraphicsMatrixOperationOrder_t order);

pcf_status_t  PCF_GFX_API pcf_graphics_matrix_invert(GraphicsMatrix_t* matrix);
pcf_status_t  PCF_GFX_API pcf_graphics_matrix_identity(GraphicsMatrix_t* matrix);
EnBoolean_t   PCF_GFX_API pcf_graphics_matrix_isInvertible(const GraphicsMatrix_t* matrix, pcf_status_t* status);
EnBoolean_t   PCF_GFX_API pcf_graphics_matrix_isIdentity(const GraphicsMatrix_t* matrix, pcf_status_t* status);
EnBoolean_t   PCF_GFX_API pcf_graphics_matrix_isEqual(const GraphicsMatrix_t* matrixA, const GraphicsMatrix_t* matrixB,
                                                       pcf_status_t* status);

pcf_status_t  PCF_GFX_API pcf_graphics_matrix_transformPointsF(const GraphicsMatrix_t* matrix,
                                                                      GraphicsPointF_t* points, size_t count);
pcf_status_t  PCF_GFX_API pcf_graphics_matrix_transformPoints(const GraphicsMatrix_t* matrix,
                                                                     GraphicsPoint_t* points, size_t count);

pcf_status_t  PCF_GFX_API pcf_graphics_matrix_getElements(const GraphicsMatrix_t* matrix,
                                                          GraphicsTransformation2D_t* transform);

PCF_GRAPHICS_END_CDECLARE

#endif // __PCF_GRAPHICS_MATRIX_H_INCLUDED__
