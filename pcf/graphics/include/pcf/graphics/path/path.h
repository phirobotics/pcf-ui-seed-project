#ifndef __PCF_GRAPHICS_PATH_H_INCLUDED__
#define __PCF_GRAPHICS_PATH_H_INCLUDED__

#include <pcf/graphics/path/types.h>
#include <pcf/graphics/matrix/types.h>
#include <pcf/graphics/pen/types.h>

PCF_GRAPHICS_BEGIN_CDECLARE

GraphicsPath_t* PCF_GFX_API pcf_graphics_path_new(pcf_status_t* status);
pcf_status_t    PCF_GFX_API pcf_graphics_path_delete(GraphicsPath_t* path);
GraphicsPath_t* PCF_GFX_API pcf_graphics_path_clone(const GraphicsPath_t* other, pcf_status_t* status);
pcf_status_t    PCF_GFX_API pcf_graphics_path_reset(GraphicsPath_t* path);

pcf_status_t PCF_GFX_API pcf_graphics_path_getBounds(const GraphicsPath_t* path, const GraphicsPen_t* pen,
                                                     const GraphicsMatrix_t* transform, GraphicsRect_t* bounds);
pcf_status_t PCF_GFX_API pcf_graphics_path_getBoundsF(const GraphicsPath_t* path, const GraphicsPen_t* pen,
                                                      const GraphicsMatrix_t* transform, GraphicsRectF_t* bounds);

pcf_status_t PCF_GFX_API pcf_graphics_path_getPointCount(const GraphicsPath_t* path, size_t* count);
pcf_status_t PCF_GFX_API pcf_graphics_path_getFillMode(const GraphicsPath_t* path, EnGraphicsFillMode_t* mode);
pcf_status_t PCF_GFX_API pcf_graphics_path_setFillMode(const GraphicsPath_t* path, EnGraphicsFillMode_t mode);

pcf_status_t PCF_GFX_API pcf_graphics_path_begin(GraphicsPath_t* path);
pcf_status_t PCF_GFX_API pcf_graphics_path_close(GraphicsPath_t* path);
pcf_status_t PCF_GFX_API pcf_graphics_path_closeAll(GraphicsPath_t* path);
pcf_status_t PCF_GFX_API pcf_graphics_path_reverse(GraphicsPath_t* path);

pcf_status_t PCF_GFX_API pcf_graphics_path_getLastPointF(const GraphicsPath_t* path, GraphicsPointF_t* point);

pcf_status_t PCF_GFX_API pcf_graphics_path_addLineF(GraphicsPath_t* path, gfx_real_t x1, gfx_real_t y1,
                                                    gfx_real_t x2, gfx_real_t y2);
pcf_status_t PCF_GFX_API pcf_graphics_path_addLinesF(GraphicsPath_t* path, GraphicsPointF_t* points,
                                                     size_t count, size_t* addedCount);

pcf_status_t PCF_GFX_API pcf_graphics_path_addRectangleF(GraphicsPath_t* path, const GraphicsRectF_t* rect);
pcf_status_t PCF_GFX_API pcf_graphics_path_addRectangleF1(GraphicsPath_t* path, gfx_real_t top, gfx_real_t left,
                                                          gfx_real_t width, gfx_real_t height);
pcf_status_t PCF_GFX_API pcf_graphics_path_addRectanglesF(GraphicsPath_t* path, const GraphicsRectF_t* rects,
                                                          size_t count, size_t* addedCount);

pcf_status_t PCF_GFX_API pcf_graphics_path_addArcF(GraphicsPath_t* path, gfx_real_t x, gfx_real_t y,
                                                   gfx_real_t width, gfx_real_t height,
                                                   gfx_real_t startAngleInRadians,
                                                   gfx_real_t sweepAngleInRadians);

pcf_status_t PCF_GFX_API pcf_graphics_path_addBezierF(GraphicsPath_t* path, gfx_real_t x1, gfx_real_t y1,
                                                      gfx_real_t x2, gfx_real_t y2,
                                                      gfx_real_t x3, gfx_real_t y3,
                                                      gfx_real_t x4, gfx_real_t y4);

pcf_status_t PCF_GFX_API pcf_graphics_path_addBeziersF(GraphicsPath_t* path, const GraphicsPointF_t* points,
                                                       size_t count, size_t* addedBezierCount);


pcf_status_t PCF_GFX_API pcf_graphics_path_addEllipseF(GraphicsPath_t* path, gfx_real_t top, gfx_real_t left,
                                                       gfx_real_t width, gfx_real_t height);

pcf_status_t PCF_GFX_API pcf_graphics_path_addPieF(GraphicsPath_t* path, gfx_real_t top, gfx_real_t left,
                                                   gfx_real_t width, gfx_real_t height,
                                                   gfx_real_t startAngleInRadians, gfx_real_t sweepAngleInRadians);

pcf_status_t PCF_GFX_API pcf_graphics_path_addPolygonF(GraphicsPath_t* path, const GraphicsPointF_t* points,
                                                       size_t count);

pcf_status_t PCF_GFX_API pcf_graphics_path_addPath(GraphicsPath_t* path, const GraphicsPath_t* pathToAdd,
                                                   EnBoolean_t connect);

pcf_status_t PCF_GFX_API pcf_graphics_path_addLine(GraphicsPath_t* path, gfx_int_t x1, gfx_int_t y1,
                                                   gfx_int_t x2, gfx_int_t y2);
pcf_status_t PCF_GFX_API pcf_graphics_path_addLines(GraphicsPath_t* path, GraphicsPoint_t* points, size_t count, size_t* addedCount);

pcf_status_t PCF_GFX_API pcf_graphics_path_addRectangle(GraphicsPath_t* path, const GraphicsRect_t* rect);
pcf_status_t PCF_GFX_API pcf_graphics_path_addRectangle1(GraphicsPath_t* path, gfx_int_t top, gfx_int_t left,
                                                         gfx_int_t width, gfx_int_t height);
pcf_status_t PCF_GFX_API pcf_graphics_path_addRectangles(GraphicsPath_t* path, const GraphicsRect_t* rect,
                                                         size_t count, size_t* addedCount);

pcf_status_t PCF_GFX_API pcf_graphics_path_addArc(GraphicsPath_t* path, gfx_int_t x, gfx_int_t y,
                                                  gfx_int_t width, gfx_int_t height,
                                                   gfx_real_t startAngleInRadians, gfx_real_t sweepAngleInRadians);

pcf_status_t PCF_GFX_API pcf_graphics_path_addBezier(GraphicsPath_t* path, gfx_int_t x1, gfx_int_t y1,
                                                     gfx_int_t x2, gfx_int_t y2,
                                                   gfx_int_t x3, gfx_int_t y3,
                                                   gfx_int_t x4, gfx_int_t y4);
pcf_status_t PCF_GFX_API pcf_graphics_path_addBeziers(GraphicsPath_t* path, const GraphicsPoint_t* points,
                                                      size_t count, size_t* addedBezierCount);

pcf_status_t PCF_GFX_API pcf_graphics_path_addEllipse(GraphicsPath_t* path, gfx_int_t top, gfx_int_t left,
                                                      gfx_int_t width, gfx_int_t height);

pcf_status_t PCF_GFX_API pcf_graphics_path_addPie(GraphicsPath_t* path, gfx_int_t top, gfx_int_t left,
                                                  gfx_int_t width, gfx_int_t height,
                                                  gfx_real_t startAngleInRadians, gfx_real_t sweepAngleInRadians);

pcf_status_t PCF_GFX_API pcf_graphics_path_addPolygon(GraphicsPath_t* path, const GraphicsPoint_t* points,
                                                      size_t count);

PCF_GRAPHICS_END_CDECLARE

#endif // __PCF_GRAPHICS_PATH_H_INCLUDED__
