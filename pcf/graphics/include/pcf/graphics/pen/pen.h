#ifndef __PCF_GRAPHICS_PEN_H_INCLUDED__
#define __PCF_GRAPHICS_PEN_H_INCLUDED__

#include <pcf/graphics/pen/types.h>
#include <pcf/graphics/matrix/types.h>

PCF_GRAPHICS_BEGIN_CDECLARE

GraphicsPen_t* PCF_GFX_API pcf_graphics_pen_new(const GraphicsPenConfig_t* config, pcf_status_t* status);
pcf_status_t   PCF_GFX_API pcf_graphics_pen_delete(GraphicsPen_t* pen);
GraphicsPen_t* PCF_GFX_API pcf_graphics_pen_clone(const GraphicsPen_t* other, pcf_status_t* status);

pcf_status_t PCF_GFX_API pcf_graphics_pen_setWidth(GraphicsPen_t* pen, gfx_real_t width);
pcf_status_t PCF_GFX_API pcf_graphics_pen_getWidth(const GraphicsPen_t* pen, gfx_real_t* width);

pcf_status_t PCF_GFX_API pcf_graphics_pen_setStyle(GraphicsPen_t* pen, EnGraphicsPenStyle_t style);
pcf_status_t PCF_GFX_API pcf_graphics_pen_getStyle(const GraphicsPen_t* pen, EnGraphicsPenStyle_t* style);

pcf_status_t PCF_GFX_API pcf_graphics_pen_setColor(GraphicsPen_t* pen, uint32_t argb);
pcf_status_t PCF_GFX_API pcf_graphics_pen_getColor(const GraphicsPen_t* pen, uint32_t* argb);

pcf_status_t PCF_GFX_API pcf_graphics_pen_setStartCap(GraphicsPen_t* pen, EnGraphicsLineCap_t cap);
pcf_status_t PCF_GFX_API pcf_graphics_pen_getStartCap(const GraphicsPen_t* pen, EnGraphicsLineCap_t* cap);

pcf_status_t PCF_GFX_API pcf_graphics_pen_setEndCap(GraphicsPen_t* pen, EnGraphicsLineCap_t cap);
pcf_status_t PCF_GFX_API pcf_graphics_pen_getEndCap(const GraphicsPen_t* pen, EnGraphicsLineCap_t* cap);

pcf_status_t PCF_GFX_API pcf_graphics_pen_setLineJoinType(GraphicsPen_t* pen, EnGraphicsLineJoinType_t joinType);
pcf_status_t PCF_GFX_API pcf_graphics_pen_getLineJoinType(const GraphicsPen_t* pen, EnGraphicsLineJoinType_t* joinType);

pcf_status_t PCF_GFX_API pcf_graphics_pen_setAlignment(GraphicsPen_t* pen, EnGraphicsPenAlignment_t alignment);
pcf_status_t PCF_GFX_API pcf_graphics_pen_getAlignment(const GraphicsPen_t* pen, EnGraphicsPenAlignment_t* alignment);

pcf_status_t PCF_GFX_API pcf_graphics_pen_setMiterLimit(GraphicsPen_t* pen, gfx_real_t limit);
pcf_status_t PCF_GFX_API pcf_graphics_pen_getMiterLimit(const GraphicsPen_t* pen, gfx_real_t* limit);

pcf_status_t PCF_GFX_API pcf_graphics_pen_setTransform(GraphicsPen_t* pen, GraphicsMatrix_t* matrix);
pcf_status_t PCF_GFX_API pcf_graphics_pen_getTransform(const GraphicsPen_t* pen, GraphicsMatrix_t* matrix);
pcf_status_t PCF_GFX_API pcf_graphics_pen_resetTransform(GraphicsPen_t* pen);
pcf_status_t PCF_GFX_API pcf_graphics_pen_multiplyTransform(GraphicsPen_t* pen, const GraphicsMatrix_t* matrix,
                                                            EnGraphicsMatrixOperationOrder_t order);
pcf_status_t PCF_GFX_API pcf_graphics_pen_translateTransform(GraphicsPen_t* pen, gfx_real_t tx,gfx_real_t ty,
                                                             EnGraphicsMatrixOperationOrder_t order);
pcf_status_t PCF_GFX_API pcf_graphics_pen_scaleTransform(GraphicsPen_t* pen, gfx_real_t sx,gfx_real_t sy,
                                                         EnGraphicsMatrixOperationOrder_t order);
pcf_status_t PCF_GFX_API pcf_graphics_pen_rotateTransform(GraphicsPen_t* pen, gfx_real_t angleInRadians,
                                                          EnGraphicsMatrixOperationOrder_t order);

PCF_GRAPHICS_END_CDECLARE
#endif // __PCF_GRAPHICS_PEN_H_INCLUDED__
