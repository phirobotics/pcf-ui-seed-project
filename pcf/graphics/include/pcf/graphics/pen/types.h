#ifndef __PCF_GRAPHICS_PEN_TYPES_H_INCLUDED__
#define __PCF_GRAPHICS_PEN_TYPES_H_INCLUDED__
#include <pcf/graphics/types.h>

PCF_GRAPHICS_BEGIN_CDECLARE

typedef enum
{
    enGraphicsPenStyle_Default,
    enGraphicsPenStyle_Solid,
    enGraphicsPenStyle_Dash,
    enGraphicsPenStyle_Dot,
    enGraphicsPenStyle_DashDot,
    enGraphicsPenStyle_DashDotDot,
    enGraphicsPenStyle_MaxValue = PCF_ENUM_MAXVALUE
}EnGraphicsPenStyle_t;

typedef enum
{
    enGraphicsLineCap_None = 0x00,
	enGraphicsLineCap_Flat = enGraphicsLineCap_None,
	enGraphicsLineCap_Square = 0x01,
	enGraphicsLineCap_Round = 0x02,
	enGraphicsLineCap_Triangle = 0x03,
	enGraphicsLineCap_NoAnchor = 0x10,
	enGraphicsLineCap_SquareAnchor = (enGraphicsLineCap_NoAnchor | enGraphicsLineCap_Square),
	enGraphicsLineCap_RoundAnchor = (enGraphicsLineCap_NoAnchor | enGraphicsLineCap_Round),
	enGraphicsLineCap_DiamondAnchor = (enGraphicsLineCap_NoAnchor | enGraphicsLineCap_Triangle),
	enGraphicsLineCap_ArrowAnchor = ((enGraphicsLineCap_NoAnchor | enGraphicsLineCap_Triangle) + 1),
}EnGraphicsLineCap_t;

typedef enum
{
  enGraphicsPenAlignment_Center,
  enGraphicsPenAlignment_Inset
}EnGraphicsPenAlignment_t;

typedef enum
{
	enGraphicsLineJoinType_Miter = 0,
	enGraphicsLineJoinType_Bevel = 1,
	enGraphicsLineJoinType_Round = 2,
	enGraphicsLineJoinType_MiterClipped = 3
}EnGraphicsLineJoinType_t;

typedef struct
{
    EnGraphicsLineCap_t startCap;
    EnGraphicsLineCap_t endCap;
    EnGraphicsLineJoinType_t lineJoinType;
    EnGraphicsPenStyle_t style;
    float width;
    GraphicsColor_t color;
    void* extra;
}GraphicsPenConfig_t;

typedef void GraphicsPen_t;

PCF_GRAPHICS_END_CDECLARE

#endif // __PCF_GRAPHICS_PEN_TYPES_H_INCLUDED__
