#ifndef __PCF_GDIPLUS_REGION_H_INCLUDED__
#define __PCF_GDIPLUS_REGION_H_INCLUDED__

#include <pcf/graphics/region/types.h>
#include <pcf/graphics/path/types.h>
#include <pcf/graphics/context2d/types.h>

PCF_GRAPHICS_BEGIN_CDECLARE

GraphicsRegion_t* PCF_GFX_API pcf_graphics_region_new(pcf_status_t* status);
GraphicsRegion_t* PCF_GFX_API pcf_graphics_region_newFromRect(const GraphicsRect_t* rect, pcf_status_t* status);
GraphicsRegion_t* PCF_GFX_API pcf_graphics_region_newFromRectF(const GraphicsRectF_t* rect, pcf_status_t* status);
GraphicsRegion_t* PCF_GFX_API pcf_graphics_region_newFromPath(const GraphicsPath_t* path, pcf_status_t* status);
pcf_status_t      PCF_GFX_API pcf_graphics_region_delete(GraphicsRegion_t* region);
GraphicsRegion_t* PCF_GFX_API pcf_graphics_region_clone(const GraphicsRegion_t* other, pcf_status_t* status);

pcf_status_t      PCF_GFX_API pcf_graphics_region_makeEmpty(GraphicsRegion_t* region);
EnBoolean_t       PCF_GFX_API pcf_graphics_region_isEmpty(GraphicsRegion_t* region,
                                                          GraphicsContext2D_t* context, pcf_status_t* status);
EnBoolean_t       PCF_GFX_API pcf_graphics_region_isInfinite(GraphicsRegion_t* region,
                                                          GraphicsContext2D_t* context, pcf_status_t* status);

EnBoolean_t       PCF_GFX_API pcf_graphics_region_isPointVisible(GraphicsRegion_t* region, GraphicsContext2D_t* context,
                                                                 gfx_int_t x, gfx_int_t y, pcf_status_t* status);
EnBoolean_t       PCF_GFX_API pcf_graphics_region_isPointVisibleF(GraphicsRegion_t* region, GraphicsContext2D_t* context,
                                                                 gfx_real_t x, gfx_real_t y, pcf_status_t* status);
EnBoolean_t       PCF_GFX_API pcf_graphics_region_isRectVisible(GraphicsRegion_t* region, GraphicsContext2D_t* context,
                                                                 gfx_int_t x, gfx_int_t y,
                                                                 gfx_int_t width, gfx_int_t height, pcf_status_t* status);
EnBoolean_t       PCF_GFX_API pcf_graphics_region_isRectVisibleF(GraphicsRegion_t* region, GraphicsContext2D_t* context,
                                                                 gfx_real_t x, gfx_real_t y,
                                                                 gfx_int_t width, gfx_int_t height, pcf_status_t* status);



pcf_status_t      PCF_GFX_API pcf_graphics_region_getBounds(const GraphicsRegion_t* region, const GraphicsContext2D_t* context,
                                                            GraphicsRect_t* rect);
pcf_status_t      PCF_GFX_API pcf_graphics_region_getBoundsF(const GraphicsRegion_t* region, const GraphicsContext2D_t* context,
                                                             GraphicsRectF_t* rect);

EnBoolean_t       PCF_GFX_API pcf_graphics_region_isEqual(const GraphicsRegion_t* region, const GraphicsRegion_t* region2,
                                                          const GraphicsContext2D_t* context, pcf_status_t* status);

pcf_status_t      PCF_GFX_API pcf_graphics_region_combineRect(GraphicsRegion_t* region, const GraphicsRect_t* rect,
                                                              EnGraphicsCombineMode_t mode);
pcf_status_t      PCF_GFX_API pcf_graphics_region_combineRectF(GraphicsRegion_t* region, const GraphicsRectF_t* rect,
                                                              EnGraphicsCombineMode_t mode);
pcf_status_t      PCF_GFX_API pcf_graphics_region_combinePath(GraphicsRegion_t* region, GraphicsPath_t* path,
                                                              EnGraphicsCombineMode_t mode);
pcf_status_t      PCF_GFX_API pcf_graphics_region_combineRegion(GraphicsRegion_t* region, GraphicsRegion_t* regionToCombine,
                                                              EnGraphicsCombineMode_t mode);

pcf_status_t      PCF_GFX_API pcf_graphics_region_translate(GraphicsRegion_t* region, gfx_int_t tx, gfx_int_t ty);
pcf_status_t      PCF_GFX_API pcf_graphics_region_translateF(GraphicsRegion_t* region, gfx_real_t tx, gfx_real_t ty);
pcf_status_t      PCF_GFX_API pcf_graphics_region_transform(GraphicsRegion_t* region, GraphicsMatrix_t* matrix);

PCF_GRAPHICS_END_CDECLARE
#endif // __PCF_GDIPLUS_REGION_H_INCLUDED__
