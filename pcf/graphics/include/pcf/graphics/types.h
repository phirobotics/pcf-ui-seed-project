#ifndef _PCF_GRAPHICS_TYPES_H_INCLUDED__
#define _PCF_GRAPHICS_TYPES_H_INCLUDED__

#include <pcf/graphics/defs.h>

PCF_GRAPHICS_BEGIN_CDECLARE

typedef char    pcf_graphics_property_text[PCF_GFX_PROPERTY_TEXT_SIZE];
typedef char    pcf_graphics_path_text[PCF_PATH_TEXT_SIZE];

typedef int32_t    gfx_int_t;
typedef float      gfx_real_t;

typedef struct _pcf_graphics_size_t
{
    gfx_int_t Width;
    gfx_int_t Height;
}GraphicsSize_t;

typedef struct _pcf_graphics_sizeF_t
{
    gfx_real_t Width;
    gfx_real_t Height;
}GraphicsSizeF_t;

typedef struct _pcf_graphics_point_t
{
    gfx_int_t  X;
    gfx_int_t  Y;
}GraphicsPoint_t;

typedef struct _pcf_graphics_pointF_t
{
    float  X;
    float  Y;
}GraphicsPointF_t;

typedef struct _pcf_graphics_rect_t
{
    gfx_int_t X;
	gfx_int_t Y;
	gfx_int_t Width;
	gfx_int_t Height;
}GraphicsRect_t;

typedef struct _pcf_graphics_rectF_t
{
   gfx_real_t X;
   gfx_real_t Y;
   gfx_real_t Width;
   gfx_real_t Height;
}GraphicsRectF_t;

typedef uint32_t  Context2DState_t;
typedef uint32_t  Context2DSection_t;

typedef struct _pcf_graphics_pointArray_t
{
    GraphicsPoint_t* points;
    size_t           length;
}GraphicsPointArray_t;

typedef struct _pcf_graphics_pointArrayF_t
{
    GraphicsPointF_t* points;
    size_t           length;
}GraphicsPointArrayF_t;

typedef struct
{
    GraphicsRect_t  boundRect;
    GraphicsPoint_t startPoint;
    GraphicsPoint_t endPoint;
}GraphicsArc_t;

typedef GraphicsPointArray_t GraphicsPolygon_t;
typedef GraphicsPointArray_t GraphicsPolyline_t;

typedef struct _pcf_graphics_transformation2D_t
{
    float m11;
    float m12;
    float m21;
    float m22;
    float dx;
    float dy;
}GraphicsTransformation2D_t;

typedef enum
{
    enGraphicsFillMode_Alternate = 0,
	enGraphicsFillMode_Winding = 1
}EnGraphicsFillMode_t;

typedef enum
{
    enGraphicsPlatformType_None = PCF_GFX_PLATFORM_NONE,
    enGraphicsPlatformType_Win32Gdi = PCF_GFX_PLATFORM_WIN32_GDI,
    enGraphicsPlatformType_Win32GdiPlus = PCF_GFX_PLATFORM_WIN32_GDIPLUS,
    enGraphicsPlatformType_Win32DDraw = PCF_GFX_PLATFORM_WIN32_DDRAW,
    enGraphicsPlatformType_Win32DirectX10 = PCF_GFX_PLATFORM_WIN32_DIRECTX10,
    enGraphicsPlatformType_Win32DirectX11 = PCF_GFX_PLATFORM_WIN32_DIRECTX11,
    enGraphicsPlatformType_OpenGL = PCF_GFX_PLATFORM_OPENGL,
    enGraphicsPlatformType_ModernOpenGL = PCF_GFX_PLATFORM_OPENGL_MODERN,
    enGraphicsPlatformType_RawDisplayDevice = PCF_GFX_PLATFORM_RAWDISPLAY,
    enGraphicsPlatformType_MaxValue = 0xFF
}EnGraphicsPlatformType_t;

typedef enum
{
   enGraphicsErrorCode_None = -PCF_GFX_StatusCode_None,
   enGraphicsErrorCode_Error = -PCF_GFX_StatusCode_Error,
   enGraphicsErrorCode_InvalidArg = -PCF_GFX_StatusCode_InvalidArg,
   enGraphicsErrorCode_NullArg = -PCF_GFX_StatusCode_NullArg,
   enGraphicsErrorCode_NotImplemented = -PCF_GFX_StatusCode_NotImplemented,
   enGraphicsErrorCode_NotSupported = -PCF_GFX_StatusCode_NotSupported,
   enGraphicsErrorCode_NotEnoughMemory = -PCF_GFX_StatusCode_NotEnoughMemory,
   enGraphicsErrorCode_AllocationError = -PCF_GFX_StatusCode_AllocationError,
   enGraphicsErrorCode_InvalidCapacity = -PCF_GFX_StatusCode_InvalidCapacity,
   enGraphicsErrorCode_InvalidRange = -PCF_GFX_StatusCode_InvalidRange,
   enGraphicsErrorCode_MaxCapacity = -PCF_GFX_StatusCode_MaxCapacity,
   enGraphicsErrorCode_ResourceNotFound = -PCF_GFX_StatusCode_ResourceNotFound,
   enGraphicsErrorCode_IndexOutOfRange = -PCF_GFX_StatusCode_IndexOutOfRange,
   enGraphicsErrorCode_InvalidOperation = -PCF_GFX_StatusCode_InvalidOperation,
   enGraphicsErrorCode_FileDoesntExist = -PCF_GFX_StatusCode_FileDoesntExist,
   enGraphicsErrorCode_FileFormatNotSupported = -PCF_GFX_StatusCode_FileFormatNotSupported,
   enGraphicsErrorCode_InvalidFileFormat = -PCF_GFX_StatusCode_InvalidFileFormat,
   enGraphicsErrorCode_InternalError = -PCF_GFX_StatusCode_InternalError,
   enGraphicsErrorCode_NotAllowed = -PCF_GFX_StatusCode_NotAllowed,
   enGraphicsErrorCode_InvalidState = -PCF_GFX_StatusCode_InvalidState,
   enGraphicsErrorCodeCount = PCF_GFX_StatusCode_Count,
   enGraphicsError_MaxValue = PCF_ENUM_MAXVALUE
}EnGraphicsErrorCode_t;

typedef enum
{
   enGraphicsColor_Black = PCF_GFX_COLOR_BLACK,
   enGraphicsColor_DarkRed = PCF_GFX_COLOR_DARKRED,
   enGraphicsColor_DarkGreen = PCF_GFX_COLOR_DARKGREEN,
   enGraphicsColor_DarkBlue = PCF_GFX_COLOR_DARKBLUE,
   enGraphicsColor_DarkYellow = PCF_GFX_COLOR_DARKYELLOW,
   enGraphicsColor_DarkMagenta = PCF_GFX_COLOR_DARKMAGENTA,
   enGraphicsColor_DarkCyan = PCF_GFX_COLOR_DARKCYAN,
   enGraphicsColor_LightGray = PCF_GFX_COLOR_LIGHTGRAY,
   enGraphicsColor_MoneyGreen = PCF_GFX_COLOR_MONEYGREEN,
   enGraphicsColor_SkyBlue = PCF_GFX_COLOR_SKYBLUE,
   enGraphicsColor_Cream = PCF_GFX_COLOR_CREAM,
   enGraphicsColor_MediumGray = PCF_GFX_COLOR_MEDIUMGRAY,
   enGraphicsColor_DarkGray = PCF_GFX_COLOR_DARKGRAY,
   enGraphicsColor_Red = PCF_GFX_COLOR_RED,
   enGraphicsColor_Green = PCF_GFX_COLOR_GREEN,
   enGraphicsColor_Yellow = PCF_GFX_COLOR_YELLOW,
   enGraphicsColor_Blue = PCF_GFX_COLOR_BLUE,
   enGraphicsColor_Magenta = PCF_GFX_COLOR_MAGENTA,
   enGraphicsColor_Cyan = PCF_GFX_COLOR_CYAN,
   enGraphicsColor_White = PCF_GFX_COLOR_WHITE,
   enGraphicsColor_MaxValue = PCF_ENUM_MAXVALUE
}EnGraphicsColor_t;

typedef enum
{
   enGraphicsColorCode_Black = 0,
   enGraphicsColorCode_DarkRed = 1,
   enGraphicsColorCode_DarkGreen = 2,
   enGraphicsColorCode_DarkBlue = 3,
   enGraphicsColorCode_DarkYellow = 4,
   enGraphicsColorCode_DarkMagenta = 5,
   enGraphicsColorCode_DarkCyan = 6,
   enGraphicsColorCode_LightGray = 7,
   enGraphicsColorCode_MoneyGreen = 8,
   enGraphicsColorCode_SkyBlue = 9,
   enGraphicsColorCode_Cream = 10,
   enGraphicsColorCode_MediumGray = 11,
   enGraphicsColorCode_DarkGray = 12,
   enGraphicsColorCode_Red = 13,
   enGraphicsColorCode_Green = 14,
   enGraphicsColorCode_Yellow = 15,
   enGraphicsColorCode_Blue = 16,
   enGraphicsColorCode_Magenta = 17,
   enGraphicsColorCode_Cyan = 18,
   enGraphicsColorCode_White = 19,
   enGraphicsColorCode_Custom = 20,
   enGraphicsColorCode_MaxValue = PCF_ENUM_MAXVALUE
}EnGraphicsColorCode_t;

typedef union _pcf_graphics_color_t
{
    uint32_t value;
    struct
    {
        uint8_t alpha;
        uint8_t blue;
        uint8_t green;
        uint8_t red;
    }abgr;
    struct
    {
        uint8_t alpha;
        uint8_t red;
        uint8_t green;
        uint8_t blue;
    }argb;
}GraphicsColor_t;

typedef struct
{
    PcfMemoryConfig_t memConfig;
    void* extra;
}GraphicsConfig_t;

typedef union
{
    struct
    {
        uint8_t  platform;
        uint8_t  major;
        uint16_t minor;
    }Elements;
    uint32_t value;
}GraphicsVersion_t;

typedef struct _pcf_graphics_context2D_t GraphicsContext2D_t;
//Following type isn't meant to be used by the end user.
//it is provided for creation of the rendering context
//The context info will be provided by the UI layer or the Device layer.
typedef struct _pcf_graphics_context2DInfo_t GraphicsContext2DInfo_t;

PCF_GRAPHICS_END_CDECLARE

#endif // _PCF_GRAPHICS_TYPES_H_INCLUDED__
