#ifndef __PCF_TEMP_CONFIG_H_INCLUDED__
#define __PCF_TEMP_CONFIG_H_INCLUDED__

#include <stdint.h>
#include <stddef.h>



#ifdef __cplusplus
#define PCF_BEGIN_CDECLARE extern "C"{
#define PCF_END_CDECLARE   }
#else
#define PCF_BEGIN_CDECLARE
#define PCF_END_CDECLARE
#endif

#define PCF_FILE __FILE__
#define PCF_LINE __LINE__
#define PCF_FUNC __PRETTY_FUNCTION__

#define PCF_ENUM_SIZE     4
#define PCF_ENUM_MAXVALUE 0x7FFFFFFF


#define PCF_CDECL     __attribute__((cdecl))
#define PCF_STDCALL   __attribute__((stdcall))


#define PCF_PATH_TEXT_SIZE     260
#define PCF_PROPERTY_TEXT_SIZE 128

#define PCF_EXPORT_API  __declspec(dllexport)
#define PCF_IMPORT_API  __declspec(dllimport)
#define PCF_INLINE         static inline
#define PCF_ALWAYS_INLINE  static inline __attribute__((always_inline))
#define PCF_ARCH_SIZE      32

PCF_BEGIN_CDECLARE



//This is must for implementing the newer APIs
//Here we are making our life easier by typedefing the
//primitives like Points and Rects to Native Win32 which will
//remove the overhead of casting and copying it from our format.
//In order to know this we expect the PCF core to define the macro
//PCF_DEFINES_CORE_GRAPHICS_ELEMENTS as 0 or 1
//Define the following data types.
//gfx_int_t
//gfx_real_t
//GraphicsRectF_t, pcf_gfx_real_t rectangle
//GraphicsRect_t,  pcf_gfx_int_t rectangle
//GraphicsPointF_t pcf_gfx_real_t point
//GraphicsPoint_t  pcf_gfx_int_t point
//Following snippet shall move to core pcf
//Ensured that windows is included

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif // WIN32_LEAN_AND_MEAN

#define INITGUID  // Without this GUIDs are declared extern and fail to link
#define CINTERFACE
#define COBJMACROS
#ifndef _WIN32_IE
#define _WIN32_IE 0x500  // Workaround bug in shlwapi.h when compiling C++
                         // code with COBJMACROS.
#endif

#include <shlwapi.h>
#include <windows.h>
#include <wincodec.h>
#include <windowsx.h>
#include <gdiplus/gdiplus.h>
#include <gdiplus/gdiplusflat.h>
#include <gdiplus/gdiplustypes.h>

#define PCF_DEFINES_CORE_GRAPHICS_ELEMENTS 1

typedef int32_t pcf_status_t;

typedef enum
{
    enBoolean_False = 0,
    enBoolean_True = 1,
    enBoolean_MaxValue = PCF_ENUM_MAXVALUE
}EnBoolean_t;

typedef enum
{
    enLogType_Trace = 0,
    enLogType_Debug = 1,
    enLogType_Information = 2,
    enLogType_Warn = 3,
    enLogType_Error = 4,
    enLogType_Fatal = 5,
    enLogType_MaxValue = PCF_ENUM_MAXVALUE
}EnLogType_t;


typedef void* (PCF_CDECL *MemoryAlloc_fp)(size_t memSize,
                                               pcf_status_t* status);
typedef void* (PCF_CDECL *MemoryReAlloc_fp)(void* memory,
                                                 size_t memSize,
                                                 pcf_status_t* status);
typedef pcf_status_t (PCF_CDECL *MemoryFree_fp)(void* memory);

typedef struct _pcf_memory_config_t
{
    MemoryAlloc_fp   memory_alloc;
    MemoryReAlloc_fp memory_realloc;
    MemoryFree_fp    memory_free;
}PcfMemoryConfig_t;

typedef union
{
    uint32_t id;
    char     typeId[4];
}PcfObjectTypeId_t;

typedef struct
{
    uint8_t* bytes;
    size_t   size;
}Blob_t;

static const PcfObjectTypeId_t Int8Id = {.typeId = { '\0','I','8' }};
static const PcfObjectTypeId_t UInt8Id = {.typeId = { '\0','U','8' }};
static const PcfObjectTypeId_t Int16Id = {.typeId = { '\0','I','1', '6' }};
static const PcfObjectTypeId_t UInt16Id = {.typeId = { '\0','U','1', '6' }};
static const PcfObjectTypeId_t Int32Id = {.typeId = { '\0','I','3', '2' }};
static const PcfObjectTypeId_t UInt32Id = {.typeId = { '\0','U','3', '2' }};
static const PcfObjectTypeId_t Int64Id = {.typeId = { '\0','I','6', '4' }};
static const PcfObjectTypeId_t UInt64Id = {.typeId = { '\0','U','6', '4' }};
static const PcfObjectTypeId_t CharId = {.typeId = { '\0','C','H','R' }};
static const PcfObjectTypeId_t WCharId = {.typeId = { '\0','W', 'C', 'H' }};
static const PcfObjectTypeId_t FloatId = {.typeId = { '\0','F','L', 'T' }};
static const PcfObjectTypeId_t DoubleId = {.typeId = { '\0', 'D','B', 'L' }};
static const PcfObjectTypeId_t PropertyTextId = {.typeId = { '\0', 'P', 'T', 'X' }};
static const PcfObjectTypeId_t PathTextId = {.typeId = { '\0', 'P', 'T', 'H' }};
static const PcfObjectTypeId_t CStrId = {.typeId = { '\0', 'C', 'T', 'X', }};
static const PcfObjectTypeId_t BlobId = {.typeId = { '\0', 'B','L', 'B' }};
static const PcfObjectTypeId_t ObjectId = {.typeId = { '\0','O', 'B', 'T' }};

static const PcfObjectTypeId_t ListInt8Id = {.typeId = { 'L','I','8' }};
static const PcfObjectTypeId_t ListUInt8Id = {.typeId = { 'L','U','8' }};
static const PcfObjectTypeId_t ListInt16Id = {.typeId = { 'L','I','1', '6' }};
static const PcfObjectTypeId_t ListUInt16Id = {.typeId = { 'L','U','1', '6' }};
static const PcfObjectTypeId_t ListInt32Id = {.typeId = { 'L','I','3', '2' }};
static const PcfObjectTypeId_t ListUInt32Id = {.typeId = { 'L','U','3', '2' }};
static const PcfObjectTypeId_t ListInt64Id = {.typeId = { 'L','I','6', '4' }};
static const PcfObjectTypeId_t ListUInt64Id = {.typeId = { 'L','U','6', '4' }};
static const PcfObjectTypeId_t ListCharId = {.typeId = { 'L','C','H','R' }};
static const PcfObjectTypeId_t ListWCharId = {.typeId = { 'L','W', 'C', 'H' }};
static const PcfObjectTypeId_t ListFloatId = {.typeId = { 'L','F', 'L', 'T' }};
static const PcfObjectTypeId_t ListDoubleId = {.typeId = { 'L', 'D','B', 'L' }};
static const PcfObjectTypeId_t ListPropertyTextId = {.typeId = { 'L', 'P', 'T', 'X' }};
static const PcfObjectTypeId_t ListPathTextId = {.typeId = { 'L', 'P', 'T', 'H' }};
static const PcfObjectTypeId_t ListCStrId = {.typeId = { 'L', 'C', 'T', 'X', }};
static const PcfObjectTypeId_t ListBlobId = {.typeId = { 'L', 'B','L', 'B' }};
static const PcfObjectTypeId_t ListObjectId = {.typeId = { 'L','O', 'B', 'T' }};

typedef uint32_t hash_t;

typedef void Object_t;

typedef pcf_status_t    (*PcfObjectHashHandler_fp)(const Object_t* obj, hash_t* result);
typedef pcf_status_t    (*PcfObjectComparisonHandler_fp)(const Object_t* objectA, const Object_t* objectB,
                                                         int32_t* result);
typedef pcf_status_t    (*PcfObjectEqualityHandler_fp)(const Object_t* objectA, const Object_t* objectB,
                                                       EnBoolean_t* result);
typedef pcf_status_t    (*PcfObjectValidationHandler_fp)(const Object_t* object,
                                                         EnBoolean_t* result);
typedef Object_t*       (*PcfObjectDefaultCtor_fp)(pcf_status_t* status);
typedef pcf_status_t    (*PcfObjectDtor_fp)(Object_t* instance);
typedef const char*     (*PcfObjectToStringHandler_fp)(const Object_t* instance, pcf_status_t* status);
typedef pcf_status_t    (*PcfObjectFromStringHandler_fp)(Object_t* instance, const char* text,
                                                           EnBoolean_t* result);
typedef Object_t*       (*PcfObjectCloningHandler_fp)(const Object_t* instance,  pcf_status_t* status);

typedef uint8_t*        (*PcfObjectToBlobHandler_fp)(const Object_t* instance, size_t* bufferSize, pcf_status_t* result);
typedef pcf_status_t    (*PcfObjectFromBlobHandler_fp)(Object_t* instance, uint8_t* buffer,
                                                            size_t bufferSize, EnBoolean_t* result);
typedef const void*     (*PcfObjectPropertyGetter_fp)(Object_t* instance, const char* attributeName, pcf_status_t* result);
typedef pcf_status_t    (*PcfObjectPropertySetter_fp)(Object_t* instance, const char* attributeName, void* attributeValue);


typedef struct
{
    PcfObjectTypeId_t                              TypeId;

    PcfObjectHashHandler_fp                        GetHashCode;

    PcfObjectComparisonHandler_fp                  Compare;
    PcfObjectEqualityHandler_fp                    Equals;

    PcfObjectValidationHandler_fp                  IsValid;
    PcfObjectDefaultCtor_fp                        Ctor;
    PcfObjectDtor_fp                               Dtor;

    PcfObjectCloningHandler_fp                     Clone;

    PcfObjectPropertyGetter_fp                     GetProperty;
    PcfObjectPropertySetter_fp                     SetProperty;

    PcfObjectToStringHandler_fp                    ToString;
    PcfObjectToBlobHandler_fp                      ToBlob;
    PcfObjectFromStringHandler_fp                  FromString;
    PcfObjectFromBlobHandler_fp                    FromBlob;
}PcfObjectVTable_t;

PCF_END_CDECLARE
#endif // __PCF_TEMP_CONFIG_H_INCLUDED__
