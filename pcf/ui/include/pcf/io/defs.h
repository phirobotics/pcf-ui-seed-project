#ifndef __PCF_IO_DEFS_H_INCLUDED__
#define __PCF_IO_DEFS_H_INCLUDED__

#include <pcf/ui/defs.h>

#define PCF_IO_BEGIN_CDECLARE PCF_UI_BEGIN_CDECLARE
#define PCF_IO_END_CDECLARE   PCF_UI_BEGIN_CDECLARE
#define PCF_IO_API            PCF_UI_API

#endif // __PCF_IO_DEFS_H_INCLUDED__
