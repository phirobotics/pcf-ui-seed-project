#ifndef __PCF_IO_PATH_DEFS_H_INCLUDED__
#define __PCF_IO_PATH_DEFS_H_INCLUDED__

#include <pcf/io/defs.h>

#ifdef PCF_PATH_MAX_LENGTH
#error "Don't define macro PCF_PATH_MAX_LENGTH anywhere else."
#endif // PCF_PATH_MAX_LENGTH

#ifndef PCF_PATH_TEXT_SIZE
#define PCF_PATH_MAX_LENGTH 260
#else
#define PCF_PATH_MAX_LENGTH PCF_PATH_TEXT_SIZE
#endif // PCF_PATH_TEXT_SIZE

#endif // __PCF_IO_PATH_DEFS_H_INCLUDED__
