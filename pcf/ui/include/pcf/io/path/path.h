#ifndef __PCF_IO_PATH_H_INCLUDED__
#define __PCF_IO_PATH_H_INCLUDED__

#include <pcf/io/path/types.h>

PCF_IO_BEGIN_CDECLARE

EnBoolean_t  PCF_IO_API pcf_io_path_doesFileExist(const char* path);
EnBoolean_t  PCF_IO_API pcf_io_path_doesDirectoryExist(const char* path);
EnBoolean_t  PCF_IO_API pcf_io_path_isRooted(const char* path);
EnBoolean_t  PCF_IO_API pcf_io_path_hasSameRoot(const char* path1,
                                             const char* path2);

pcf_status_t PCF_IO_API pcf_io_path_addQuotes(char* path);
pcf_status_t PCF_IO_API pcf_io_path_addBackSlash(char* path);
pcf_status_t PCF_IO_API pcf_io_path_removeExtension(pcf_io_path_t path);
pcf_status_t PCF_IO_API pcf_io_path_normalize(pcf_io_path_t path);

pcf_status_t PCF_IO_API pcf_io_path_changeExtension(pcf_io_path_t path,
                                                 const char* newExtension);

pcf_status_t PCF_IO_API pcf_io_path_joinPath(const char* baseDir,
                                          const char* joiningPath,
                                          pcf_io_path_t path);

pcf_status_t PCF_IO_API pcf_io_path_appendPath(pcf_io_path_t basePath,
                                            const char* toAppend);

pcf_status_t PCF_IO_API pcf_io_path_getProgramFilesDirectory(pcf_io_path_t path);
pcf_status_t PCF_IO_API pcf_io_path_getSystemRootDirectory(pcf_io_path_t path);
pcf_status_t PCF_IO_API pcf_io_path_getUserAppDataDirectory(pcf_io_path_t path);
pcf_status_t PCF_IO_API pcf_io_path_getSystemName(pcf_io_path_t systemName);

pcf_status_t PCF_IO_API pcf_io_path_getFullPath(const char* path,
                                             pcf_io_path_t fullPath);

pcf_status_t PCF_IO_API pcf_io_path_getFileNameSegment(const char* path,
                                                    pcf_io_path_t fileNameSegment);

pcf_status_t PCF_IO_API pcf_io_path_getFileExtension(const char* path,
                                                  pcf_io_path_t extension);

pcf_status_t PCF_IO_API pcf_io_path_getDirectory(const char* path,
                                              pcf_io_path_t directory);

pcf_status_t PCF_IO_API pcf_io_path_createFilePath(const char* directoryPath,
                                                const char* onlyFileName,
                                                const char* extension,
                                                pcf_io_path_t path);

pcf_status_t PCF_IO_API pcf_io_path_getRelativePathOfDirectoryFromDirectory(const char* from,
                                            const char* to, pcf_io_path_t relativePath);

pcf_status_t PCF_IO_API pcf_io_path_getRelativePathOfFileFromDirectory(const char* from,
                                            const char* to, pcf_io_path_t relativePath);

pcf_status_t PCF_IO_API pcf_io_path_getRelativePathOfDirectoryFromFile(const char* from,
                                            const char* to, pcf_io_path_t relativePath);

pcf_status_t PCF_IO_API pcf_io_path_getRelativePathOfFileFromFile(const char* from,
                                            const char* to, pcf_io_path_t relativePath);

PCF_IO_END_CDECLARE

#endif // __PCF_IO_PATH_TYPES_H_INCLUDED__
