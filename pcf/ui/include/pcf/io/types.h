#ifndef __PCF_IO_TYPES_H_INCLUDED__
#define __PCF_IO_TYPES_H_INCLUDED__

#include <pcf/ui/types.h>
#include <pcf/io/defs.h>

PCF_IO_BEGIN_CDECLARE

typedef pcf_ui_path_text  pcf_io_path_t;

PCF_IO_END_CDECLARE

#endif // __PCF_IO_TYPES_H_INCLUDED__


