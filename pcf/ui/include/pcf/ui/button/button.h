#ifndef __PCF_UI_BUTTON_H_INCLUDED__
#define __PCF_UI_BUTTON_H_INCLUDED__
#include <pcf/ui/button/types.h>

PCF_UI_BEGIN_CDECLARE

UiButton_t*   PCF_UI_API pcf_ui_button_new(const UiControlInfo_t* args, pcf_status_t* result);
UiControl_t*  PCF_UI_API pcf_ui_button_asControl(UiButton_t* button);
pcf_status_t  PCF_UI_API pcf_ui_button_delete(UiButton_t* button);

pcf_status_t  PCF_UI_API pcf_ui_button_attachClickedEventHandler(UiButton_t* button,
                                                            UiButtonClickedEventHandler_fp handler);
pcf_status_t  PCF_UI_API pcf_ui_button_removeClickHandler(UiButton_t* button);


PCF_UI_END_CDECLARE

#endif // __PCF_UI_BUTTON_H_INCLUDED__
