#ifndef __PCF_UI_BUTTON_TYPES_H_INCLUDED__
#define __PCF_UI_BUTTON_TYPES_H_INCLUDED__

#include <pcf/ui/control/types.h>

PCF_UI_BEGIN_CDECLARE

typedef struct  _pcf_ui_button_t UiButton_t;

typedef void (PCF_STDCALL *UiButtonClickedEventHandler_fp)(UiButton_t* sender);

PCF_UI_END_CDECLARE
#endif // __PCF_UI_BUTTON_TYPES_H_INCLUDED__
