#ifndef __PCF_UI_CHECKBOX_H_INCLUDED__
#define __PCF_UI_CHECKBOX_H_INCLUDED__

#include <pcf/ui/checkbox/types.h>

PCF_UI_BEGIN_CDECLARE

UiCheckBox_t* PCF_UI_API pcf_ui_checkbox_new(const UiControlInfo_t* args, pcf_status_t* result);
UiControl_t*  PCF_UI_API pcf_ui_checkbox_asControl(UiCheckBox_t* checkbox);
pcf_status_t  PCF_UI_API pcf_ui_checkbox_delete(UiCheckBox_t* checkbox);

EnBoolean_t   PCF_UI_API pcf_ui_checkbox_isChecked(const UiCheckBox_t* checkbox, pcf_status_t* status);

pcf_status_t  PCF_UI_API pcf_ui_checkbox_attachStateChangeEventHandler(UiCheckBox_t* checkbox,
                                                                               UiCheckBoxChangedEventHandler_fp handler);
pcf_status_t  PCF_UI_API pcf_ui_checkbox_removeStateChangeEventHandler(UiCheckBox_t* checkbox);

PCF_UI_END_CDECLARE

#endif // __PCF_UI_CHECKBOX_H_INCLUDED__
