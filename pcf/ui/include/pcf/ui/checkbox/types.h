#ifndef __PCF_UI_CHECKBOX_TYPES_H_INCLUDED__
#define __PCF_UI_CHECKBOX_TYPES_H_INCLUDED__

#include <pcf/ui/control/types.h>

PCF_UI_BEGIN_CDECLARE

typedef struct  _pcf_ui_checkbox_t UiCheckBox_t;

typedef void (PCF_STDCALL *UiCheckBoxChangedEventHandler_fp)(const UiCheckBox_t* sender, EnBoolean_t checked);

PCF_UI_END_CDECLARE

#endif // __PCF_UI_CHECKBOX_TYPES_H_INCLUDED__
