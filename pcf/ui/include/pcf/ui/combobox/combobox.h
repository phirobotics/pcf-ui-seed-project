#ifndef __PCF_UI_COMBOBOX_H_INCLUDED__
#define __PCF_UI_COMBOBOX_H_INCLUDED__

#include <pcf/ui/combobox/types.h>

PCF_UI_BEGIN_CDECLARE

UiComboBox_t* PCF_UI_API pcf_ui_combobox_new(const UiControlInfo_t* args, pcf_status_t* result);
UiControl_t*  PCF_UI_API pcf_ui_combobox_asControl(UiComboBox_t* combobox);
pcf_status_t  PCF_UI_API pcf_ui_combobox_delete(UiComboBox_t* combobox);

pcf_status_t  PCF_UI_API pcf_ui_combobox_getItemTextCharLimit(UiComboBox_t* combobox, size_t* limit);

pcf_status_t  PCF_UI_API pcf_ui_combobox_addText(UiComboBox_t* combobox, const char* text, int32_t* resultIndex);
pcf_status_t  PCF_UI_API pcf_ui_combobox_removeTextAtIndex(UiComboBox_t* combobox, int32_t index);
pcf_status_t  PCF_UI_API pcf_ui_combobox_getItemText(const UiComboBox_t* combobox, int32_t index, pcf_ui_property_text text);


pcf_status_t  PCF_UI_API pcf_ui_combobox_attachSelectedIndexChangeEventHandler(UiComboBox_t* combobox,
                                                                               UiComboBoxItemIndexChangedEventHandler_fp handler);
pcf_status_t  PCF_UI_API pcf_ui_combobox_removeSelectedIndexChangeEventHandler(UiComboBox_t* combobox);

PCF_UI_END_CDECLARE

#endif // __PCF_UI_COMBOBOX_H_INCLUDED__
