#ifndef __PCF_UI_COMBOBOX_TYPES_H_INCLUDED__
#define __PCF_UI_COMBOBOX_TYPES_H_INCLUDED__

#include <pcf/ui/control/types.h>

PCF_UI_BEGIN_CDECLARE

typedef struct  _pcf_ui_combobox_t UiComboBox_t;

typedef void (PCF_STDCALL *UiComboBoxItemIndexChangedEventHandler_fp)(const UiComboBox_t* sender, int32_t oldIndex, int32_t newIndex);

PCF_UI_END_CDECLARE


#endif // __PCF_UI_COMBOBOX_TYPES_H_INCLUDED__
