#ifndef __PCF_UI_CONFIG_H_INCLUDED__
#define __PCF_UI_CONFIG_H_INCLUDED__


//Remove following and include core pcf headers
#include <pcf/config/config.h>

#ifdef PCF_UI_CONF_BUILD_STATICLIB
    #define PCF_UI_API
    #define PCF_UI_IS_BUILDING_SHAREDLIB 0

#elif defined(PCF_UI_CONF_BUILD_SHAREDLIB)
    #define PCF_UI_API PCF_EXPORT_API
    #define PCF_UI_IS_BUILDING_SHAREDLIB 1

#elif defined(PCF_UI_CONF_USING_SHAREDLIB)
    #define PCF_UI_API PCF_IMPORT_API
	#define PCF_UI_IS_BUILDING_SHAREDLIB 0

#elif defined(PCF_UI_CONF_USING_STATICLIB)
    #define PCF_UI_API
	#define PCF_UI_IS_BUILDING_SHAREDLIB 0

#else
#error "Define one of the above 4 macros."
#endif // PCF_UI_CONF_BUILD_STATICLIB

//#define PCF_UI_CONF_ENABLE_DEBUG



#endif // __PCF_UI_CONFIG_H_INCLUDED__
