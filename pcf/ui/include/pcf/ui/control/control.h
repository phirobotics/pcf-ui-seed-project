#ifndef __PCF_UI_CONTROL_H_INCLUDED__
#define __PCF_UI_CONTROL_H_INCLUDED__

#include <pcf/ui/control/types.h>
//Event Handlers
pcf_status_t PCF_UI_API pcf_ui_control_attachActiveChangeEventHandler(UiControl_t* control,
                                                        UiControlActiveChangedEventHandler_fp handler);
pcf_status_t PCF_UI_API  pcf_ui_control_removeActiveChangeEventHandler(UiControl_t* control);

pcf_status_t PCF_UI_API  pcf_ui_control_attachResizeEventHandler(UiControl_t* control,
                                                        UiControlResizedEventHandler_fp handler);
pcf_status_t PCF_UI_API  pcf_ui_control_removeResizeEventHandler(UiControl_t* control);

pcf_status_t PCF_UI_API  pcf_ui_control_attachMouseMoveEventHandler(UiControl_t* control,
                                                        UiControlMouseMoveEventHandler_fp handler);
pcf_status_t PCF_UI_API  pcf_ui_control_removeMouseMoveEventHandler(UiControl_t* control);

pcf_status_t PCF_UI_API  pcf_ui_control_attachMouseClickEventHandler(UiControl_t* control,
                                                        UiControlMouseClickEventHandler_fp handler);
pcf_status_t PCF_UI_API  pcf_ui_control_removeMouseClickEventHandler(UiControl_t* control);

pcf_status_t PCF_UI_API  pcf_ui_control_attachMouseDoubleClickEventHandler(UiControl_t* control,
                                                        UiControlMouseDoubleClickEventHandler_fp handler);
pcf_status_t PCF_UI_API  pcf_ui_control_removeMouseDoubleClickEventHandler(UiControl_t* control);

pcf_status_t PCF_UI_API  pcf_ui_control_attachDestroyedEventHandler(UiControl_t* control,
                                                        UiControlDestroyedEventHandler_fp handler);
pcf_status_t PCF_UI_API  pcf_ui_control_removeDestroyEventHandler(UiControl_t* control);

pcf_status_t PCF_UI_API  pcf_ui_control_attachInitEventHandler(UiControl_t* control,
                                                        UiControlInitEventHandler_fp handler);
pcf_status_t PCF_UI_API  pcf_ui_control_removeInitEventHandler(UiControl_t* control);

pcf_status_t PCF_UI_API  pcf_ui_control_attachUserDataChangeEventHandler(UiControl_t* control,
                                                        UiControlUserDataChangeEventHandler_fp handler);
pcf_status_t PCF_UI_API  pcf_ui_control_removeUserDataChangeEventHandler(UiControl_t* control);

//properties
//Util
EnBoolean_t  PCF_UI_API  pcf_ui_control_isValidTypeId(uint32_t typeId);
EnBoolean_t  PCF_UI_API  pcf_ui_control_canCreateControlWithTypeId(uint32_t typeId);
pcf_status_t PCF_UI_API  pcf_ui_control_convertTypeIdToText(uint32_t typeId, pcf_ui_property_text text);

pcf_status_t PCF_UI_API  pcf_ui_control_getTypeId(const UiControl_t* control, uint32_t* typeId);
EnBoolean_t  PCF_UI_API  pcf_ui_control_isEqual(const UiControl_t* controlA, const UiControl_t* controlB);
EnBoolean_t  PCF_UI_API  pcf_ui_control_isValid(const UiControl_t* control);
pcf_status_t PCF_UI_API  pcf_ui_control_getClientRect(const UiControl_t* control, GraphicsRect_t* rect);
pcf_status_t PCF_UI_API  pcf_ui_control_invalidate(UiControl_t* control);

pcf_status_t PCF_UI_API  pcf_ui_control_getControlRect(const UiControl_t* control, GraphicsRect_t* rect);
pcf_status_t PCF_UI_API  pcf_ui_control_moveTo(UiControl_t* control, int32_t x, int32_t y);
pcf_status_t PCF_UI_API  pcf_ui_control_resize(UiControl_t* control, uint32_t width, uint32_t height);
//get/set

pcf_status_t  PCF_UI_API  pcf_ui_control_getMaxSize(const UiControl_t* control, GraphicsSize_t* size);
pcf_status_t  PCF_UI_API  pcf_ui_control_getMinSize(const UiControl_t* control, GraphicsSize_t* size);

const void*  PCF_UI_API  pcf_ui_control_getUserData(const UiControl_t* control, pcf_status_t* status);
pcf_status_t PCF_UI_API  pcf_ui_control_setUserData(UiControl_t* control, void* data);

pcf_status_t PCF_UI_API  pcf_ui_control_getText(const UiControl_t* control, pcf_ui_property_text text);
pcf_status_t PCF_UI_API  pcf_ui_control_setText(UiControl_t* control, const char* text);

pcf_status_t PCF_UI_API  pcf_ui_control_getRect(const UiControl_t* control, GraphicsRect_t* rect);
pcf_status_t PCF_UI_API  pcf_ui_control_setRect(UiControl_t* control, const GraphicsRect_t* args);

EnBoolean_t  PCF_UI_API  pcf_ui_control_isVisibile(const UiControl_t* control, pcf_status_t* status);
pcf_status_t PCF_UI_API  pcf_ui_control_setVisibility(UiControl_t* control, EnBoolean_t args);

pcf_status_t PCF_UI_API  pcf_ui_control_getAnchor(const UiControl_t* control, EnControlAnchorFlag_t* anchor);
pcf_status_t PCF_UI_API  pcf_ui_control_setAnchor(UiControl_t* control, EnControlAnchorFlag_t anchor);

EnBoolean_t  PCF_UI_API  pcf_ui_control_isFocused(const UiControl_t* control, pcf_status_t* status);
pcf_status_t PCF_UI_API  pcf_ui_control_setFocused(UiControl_t* control);

EnBoolean_t  PCF_UI_API  pcf_ui_control_isEnabled(const UiControl_t* control, pcf_status_t* status);
pcf_status_t PCF_UI_API  pcf_ui_control_setEnabled(UiControl_t* control, EnBoolean_t args);




#endif // __PCF_UI_CONTROL_H_INCLUDED__
