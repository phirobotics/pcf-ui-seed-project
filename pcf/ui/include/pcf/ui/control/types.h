#ifndef __PCF_UI_CONTROL_TYPES_H_INCLUDED__
#define __PCF_UI_CONTROL_TYPES_H_INCLUDED__

#include <pcf/ui/types.h>
#include <pcf/ui/control/defs.h>

PCF_UI_BEGIN_CDECLARE
typedef enum
{
   enControlTypeIdValue_Mainform = PCF_UI_MAINFORM_CONTROL_ID, //0
   enControlTypeIdValue_Form = PCF_UI_FORM_CONTROL_ID, //1
   enControlTypeIdValue_TabControl = PCF_UI_TABCONTROL_CONTROL_ID, //2
   enControlTypeIdValue_TabPage = PCF_UI_TABPAGE_CONTROL_ID, //3

   enControlTypeIdValue_SimpleButton = PCF_UI_SIMPLEBUTTON_CONTROL_ID,//4
   enControlTypeIdValue_CheckButton = PCF_UI_CHECKBUTTON_CONTROL_ID,//5
   enControlTypeIdValue_ImageButton = PCF_UI_IMAGEBUTTON_CONTROL_ID,//6
   enControlTypeIdValue_Label = PCF_UI_LABEL_CONTROL_ID,//7
   enControlTypeIdValue_Separator = PCF_UI_SEPARATOR_CONTROL_ID,//8

   enControlTypeIdValue_ComboBox = PCF_UI_SEPARATOR_CONTROL_ID,//9
   enControlTypeIdValue_CheckComboBox = PCF_UI_CHECKEDCOMBOBOX_CONTROL_ID,//10
   enControlTypeIdValue_CheckBox = PCF_UI_CHECKBOX_CONTROL_ID,//11
   enControlTypeIdValue_RadioButton = PCF_UI_RADIOBUTTON_CONTROL_ID,//12

   enControlTypeIdValue_Panel = PCF_UI_PANEL_CONTROL_ID,//13
   enControlTypeIdValue_GroupBox = PCF_UI_GROUPBOX_CONTROL_ID,//14

   enControlTypeIdValue_MenuBar = PCF_UI_MENUBAR_CONTROL_ID,//15
   enControlTypeIdValue_StatusBar = PCF_UI_STATUSBAR_CONTROL_ID,//16
   enControlTypeIdValue_ToolBar = PCF_UI_TOOLBAR_CONTROL_ID,//17
   enControlTypeIdValue_Trackbar = PCF_UI_TRACKBAR_CONTROL_ID, //18
   enControlTypeIdValue_Progressbar = PCF_UI_PROGRESSBAR_CONTROL_ID, //19
   enControlTypeIdValue_HScrollbar = PCF_UI_HSCROLLBAR_CONTROL_ID,//20
   enControlTypeIdValue_VScrollbar = PCF_UI_VSCROLLBAR_CONTROL_ID,//21

   enControlTypeIdValue_ImageView = PCF_UI_IMAGEVIEW_CONTROL_ID,//22
   enControlTypeIdValue_DrawingView = PCF_UI_DRAWINGVIEW_CONTROL_ID,//23
   enControlTypeIdValue_CamView = PCF_UI_CAMVIEW_CONTROL_ID,//24

   enControlTypeIdValue_ListBox = PCF_UI_LISTBOX_CONTROL_ID,//25
   enControlTypeIdValue_CheckedListBox = PCF_UI_CHECKEDLISTBOX_CONTROL_ID,//26
   enControlTypeIdValue_TreeView = PCF_UI_TREEVIEW_CONTROL_ID_INDEX,//27
   enControlTypeIdValue_TextEdit = PCF_UI_TEXTEDIT_CONTROL_ID, //28
   enControlTypeIdValueCount = PCF_UI_CONTROL_ID_INDEX_COUNT,
   enControlTypeIdValue_MaxValue = 0xFFFFFFFF

}EnControlTypeIdValue_t;

typedef enum
{
   enControlTypeIdIndex_None = -1,
   enControlTypeIdIndex_Mainform = 0,
   enControlTypeIdIndex_Form = 1,
   enControlTypeIdIndex_TabControl = 2,
   enControlTypeIdIndex_TabPage = 3,
   enControlTypeIdIndex_SimpleButton = 4,
   enControlTypeIdIndex_CheckButton = 5,
   enControlTypeIdIndex_ImageButton = 6,
   enControlTypeIdIndex_Label = 7,
   enControlTypeIdIndex_Separator = 8,
   enControlTypeIdIndex_ComboBox = 9,
   enControlTypeIdIndex_CheckComboBox = 10,
   enControlTypeIdIndex_CheckBox = 11,
   enControlTypeIdIndex_RadioButton = 12,
   enControlTypeIdIndex_Panel = 13,
   enControlTypeIdIndex_GroupBox = 14,
   enControlTypeIdIndex_MenuBar = 15,
   enControlTypeIdIndex_StatusBar = 16,
   enControlTypeIdIndex_ToolBar = 17,
   enControlTypeIdIndex_Trackbar = 18,
   enControlTypeIdIndex_Progressbar = 19,
   enControlTypeIdIndex_HScrollbar = 20,
   enControlTypeIdIndex_VScrollbar = 21,
   enControlTypeIdIndex_ImageView = 22,
   enControlTypeIdIndex_DrawingView = 23,
   enControlTypeIdIndex_CamView = 24,
   enControlTypeIdIndex_ListBox = 25,
   enControlTypeIdIndex_CheckedListBox = 26,
   enControlTypeIdIndex_TreeView = 27,
   enControlTypeIdIndex_TextEdit = 28,
   enControlTypeIdIndexCount = PCF_UI_CONTROL_ID_INDEX_COUNT,
   enControlTypeIdIndex_MaxValue = 0x7FFFFFFF
}EnControlTypeIdIndex_t;

typedef void (PCF_STDCALL *UiControlActiveChangedEventHandler_fp)(const UiControl_t* sender,
                                                                EnBoolean_t active);
typedef void (PCF_STDCALL *UiControlMouseDoubleClickEventHandler_fp)(const UiControl_t* sender,
                                                                   const UiControlMouseEventArgs_t* eventArgs);

typedef void (PCF_STDCALL *UiControlMouseClickEventHandler_fp)(const UiControl_t* sender,
                                                             const UiControlMouseClickEventArgs_t* eventArgs);

typedef void (PCF_STDCALL *UiControlMouseMoveEventHandler_fp)(const UiControl_t* sender,
                                                           const UiControlMouseEventArgs_t* eventArgs);

typedef void (PCF_STDCALL *UiControlResizedEventHandler_fp)(const UiControl_t* sender,
                                                          const UiControlPositionSizeEventArgs_t* eventArgs);

typedef void (PCF_STDCALL *UiControlDestroyedEventHandler_fp)(const UiControl_t* sender);

typedef void (PCF_STDCALL *UiControlInitEventHandler_fp)(const UiControl_t* sender);

typedef EnBoolean_t (PCF_STDCALL *UiControlUserDataChangeEventHandler_fp)(const UiControl_t* sender,
                                                                        const void* oldData, const void* newData);



PCF_UI_END_CDECLARE
#endif // __PCF_UI_CONTROL_TYPES_H_INCLUDED__
