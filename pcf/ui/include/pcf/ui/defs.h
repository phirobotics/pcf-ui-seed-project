#ifndef __PCF_UI_DEFS_H_INCLUDED__
#define __PCF_UI_DEFS_H_INCLUDED__

#include <pcf/ui/config.h>
#include <pcf/graphics/defs.h>

#ifndef PCF_UI_API
#error "Required macro PCF_UI_API is missing."
#endif // PCF_UI_API

#ifndef PCF_UI_IS_BUILDING_SHAREDLIB
#error "Required macro PCF_UI_IS_BUILDING_SHAREDLIB is missing."
#endif // PCF_UI_IS_BUILDING_SHAREDLIB

#ifdef PCF_UI_CONF_ENABLE_DEBUG
#define PCF_UI_ENABLE_DEBUG 1
#else
#define PCF_UI_ENABLE_DEBUG 0
#endif // PCF_UI_CONF_ENABLE_DEBUG
#undef PCF_UI_CONF_ENABLE_DEBUG

#ifndef PCF_BEGIN_CDECLARE
#error "Required macro PCF_BEGIN_CDECLARE is missing."
#endif // PCF_BEGIN_CDECLARE

#ifndef PCF_END_CDECLARE
#error "Required macro PCF_END_CDECLARE is missing."
#endif // PCF_END_CDECLARE

#ifndef PCF_FILE
#error "Required macro PCF_FILE is missing."
#endif // PCF_FILE

#ifndef PCF_LINE
#error "Required macro PCF_LINE is missing."
#endif // PCF_LINE

#ifndef PCF_FUNC
#error "Required macro PCF_FUNC is missing."
#endif // PCF_FUNC

#ifndef PCF_ENUM_SIZE
#error "Required macro PCF_ENUM_SIZE is missing."
#endif // PCF_ENUM_SIZE

#ifndef PCF_ENUM_MAXVALUE
#error "Required macro PCF_ENUM_MAXVALUE is missing."
#endif // PCF_ENUM_MAXVALUE

#ifndef PCF_CDECL
#error "Required macro PCF_CDECL is missing."
#endif // PCF_CDECL

#ifndef PCF_STDCALL
#error "Required macro PCF_STDCALL is missing."
#endif // PCF_STDCALL

#ifndef PCF_INLINE
#error "Required macro PCF_INLINE is missing."
#endif // PCF_INLINE

#ifndef PCF_PATH_TEXT_SIZE
#error "Required macro PCF_PATH_TEXT_SIZE is missing."
#endif // PCF_PATH_TEXT_SIZE

#ifndef PCF_PROPERTY_TEXT_SIZE
#error "Required macro PCF_PROPERTY_TEXT_SIZE is missing."
#endif // PCF_PROPERTY_TEXT_SIZE

#define PCF_UI_BEGIN_CDECLARE  PCF_BEGIN_CDECLARE
#define PCF_UI_END_CDECLARE    PCF_END_CDECLARE

#ifdef PCF_UI_StatusCode_None
#error "Don't define the macro PCF_UI_StatusCode_None on your own, it is reserved."
#endif // PCF_UI_StatusCode_None
#define PCF_UI_StatusCode_None                   PCF_GFX_StatusCode_None

#ifdef PCF_UI_StatusCode_Error
#error "Don't define the macro PCF_UI_StatusCode_Error on your own, it is reserved."
#endif // PCF_UI_StatusCode_Error
#define PCF_UI_StatusCode_Error                  PCF_GFX_StatusCode_Error

#ifdef PCF_UI_StatusCode_InvalidArg
#error "Don't define the macro PCF_UI_StatusCode_InvalidArg on your own, it is reserved."
#endif // PCF_UI_StatusCode_InvalidArg
#define PCF_UI_StatusCode_InvalidArg             PCF_GFX_StatusCode_InvalidArg

#ifdef PCF_UI_StatusCode_NullArg
#error "Don't define the macro PCF_UI_StatusCode_NullArg on your own, it is reserved."
#endif // PCF_UI_StatusCode_NullArg
#define PCF_UI_StatusCode_NullArg                PCF_GFX_StatusCode_NullArg

#ifdef PCF_UI_StatusCode_NotImplemented
#error "Don't define the macro PCF_UI_StatusCode_NotImplemented on your own, it is reserved."
#endif // PCF_UI_StatusCode_NotImplemented
#define PCF_UI_StatusCode_NotImplemented         PCF_GFX_StatusCode_NotImplemented

#ifdef PCF_UI_StatusCode_NotSupported
#error "Don't define the macro PCF_UI_StatusCode_NotSupported on your own, it is reserved."
#endif // PCF_UI_StatusCode_NotSupported
#define PCF_UI_StatusCode_NotSupported           PCF_GFX_StatusCode_NotSupported

#ifdef PCF_UI_StatusCode_NotEnoughMemory
#error "Don't define the macro PCF_UI_StatusCode_NotEnoughMemory on your own, it is reserved."
#endif // PCF_UI_StatusCode_NotEnoughMemory
#define PCF_UI_StatusCode_NotEnoughMemory        PCF_GFX_StatusCode_NotEnoughMemory

#ifdef PCF_UI_StatusCode_AllocationError
#error "Don't define the macro PCF_UI_StatusCode_AllocationError on your own, it is reserved."
#endif // PCF_UI_StatusCode_AllocationError
#define PCF_UI_StatusCode_AllocationError        PCF_GFX_StatusCode_AllocationError

#ifdef PCF_UI_StatusCode_InvalidCapacity
#error "Don't define the macro PCF_UI_StatusCode_InvalidCapacity on your own, it is reserved."
#endif // PCF_UI_StatusCode_InvalidCapacity
#define PCF_UI_StatusCode_InvalidCapacity        PCF_GFX_StatusCode_InvalidCapacity

#ifdef PCF_UI_StatusCode_InvalidRange
#error "Don't define the macro PCF_UI_StatusCode_InvalidRange on your own, it is reserved."
#endif // PCF_UI_StatusCode_InvalidRange
#define PCF_UI_StatusCode_InvalidRange           PCF_GFX_StatusCode_InvalidRange

#ifdef PCF_UI_StatusCode_MaxCapacity
#error "Don't define the macro PCF_UI_StatusCode_MaxCapacity on your own, it is reserved."
#endif // PCF_UI_StatusCode_MaxCapacity
#define PCF_UI_StatusCode_MaxCapacity            PCF_GFX_StatusCode_MaxCapacity


#ifdef PCF_UI_StatusCode_ResourceNotFound
#error "Don't define the macro PCF_UI_StatusCode_ResourceNotFound on your own, it is reserved."
#endif // PCF_UI_StatusCode_ResourceNotFound
#define PCF_UI_StatusCode_ResourceNotFound       PCF_GFX_StatusCode_ResourceNotFound

#ifdef PCF_UI_StatusCode_IndexOutOfRange
#error "Don't define the macro PCF_UI_StatusCode_IndexOutOfRange on your own, it is reserved."
#endif // PCF_UI_StatusCode_IndexOutOfRange
#define PCF_UI_StatusCode_IndexOutOfRange        PCF_GFX_StatusCode_IndexOutOfRange

#ifdef PCF_UI_StatusCode_InvalidOperation
#error "Don't define the macro PCF_UI_StatusCode_InvalidOperation on your own, it is reserved."
#endif // PCF_UI_StatusCode_InvalidOperation
#define PCF_UI_StatusCode_InvalidOperation       PCF_GFX_StatusCode_InvalidOperation

#ifdef PCF_UI_StatusCode_FileDoesntExist
#error "Don't define the macro PCF_UI_StatusCode_FileDoesntExist on your own, it is reserved."
#endif // PCF_UI_StatusCode_FileDoesntExist
#define PCF_UI_StatusCode_FileDoesntExist        PCF_GFX_StatusCode_FileDoesntExist

#ifdef PCF_UI_StatusCode_FileFormatNotSupported
#error "Don't define the macro PCF_UI_StatusCode_FileFormatNotSupported on your own, it is reserved."
#endif // PCF_UI_StatusCode_FileFormatNotSupported
#define PCF_UI_StatusCode_FileFormatNotSupported PCF_GFX_StatusCode_FileFormatNotSupported

#ifdef PCF_UI_StatusCode_InvalidFileFormat
#error "Don't define the macro PCF_UI_StatusCode_InvalidFileFormat on your own, it is reserved."
#endif // PCF_UI_StatusCode_InvalidFileFormat
#define PCF_UI_StatusCode_InvalidFileFormat     PCF_GFX_StatusCode_InvalidFileFormat

#ifdef PCF_UI_StatusCode_InternalError
#error "Don't define the macro PCF_UI_StatusCode_InternalError on your own, it is reserved."
#endif // PCF_UI_StatusCode_InternalError
#define PCF_UI_StatusCode_InternalError         PCF_GFX_StatusCode_InternalError

#ifdef PCF_UI_StatusCode_NotAllowed
#error "Don't define the macro PCF_UI_StatusCode_NotAllowed on your own, it is reserved."
#endif // PCF_UI_StatusCode_NotAllowed
#define PCF_UI_StatusCode_NotAllowed            PCF_GFX_StatusCode_NotAllowed

#ifdef PCF_UI_StatusCode_InvalidState
#error "Don't define the macro PCF_UI_StatusCode_InvalidState on your own, it is reserved."
#endif // PCF_UI_StatusCode_InvalidState
#define PCF_UI_StatusCode_InvalidState          PCF_GFX_StatusCode_InvalidState

#ifdef PCF_UI_StatusCode_BoundaryStart
#error "Don't define the macro PCF_UI_StatusCode_BoundaryStart on your own, it is reserved."
#endif // PCF_UI_StatusCode_BoundaryStart
#define PCF_UI_StatusCode_BoundaryStart         (1000)

#ifdef PCF_UI_StatusCode_WidgetRegistrationError
#error "Don't define the macro PCF_UI_StatusCode_WidgetRegistrationError on your own, it is reserved."
#endif // PCF_UI_StatusCode_WidgetRegistrationError
#define PCF_UI_StatusCode_WidgetRegistrationError (PCF_UI_StatusCode_InvalidState - PCF_UI_StatusCode_BoundaryStart - 1)

#ifdef PCF_UI_StatusCode_WidgetCreationError
#error "Don't define the macro PCF_UI_StatusCode_WidgetCreationError on your own, it is reserved."
#endif // PCF_UI_StatusCode_WidgetCreationError
#define PCF_UI_StatusCode_WidgetCreationError     (PCF_UI_StatusCode_WidgetRegistrationError - 1)

#ifdef PCF_UI_PROPERTY_TEXT_SIZE
#error "Pcf Graphics reserved macro PCF_UI_PROPERTY_TEXT_SIZE is \
pre-defined."
#endif // PCF_UI_PROPERTY_TEXT_SIZE

#if   (PCF_PROPERTY_TEXT_SIZE < 17)
#define PCF_UI_PROPERTY_TEXT_SIZE 16
#elif (PCF_PROPERTY_TEXT_SIZE < 33)
#define PCF_UI_PROPERTY_TEXT_SIZE 32
#elif (PCF_PROPERTY_TEXT_SIZE < 65)
#define PCF_UI_PROPERTY_TEXT_SIZE 64
#elif (PCF_PROPERTY_TEXT_SIZE < 129)
#define PCF_UI_PROPERTY_TEXT_SIZE 128
#else
#define PCF_UI_PROPERTY_TEXT_SIZE 256
#endif


#if !PCF_UI_ENABLE_DEBUG
#define PCF_UI_TRACE(...)
#define PCF_UI_DEBUG(...)
#define PCF_UI_INFO(...)
#define PCF_UI_WARN(...)
#define PCF_UI_ERROR(...)
#define PCF_UI_FATAL(...)
#define PCF_UI_ASSERT_NONULL(expression)
#define PCF_UI_ASSERT_NULL(expression)
#define PCF_UI_ASSERT(expression)
#else
#define PCF_UI_TRACE(...)  pcf_ui_log(enLogType_Trace, PCF_FILE, PCF_FUNC, PCF_LINE, __VA_ARGS__);
#define PCF_UI_DEBUG(...)  pcf_ui_log(enLogType_Debug, PCF_FILE, PCF_FUNC, PCF_LINE, __VA_ARGS__);
#define PCF_UI_INFO(...)   pcf_ui_log(enLogType_Information, PCF_FILE, PCF_FUNC, PCF_LINE, __VA_ARGS__);
#define PCF_UI_WARN(...)   pcf_ui_log(enLogType_Warn, PCF_FILE, PCF_FUNC, PCF_LINE, __VA_ARGS__);
#define PCF_UI_ERROR(...)  pcf_ui_log(enLogType_Error, PCF_FILE, PCF_FUNC, PCF_LINE, __VA_ARGS__);
#define PCF_UI_FATAL(...)  pcf_ui_log(enLogType_Fatal, PCF_FILE, PCF_FUNC, PCF_LINE, __VA_ARGS__);

#define PCF_UI_ASSERT_NONULL(expression) \
    if(!(expression)) \
    {\
        pcf_ui_log(enLogType_Trace, PCF_FILE, PCF_FUNC, PCF_LINE, "%s is NULL", #expression);\
    }\

#define PCF_UI_ASSERT_NULL(expression) \
    if(expression) \
    {\
        pcf_ui_log(enLogType_Trace, PCF_FILE, PCF_FUNC, PCF_LINE, "%s is NOT NULL", #expression);\
    }\

#define PCF_UI_ASSERT(expression) \
    if(!(expression)) \
    {\
        pcf_ui_log(enLogType_Trace, PCF_FILE, PCF_FUNC, PCF_LINE, "'%s' has failed", #expression);\
    }\

#endif // PCF_UI_ENABLE_DEBUG

#endif // __PCF_UI_DEFS_H_INCLUDED__
