#ifndef __PCF_UI_DRAWINGVIEW_H_INCLUDED__
#define __PCF_UI_DRAWINGVIEW_H_INCLUDED__

#include <pcf/ui/drawingview/types.h>

PCF_UI_BEGIN_CDECLARE

UiDrawingView_t* PCF_UI_API pcf_ui_drawingView_new(const UiControlInfo_t* args, pcf_status_t* result);
UiControl_t*     PCF_UI_API pcf_ui_drawingView_asControl(UiDrawingView_t* drawingView);
pcf_status_t     PCF_UI_API pcf_ui_drawingView_delete(UiDrawingView_t* drawingView);

pcf_status_t     PCF_UI_API pcf_ui_drawingView_attachPaintEventHandler(UiDrawingView_t* drawingView, UiDrawingViewPaintEventHandler_fp handler);
pcf_status_t     PCF_UI_API pcf_ui_drawingView_removePaintEventHandler(UiDrawingView_t* drawingView);

PCF_UI_END_CDECLARE

#endif // __PCF_UI_DRAWINGVIEW_H_INCLUDED__
