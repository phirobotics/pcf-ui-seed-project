#ifndef __PCF_UI_DRAWINGVIEW_TYPES_H_INCLUDED__
#define __PCF_UI_DRAWINGVIEW_TYPES_H_INCLUDED__

#include <pcf/ui/control/types.h>
#include <pcf/graphics/context2d/types.h>
PCF_UI_BEGIN_CDECLARE

typedef struct  _pcf_ui_drawingView_t UiDrawingView_t;

typedef void (PCF_STDCALL *UiDrawingViewPaintEventHandler_fp)(UiDrawingView_t* view, UiPaintEventArgs_t* e);

PCF_UI_END_CDECLARE

#endif // __PCF_UI_DRAWINGVIEW_TYPES_H_INCLUDED__
