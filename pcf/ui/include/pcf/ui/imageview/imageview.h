#ifndef __PCF_UI_IMAGEVIEW_H_INCLUDED__
#define __PCF_UI_IMAGEVIEW_H_INCLUDED__

#include <pcf/ui/imageview/types.h>

PCF_UI_BEGIN_CDECLARE

UiImageView_t* PCF_UI_API pcf_ui_imageView_new(const UiControlInfo_t* args, pcf_status_t* result);
UiControl_t*   PCF_UI_API pcf_ui_imageView_asControl(UiImageView_t* imageView);
pcf_status_t   PCF_UI_API pcf_ui_imageView_delete(UiImageView_t* imageView);

pcf_status_t   PCF_UI_API pcf_ui_imageView_show(UiImageView_t* imageView, const char* imageFile);

PCF_UI_END_CDECLARE

#endif // __PCF_UI_IMAGEVIEW_H_INCLUDED__
