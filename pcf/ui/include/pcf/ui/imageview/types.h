#ifndef __PCF_UI_IMAGEVIEW_TYPES_H_INCLUDED__
#define __PCF_UI_IMAGEVIEW_TYPES_H_INCLUDED__

#include <pcf/ui/control/types.h>

PCF_UI_BEGIN_CDECLARE

typedef struct  _pcf_ui_imageView_t UiImageView_t;

PCF_UI_END_CDECLARE
#endif // __PCF_UI_IMAGEVIEW_TYPES_H_INCLUDED__
