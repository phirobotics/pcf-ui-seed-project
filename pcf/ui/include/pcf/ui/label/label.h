#ifndef __PCF_UI_LABEL_H_INCLUDED__
#define __PCF_UI_LABEL_H_INCLUDED__

#include <pcf/ui/label/types.h>
#include <pcf/graphics/types.h>

PCF_UI_BEGIN_CDECLARE

UiLabel_t*   PCF_UI_API pcf_ui_label_new(const UiControlInfo_t* args, pcf_status_t* result);
UiControl_t* PCF_UI_API pcf_ui_label_asControl(UiLabel_t* label);
pcf_status_t PCF_UI_API pcf_ui_label_delete(UiLabel_t* label);

pcf_status_t PCF_UI_API pcf_ui_label_getTextColor(const UiLabel_t* label, EnGraphicsColorCode_t* code);
pcf_status_t PCF_UI_API pcf_ui_label_setTextColor(UiLabel_t* label, EnGraphicsColorCode_t code);

pcf_status_t PCF_UI_API pcf_ui_label_getBackgroundColor(const UiLabel_t* label, EnGraphicsColorCode_t* code);
pcf_status_t PCF_UI_API pcf_ui_label_setBackgroundColor(UiLabel_t* label, EnGraphicsColorCode_t code);

pcf_status_t PCF_UI_API pcf_ui_label_getTextAlignment(const UiLabel_t* label, EnUiLabelTextAlignment_t* alignement);
pcf_status_t PCF_UI_API pcf_ui_label_setTextAlignment(UiLabel_t* label, EnUiLabelTextAlignment_t alignement);

PCF_UI_END_CDECLARE

#endif // __PCF_UI_LABEL_H_INCLUDED__
