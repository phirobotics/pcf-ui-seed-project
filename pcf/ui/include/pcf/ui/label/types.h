#ifndef __PCF_UI_LABEL_TYPES_H_INCLUDED__
#define __PCF_UI_LABEL_TYPES_H_INCLUDED__

#include <pcf/ui/control/types.h>

PCF_UI_BEGIN_CDECLARE
typedef enum
{
    enUiLabelTextAlignment_UnKnown,
    enUiLabelTextAlignment_Default,
    enUiLabelTextAlignment_Left,
    enUiLabelTextAlignment_Right,
    enUiLabelTextAlignment_Center,
    enUiLabelTextAlignment_MaxValue = PCF_ENUM_MAXVALUE
}EnUiLabelTextAlignment_t;

typedef struct  _pcf_ui_label_t UiLabel_t;

PCF_UI_END_CDECLARE

#endif // __PCF_UI_LABEL_TYPES_H_INCLUDED__
