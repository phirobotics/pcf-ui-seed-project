#ifndef __PCF_UI_MAINFORM_H_INCLUDED__
#define __PCF_UI_MAINFORM_H_INCLUDED__

#include <pcf/ui/mainform/types.h>

PCF_UI_BEGIN_CDECLARE

UiMainForm_t* PCF_UI_API pcf_ui_mainform_create(const UiControlInfo_t* info, pcf_status_t* result);
pcf_status_t  PCF_UI_API pcf_ui_mainform_configure(UiMainForm_t* mainform,  const UiMainformConfig_t* config);

UiControl_t*  PCF_UI_API pcf_ui_mainform_asControl(UiMainForm_t* mainform);

pcf_status_t  PCF_UI_API pcf_ui_mainform_run(const UiMainForm_t* mainform, int32_t* exitcode);

EnBoolean_t   PCF_UI_API pcf_ui_mainform_isMinimized(const UiMainForm_t* mainform, pcf_status_t* status);
EnBoolean_t   PCF_UI_API pcf_ui_mainform_isMaximize(const UiMainForm_t* mainform, pcf_status_t* status);
EnBoolean_t   PCF_UI_API pcf_ui_mainform_isNormal(const UiMainForm_t* mainform, pcf_status_t* status);

pcf_status_t  PCF_UI_API pcf_ui_mainform_minimize(UiMainForm_t* mainform);
pcf_status_t  PCF_UI_API pcf_ui_mainform_maximize(UiMainForm_t* mainform);
pcf_status_t  PCF_UI_API pcf_ui_mainform_setNormalSize(UiMainForm_t* mainform);

pcf_status_t  PCF_UI_API pcf_ui_mainform_attachMinizeHandler(UiMainForm_t* mainform, UiMainformMinizeEventHandler_fp handler);
pcf_status_t  PCF_UI_API pcf_ui_mainform_removeMinizeHandler(UiMainForm_t* mainform);

PCF_UI_END_CDECLARE

#endif // __PCF_UI_MAINFORM_H_INCLUDED__
