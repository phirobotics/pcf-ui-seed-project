#ifndef __PCF_UI_MAINFORM_TYPES_H_INCLUDED__
#define __PCF_UI_MAINFORM_TYPES_H_INCLUDED__

#include <pcf/ui/control/types.h>

PCF_UI_BEGIN_CDECLARE

typedef struct  _pcf_ui_mainform_t UiMainForm_t;

typedef struct
{
    pcf_ui_property_text title;
}UiMainformConfig_t;

typedef void (PCF_STDCALL *UiMainformMinizeEventHandler_fp)(const UiMainForm_t* sender);

PCF_UI_END_CDECLARE

#endif // __PCF_UI_MAINFORM_TYPES_H_INCLUDED__
