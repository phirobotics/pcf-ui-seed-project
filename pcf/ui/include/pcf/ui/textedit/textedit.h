#ifndef __PCF_UI_TEXTEDIT_H_INCLUDED__
#define __PCF_UI_TEXTEDIT_H_INCLUDED__

#include <pcf/ui/textedit/types.h>
#include <pcf/graphics/types.h>

PCF_UI_BEGIN_CDECLARE

UiTextEdit_t*   PCF_UI_API pcf_ui_textedit_new(const UiControlInfo_t* args, pcf_status_t* result);
UiControl_t*    PCF_UI_API pcf_ui_textedit_asControl(UiTextEdit_t* label);
pcf_status_t    PCF_UI_API pcf_ui_textedit_delete(UiTextEdit_t* label);

pcf_status_t    PCF_UI_API pcf_ui_textedit_setDefaultType(UiTextEdit_t* edit);
pcf_status_t    PCF_UI_API pcf_ui_textedit_setType(UiTextEdit_t* edit, EnUiTextEditType_t type);
pcf_status_t    PCF_UI_API pcf_ui_textedit_getType(UiTextEdit_t* edit, EnUiTextEditType_t* type);

pcf_status_t    PCF_UI_API pcf_ui_textedit_setCharValidator(UiTextEdit_t* edit,
                                                            UiEditCharValidator_fp validator);
pcf_status_t    PCF_UI_API pcf_ui_textedit_removeCharValidator(UiTextEdit_t* edit);

pcf_status_t    PCF_UI_API pcf_ui_textedit_attachMaxTextEventHandler(UiTextEdit_t* edit,
                                                                     UiTextEditStateEventHandler_fp handler);
pcf_status_t    PCF_UI_API pcf_ui_textedit_removeMaxTextEventHandler(UiTextEdit_t* edit);

pcf_status_t    PCF_UI_API pcf_ui_textedit_attachUpdateEventHandler(UiTextEdit_t* edit,
                                                                    UiTextEditStateEventHandler_fp handler);
pcf_status_t    PCF_UI_API pcf_ui_textedit_removeUpdateEventHandler(UiTextEdit_t* edit);

pcf_status_t    PCF_UI_API pcf_ui_textedit_attachChangeEventHandler(UiTextEdit_t* edit,
                                                                    UiTextEditStateEventHandler_fp handler);
pcf_status_t    PCF_UI_API pcf_ui_textedit_removeChangeEventHandler(UiTextEdit_t* edit);

pcf_status_t    PCF_UI_API pcf_ui_textedit_attachMemoryErrorEventHandler(UiTextEdit_t* edit,
                                                                        UiTextEditStateEventHandler_fp handler);
pcf_status_t    PCF_UI_API pcf_ui_textedit_removeMemoryErrorEventHandler(UiTextEdit_t* edit);

pcf_status_t    PCF_UI_API pcf_ui_textedit_attachFocusChangeEventHandler(UiTextEdit_t* edit,
                                                                         UiTextEditFocusChangedEventHandler_fp handler);
pcf_status_t    PCF_UI_API pcf_ui_textedit_removeFocusChangeEventHandler(UiTextEdit_t* edit);

pcf_status_t    PCF_UI_API pcf_ui_textedit_setCharLimit(UiTextEdit_t* edit, int32_t limit);
pcf_status_t    PCF_UI_API pcf_ui_textedit_getCharLimit(UiTextEdit_t* edit, int32_t* limit);

pcf_status_t    PCF_UI_API pcf_ui_textedit_getSelectedTextRange(UiTextEdit_t* edit, int32_t* start, int32_t* end);
pcf_status_t    PCF_UI_API pcf_ui_textedit_setSelectedTextRange(UiTextEdit_t* edit, int32_t start, int32_t end);

pcf_status_t    PCF_UI_API pcf_ui_textedit_getCaretPosition(UiTextEdit_t* edit, int32_t* position);
pcf_status_t    PCF_UI_API pcf_ui_textedit_setCaretPosition(UiTextEdit_t* edit, int32_t position);

pcf_status_t    PCF_UI_API pcf_ui_textedit_clear(UiTextEdit_t* edit);
pcf_status_t    PCF_UI_API pcf_ui_textedit_appendText(UiTextEdit_t* edit, const char* text);
pcf_status_t    PCF_UI_API pcf_ui_textedit_getTextLength(UiTextEdit_t* edit, size_t* length);
pcf_status_t    PCF_UI_API pcf_ui_textedit_insertTextAt(UiTextEdit_t* edit, int32_t index, const char* text);

PCF_UI_END_CDECLARE

#endif // __PCF_UI_TEXTEDIT_H_INCLUDED__
