#ifndef __PCF_UI_TEXTEDIT_TYPES_H_INCLUDED__
#define __PCF_UI_TEXTEDIT_TYPES_H_INCLUDED__

#include <pcf/ui/control/types.h>

PCF_UI_BEGIN_CDECLARE

typedef struct  _pcf_ui_textedit_t UiTextEdit_t;

typedef enum
{
    enUiTextEditType_Default,
    enUiTextEditType_AlphaNumeric,
    enUiTextEditType_UpperCase,
    enUiTextEditType_LowerCase,
    enUiTextEditType_Numeric,
    enUiTextEditType_Password,
    enUiTextEditType_Custom,
    enUiTextEditType_MaxValue = PCF_ENUM_MAXVALUE
}EnUiTextEditType_t;

typedef void (PCF_STDCALL *UiTextEditStateEventHandler_fp)(const UiTextEdit_t* sender);
typedef void (PCF_STDCALL *UiTextEditFocusChangedEventHandler_fp)(const UiTextEdit_t* sender, EnBoolean_t focused);



PCF_UI_END_CDECLARE

#endif // __PCF_UI_TEXTEDIT_TYPES_H_INCLUDED__
