#ifndef __PCF_UI_TRACKBAR_H_INCLUDED__
#define __PCF_UI_TRACKBAR_H_INCLUDED__

#include <pcf/ui/trackbar/types.h>
#include <pcf/ui/label/types.h>

PCF_UI_BEGIN_CDECLARE

UiTrackbar_t* PCF_UI_API pcf_ui_trackbar_newH(const UiControlInfo_t* args, pcf_status_t* result);
UiTrackbar_t* PCF_UI_API pcf_ui_trackbar_newV(const UiControlInfo_t* args, pcf_status_t* result);
UiControl_t* PCF_UI_API pcf_ui_trackbar_asControl(UiTrackbar_t* trackbar);
pcf_status_t PCF_UI_API pcf_ui_trackbar_delete(UiTrackbar_t* trackbar);

EnBoolean_t  PCF_UI_API  pcf_ui_trackbar_isVertical(const UiTrackbar_t* trackbar, pcf_status_t* status);

pcf_status_t PCF_UI_API  pcf_ui_trackbar_configure(UiTrackbar_t* trackbar, const UiTrackbarConfiguration_t* config);

pcf_status_t PCF_UI_API  pcf_ui_trackbar_setInfo(UiTrackbar_t* trackbar, const UiTrackbarInfo_t* info);
pcf_status_t PCF_UI_API  pcf_ui_trackbar_getInfo(UiTrackbar_t* trackbar, UiTrackbarInfo_t* info);

pcf_status_t PCF_UI_API  pcf_ui_trackbar_setValue(UiTrackbar_t* trackbar, uint32_t value);
pcf_status_t PCF_UI_API  pcf_ui_trackbar_getValue(const UiTrackbar_t* trackbar, uint32_t* result);

pcf_status_t PCF_UI_API  pcf_ui_trackbar_attachChangeHandler(UiTrackbar_t* trackbar, UiTrackbarChangeEventHandler_fp handler);
pcf_status_t PCF_UI_API  pcf_ui_trackbar_removeChangeHandler(UiTrackbar_t* trackbar);

pcf_status_t PCF_UI_API  pcf_ui_trackbar_attachValueChangeHandler(UiTrackbar_t* trackbar, UiTrackbarValueChangeEventHandler_fp handler);
pcf_status_t PCF_UI_API  pcf_ui_trackbar_removeValueChangeHandler(UiTrackbar_t* trackbar);

PCF_UI_END_CDECLARE

#endif // __PCF_UI_TRACKBAR_H_INCLUDED__
