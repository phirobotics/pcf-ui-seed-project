#ifndef __PCF_UI_TRACKBAR_TYPES_H_INCLUDED__
#define __PCF_UI_TRACKBAR_TYPES_H_INCLUDED__

#include <pcf/ui/control/types.h>

PCF_UI_BEGIN_CDECLARE

typedef struct _pcf_ui_trackbar_t UiTrackbar_t;

typedef enum
{
    enUiTrackBarChangeReasonType_LineUp = 0,
    enUiTrackBarChangeReasonType_LineDown = 1,
    enUiTrackBarChangeReasonType_PageUp = 2,
    enUiTrackBarChangeReasonType_PageDown = 3,
    enUiTrackBarChangeReasonType_ThumbPosition = 4,
    enUiTrackBarChangeReasonType_ThumbTrack = 5,
    enUiTrackBarChangeReasonType_Top = 6,
    enUiTrackBarChangeReasonType_Bottom = 7,
    enUiTrackBarChangeReasonType_EndTrack = 8,
    enUiTrackBarChangeReasonType_MaxValue = PCF_ENUM_MAXVALUE
}EnUiTrackBarChangeReasonType_t;

typedef enum
{
    enUiTrackTickStyle_Top = 0x01, //Right
    enUiTrackTickStyle_Down = 0x10, //Left
    enUiTrackTickStyle_Both = (enUiTrackTickStyle_Top | enUiTrackTickStyle_Down),
    enUiTrackTickStyle_MaxValue = PCF_ENUM_MAXVALUE
}EnUiTrackTickStyle_t;

typedef struct
{
    EnBoolean_t hideTicks;
    EnBoolean_t hideToolTips;
    EnUiTrackTickStyle_t tickStyle;

    uint32_t tickFrequency;
    uint32_t pageSize;
    uint32_t minimum;
    uint32_t maximum;
}UiTrackbarConfiguration_t;

typedef struct
{
    uint32_t pageSize;
    uint32_t minimum;
    uint32_t maximum;
    uint32_t position;
}UiTrackbarInfo_t;

typedef struct
{
    EnUiTrackBarChangeReasonType_t reason;
    UiTrackbarInfo_t info;
}UiTrackbarEventArgs_t;

typedef void (PCF_STDCALL *UiTrackbarChangeEventHandler_fp)(const UiTrackbar_t* sender, const UiTrackbarEventArgs_t* args);
typedef void (PCF_STDCALL *UiTrackbarValueChangeEventHandler_fp)(const UiTrackbar_t* sender, uint32_t oldValue, uint32_t newValue);

PCF_UI_END_CDECLARE

#endif // __PCF_UI_TRACKBAR_TYPES_H_INCLUDED__
