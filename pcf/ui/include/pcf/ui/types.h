#ifndef __PCF_UI_TYPES_H_INCLUDED__
#define __PCF_UI_TYPES_H_INCLUDED__

#include <pcf/graphics/types.h>
#include <pcf/ui/defs.h>

PCF_UI_BEGIN_CDECLARE

typedef char    pcf_ui_property_text[PCF_UI_PROPERTY_TEXT_SIZE + 1];
typedef char    pcf_ui_path_text[PCF_PATH_TEXT_SIZE + 1];
typedef GraphicsVersion_t      UiVersion_t;

typedef struct  _pcf_ui_control_t UiControl_t;

typedef enum
{
    enUiPlatformType_None,
    enUiPlatformType_Win32,
    enUiPlatformType_Win32Plus,
    enUiPlatformType_XWindows,
    enUiPlatformType_Raw,
    enUiPlatformType_MaxValue = 0xFF
}EnUiPlatformType_t;

typedef enum
{
    enUiMouseButtonFlag_None,
    enUiMouseButtonFlag_Left = 0x0001,
    enUiMouseButtonFlag_Right = enUiMouseButtonFlag_Left << 1,
    enUiMouseButtonFlag_Shift = enUiMouseButtonFlag_Right << 1,
    enUiMouseButtonFlag_Control = enUiMouseButtonFlag_Shift << 1,
    enUiMouseButtonFlag_Middle = 0x0010,
    enUiMouseButtonFlag_XButton1 = enUiMouseButtonFlag_Middle << 1,
    enUiMouseButtonFlag_XButton2 = enUiMouseButtonFlag_XButton1 << 1,
    enUiMouseButtonType_MaxValue = PCF_ENUM_MAXVALUE
}EnUiMouseButtonFlag_t;

typedef enum
{
    enUiKeyType_None,
    enUiKeyType_Backspace = 0x08,
    enUiKeyType_Tab = 0x09,
    enUiKeyType_Clear = 0x0C,
    enUiKeyType_Enter = 0x0D,
    enUiKeyType_Shift = 0x10,
    enUiKeyType_Control = 0x11,
    enUiKeyType_Alt = 0x12,
    enUiKeyType_Pause = 0x13,
    enUiKeyType_CapsLock = 0x14,
    enUiKeyType_Escape = 0x1B,
    enUiKeyType_Space = 0x20,
    enUiKeyType_PageUp = 0x21,
    enUiKeyType_PageDown = 0x22,
    enUiKeyType_End = 0x23,
    enUiKeyType_Home = 0x24,
    enUiKeyType_LeftArrow = 0x25,
    enUiKeyType_UpArrow = 0x26,
    enUiKeyType_RightArrow = 0x27,
    enUiKeyType_DownArrow = 0x28,
    enUiKeyType_Select = 0x29,
    enUiKeyType_Print = 0x2A,
    enUiKeyType_Execute = 0x2B,
    enUiKeyType_PrintScreen = 0x2C,
    enUiKeyType_Insert = 0x2D,
    enUiKeyType_Delete = 0x2E,
    enUiKeyType_Help = 0x2F,
    enUiKeyType_0 = 0x30,
    enUiKeyType_1 = 0x31,
    enUiKeyType_2 = 0x32,
    enUiKeyType_3 = 0x33,
    enUiKeyType_4 = 0x34,
    enUiKeyType_5 = 0x35,
    enUiKeyType_6 = 0x36,
    enUiKeyType_7 = 0x37,
    enUiKeyType_8 = 0x38,
    enUiKeyType_9 = 0x39,
    enUiKeyType_A = 0x41,
    enUiKeyType_B = 0x42,
    enUiKeyType_C = 0x43,
    enUiKeyType_D = 0x44,
    enUiKeyType_E = 0x45,
    enUiKeyType_F = 0x46,
    enUiKeyType_G = 0x47,
    enUiKeyType_H = 0x48,
    enUiKeyType_I = 0x49,
    enUiKeyType_J = 0x4A,
    enUiKeyType_K = 0x4B,
    enUiKeyType_L = 0x4C,
    enUiKeyType_M = 0x4D,
    enUiKeyType_N = 0x4E,
    enUiKeyType_O = 0x4F,
    enUiKeyType_P = 0x50,
    enUiKeyType_Q = 0x51,
    enUiKeyType_R = 0x52,
    enUiKeyType_S = 0x53,
    enUiKeyType_T = 0x54,
    enUiKeyType_U = 0x55,
    enUiKeyType_V = 0x56,
    enUiKeyType_W = 0x57,
    enUiKeyType_X = 0x58,
    enUiKeyType_Y = 0x59,
    enUiKeyType_Z = 0x5A,
    enUiKeyType_NumPad0 = 0x60,
    enUiKeyType_NumPad1 = 0x61,
    enUiKeyType_NumPad2 = 0x62,
    enUiKeyType_NumPad3 = 0x63,
    enUiKeyType_NumPad4 = 0x64,
    enUiKeyType_NumPad5 = 0x65,
    enUiKeyType_NumPad6 = 0x66,
    enUiKeyType_NumPad7 = 0x67,
    enUiKeyType_NumPad8 = 0x68,
    enUiKeyType_NumPad9 = 0x69,
    enUiKeyType_Multiply = 0x6A,
    enUiKeyType_Add = 0x6B,
    enUiKeyType_Separator = 0x6C,
    enUiKeyType_Subtract = 0x6D,
    enUiKeyType_Decimal = 0x6E,
    enUiKeyType_Divide = 0x6F,
    enUiKeyType_F1 = 0x70,
    enUiKeyType_F2 = 0x71,
    enUiKeyType_F3 = 0x72,
    enUiKeyType_F4 = 0x73,
    enUiKeyType_F5 = 0x74,
    enUiKeyType_F6 = 0x75,
    enUiKeyType_F7 = 0x76,
    enUiKeyType_F8 = 0x77,
    enUiKeyType_F9 = 0x78,
    enUiKeyType_F10 = 0x79,
    enUiKeyType_F11 = 0x7A,
    enUiKeyType_F12 = 0x7B,
    enUiKeyType_F13 = 0x7C,
    enUiKeyType_F14 = 0x7D,
    enUiKeyType_F15 = 0x7E,
    enUiKeyType_F16 = 0x7F,
    enUiKeyType_F17 = 0x80,
    enUiKeyType_F18 = 0x81,
    enUiKeyType_F19 = 0x82,
    enUiKeyType_F20 = 0x83,
    enUiKeyType_F21 = 0x84,
    enUiKeyType_F22 = 0x85,
    enUiKeyType_F23 = 0x86,
    enUiKeyType_F24 = 0x87,
    enUiKeyType_NumLock = 0x90,
    enUiKeyType_Scroll = 0x91,

    enUiKeyType_MaxValue = PCF_ENUM_MAXVALUE
}EnUiKeyCode_t;

typedef enum
{
    enControlAnchorFlag_None = 0,

    enControlAnchorFlag_Top = 1,
    enControlAnchorFlag_Bottom = (enControlAnchorFlag_Top << 1),
    enControlAnchorFlag_Left = (enControlAnchorFlag_Bottom << 1),
    enControlAnchorFlag_Right = (enControlAnchorFlag_Left << 1),

    enControlAnchorFlag_TopLeft = (enControlAnchorFlag_Top | enControlAnchorFlag_Left),
    enControlAnchorFlag_BottomLeft = (enControlAnchorFlag_Bottom | enControlAnchorFlag_Left),
    enControlAnchorFlag_TopRight = (enControlAnchorFlag_Top | enControlAnchorFlag_Right),
    enControlAnchorFlag_BottomRight = (enControlAnchorFlag_Bottom | enControlAnchorFlag_Right),
    enControlAnchorFlag_LeftRight = (enControlAnchorFlag_Left | enControlAnchorFlag_Right),
    enControlAnchorFlag_TopBottom = (enControlAnchorFlag_Bottom | enControlAnchorFlag_Top),

    enControlAnchorFlag_TopBottomRight = (enControlAnchorFlag_TopBottom | enControlAnchorFlag_Right),
    enControlAnchorFlag_TopBottomLeft = (enControlAnchorFlag_TopBottom | enControlAnchorFlag_Left),
    enControlAnchorFlag_LeftRightTop = (enControlAnchorFlag_LeftRight | enControlAnchorFlag_Top),
    enControlAnchorFlag_LeftRightBottom = (enControlAnchorFlag_LeftRight | enControlAnchorFlag_Bottom),

    enControlAnchorFlag_All = (enControlAnchorFlag_LeftRight | enControlAnchorFlag_TopBottom),
    enControlAnchorFlag_MaxValue = PCF_ENUM_MAXVALUE
}EnControlAnchorFlag_t;

typedef union
{
    uint32_t id;
    char     text[4];
}UiControlTypeId_t;

typedef struct _pcf_ui_controlGeometry_t
{
    GraphicsPoint_t topLeft;
    GraphicsSize_t  size;
}UiControlGeometry_t;

typedef struct _pcf_ui_controlInfo
{
    UiControl_t*         parent;
    GraphicsPoint_t      location;
    GraphicsSize_t       size;
    GraphicsSize_t       minimumSize;
    GraphicsSize_t       maximumSize;
    char*                text;
    uint32_t             typeId;
}UiControlInfo_t;

typedef struct
{
    EnUiMouseButtonFlag_t buttons;
    int32_t x;
    int32_t y;
}UiControlMouseEventArgs_t;

typedef struct
{
    EnUiKeyCode_t keyCode;
    EnBoolean_t   keyDown;
    EnBoolean_t   isChar;
    EnBoolean_t   isShift;
    EnBoolean_t   isCtrl;
    EnBoolean_t   isAlt;
    EnBoolean_t   isNumpad;
}UiControlKeyboardEventArgs_t;

typedef struct
{
    EnUiMouseButtonFlag_t buttons;
    EnBoolean_t down;
    int32_t x;
    int32_t y;
}UiControlMouseClickEventArgs_t;

typedef struct
{
    EnBoolean_t minimized;
    EnBoolean_t maximized;
    int32_t x;
    int32_t y;
    int32_t width;
    int32_t height;
}UiControlPositionSizeEventArgs_t;

typedef struct
{
   GraphicsContext2D_t* context;
   GraphicsRect_t       clientRect;
}UiPaintEventArgs_t;

typedef struct
{
    PcfMemoryConfig_t memConfig;
    EnBoolean_t supportConfiguration;
    EnBoolean_t supportPlugins;
    EnBoolean_t supportResources;
    void* extra;
}UiConfig_t;

typedef EnBoolean_t (*UiEditCharValidator_fp)(uint32_t input);

PCF_UI_END_CDECLARE

#endif // __PCF_UI_TYPES_H_INCLUDED__
