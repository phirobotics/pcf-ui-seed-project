#ifndef __PCF_UI_H_INCLUDED__
#define __PCF_UI_H_INCLUDED__

#include <pcf/ui/types.h>
PCF_UI_BEGIN_CDECLARE


uint32_t            PCF_UI_API pcf_ui_getVersion(void);
EnUiPlatformType_t  PCF_UI_API pcf_ui_getPlatfromType(void);
const char*         PCF_UI_API pcf_ui_getPlatfromTypeText(void);
pcf_status_t        PCF_UI_API pcf_ui_log(EnLogType_t logType, const char* fileName,
                                           const char* functionName, int32_t line,
                                           const char* format, ...);

pcf_status_t        PCF_UI_API pcf_ui_init(const UiConfig_t* config);
EnBoolean_t         PCF_UI_API pcf_ui_isInitialized();

void*               PCF_UI_API pcf_ui_malloc(size_t size, pcf_status_t* status);
pcf_status_t        PCF_UI_API pcf_ui_free(void* mem);
void*               PCF_UI_API pcf_ui_realloc(void* mem, size_t resize, pcf_status_t* status);

const char*         PCF_UI_API pcf_ui_getDllDirectory(pcf_status_t* status);
const char*         PCF_UI_API pcf_ui_getApplicationExecutableName(pcf_status_t* status);
const char*         PCF_UI_API pcf_ui_getApplicationExeDirectory(pcf_status_t* status);
const char*         PCF_UI_API pcf_ui_getApplicationConfigDirectory(pcf_status_t* status);
const char*         PCF_UI_API pcf_ui_getApplicationResourceDirectory(pcf_status_t* status);
const char*         PCF_UI_API pcf_ui_getApplicationPluginDirectory(pcf_status_t* status);

pcf_status_t        PCF_UI_API pcf_ui_showMessage(const char* message);

PCF_UI_END_CDECLARE
#endif // __PCF_UI_H_INCLUDED__
